﻿using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NPCPortal.Jobs;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class EmailController : NPCPortal.Controllers.BaseController
    {
		[NPCAuthorize(PermissionTypes.SendBroadcasts)]
        public ActionResult Broadcast(string success = null, string error = null)
        {
			if (!User.CanDo(PermissionTypes.SendBroadcasts))
				return new HttpUnauthorizedResult();

			ViewBag.Roles = db.Roles.OrderBy(r => r.Name);

			if (!String.IsNullOrEmpty(success))
				ViewBag.Success = success;
			else if (!String.IsNullOrEmpty(error))
				ViewBag.Error = error;
			
            return View(new BroadcastModel());
        }

		[NPCAuthorize(PermissionTypes.SendBroadcasts)]
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Broadcast(BroadcastModel broadcast)
		{
			if (!User.CanDo(PermissionTypes.SendBroadcasts))
				return new HttpUnauthorizedResult();

			if (ModelState.IsValid)
			{
				try
				{
					JobManager.SubmitJob(new NPCPortal.Jobs.Emails.BroadcastEmailBurster()
						{
							Role = broadcast.Role,
							Subject = broadcast.Subject,
							Body = broadcast.Message,
							TestRecipientUserId = broadcast.TestOnly ? User.Identity.GetUserId() : null
						});

					return Broadcast("Your broadcast message has been sent.");
				}
				catch(Exception ex)
				{
					return Broadcast(null, "There was an error sending your message:\n" + ex.ToString());
				}
			}
			else
				return View();
		}

		public class BroadcastModel
		{
			[Required]
			public string Subject { get; set; }
			public string Role { get; set; }
			[Required]
			public string Message { get; set; }
			public bool TestOnly { get; set; }

			public BroadcastModel()
			{
				this.TestOnly = true;
			}
		}
	}
}
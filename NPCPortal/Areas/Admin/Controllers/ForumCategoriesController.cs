﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using NPCPortal.Controllers;
using NPCPortal.Utilities;

namespace NPCPortal.Areas.Admin.Controllers
{
	public class ForumCategoriesController : BaseController
	{
		[NPCAuthorize( new PermissionTypes[] { PermissionTypes.CreateForums, PermissionTypes.EditForums, PermissionTypes.DeleteForums })]
        public ActionResult Index()
        {
			if (!User.CanDoAny(PermissionTypes.CreateForums, PermissionTypes.EditForums, PermissionTypes.DeleteForums))
				return new HttpUnauthorizedResult();

            return View(db.ForumCategories.ToList());
        }

		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.CreateForums, PermissionTypes.EditForums, PermissionTypes.DeleteForums })]
        public ActionResult Details(int? id)
		{
			if (!User.CanDoAny(PermissionTypes.CreateForums, PermissionTypes.EditForums, PermissionTypes.DeleteForums))
				return new HttpUnauthorizedResult();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumCategory forumcategory = db.ForumCategories.Find(id);
            if (forumcategory == null)
            {
                return HttpNotFound();
            }
            return View(forumcategory);
        }

		[NPCAuthorize(PermissionTypes.CreateForums)]
        public ActionResult Create()
		{
			ViewBag.Categories = db.ForumCategories;
            return View();
        }

		[NPCAuthorize(PermissionTypes.CreateForums)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Name,Description,RestrictedToRole,MembersOnly")] ForumCategory forumcategory, int? parentCategoryID)
        {
            if (ModelState.IsValid)
			{
				db.ForumCategories.Add(forumcategory);

				if (parentCategoryID.HasValue)
					forumcategory.ParentCategory = db.ForumCategories.Single(c => c.ID == parentCategoryID.Value);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(forumcategory);
        }

		[NPCAuthorize(PermissionTypes.EditForums)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumCategory forumcategory = db.ForumCategories.Find(id);
            if (forumcategory == null)
            {
                return HttpNotFound();
            }

			ViewBag.Categories = db.ForumCategories;

            return View(forumcategory);
        }

		[NPCAuthorize(PermissionTypes.EditForums)]
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "ID,Name,Description,RestrictedToRole,MembersOnly")] ForumCategory forumCategory, int? parentCategoryID)
        {
            if (ModelState.IsValid)
            {
				var existingCategory = db.ForumCategories.Single(c => c.ID == forumCategory.ID);

				existingCategory.Name = forumCategory.Name;
				existingCategory.Description = forumCategory.Description;
				existingCategory.RestrictedToRole = forumCategory.RestrictedToRole;
				existingCategory.MembersOnly = forumCategory.MembersOnly;

				if (parentCategoryID.HasValue)
					existingCategory.ParentCategory = db.ForumCategories.Single(c => c.ID == parentCategoryID.Value);
				else if(existingCategory.ParentCategory != null && existingCategory.ParentCategory.ChildCategories.Any(c => c.ID == existingCategory.ID))
					existingCategory.ParentCategory.ChildCategories.Remove(existingCategory);

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(forumCategory);
        }

		[NPCAuthorize(PermissionTypes.DeleteForums)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumCategory forumcategory = db.ForumCategories.Find(id);
            if (forumcategory == null)
            {
                return HttpNotFound();
			}

			ViewBag.Categories = db.ForumCategories.Where(c => c.ID != id.Value);

            return View(forumcategory);
        }

		[NPCAuthorize(PermissionTypes.DeleteForums)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ForumCategory forumCategory = db.ForumCategories.Find(id);

			if (Request.Form["CascadeOption"] == "MoveDescendants")
			{
				int moveToCategoryID = Convert.ToInt32(Request.Form["MoveToForumID"]);
				ForumCategory relocationCategory = db.ForumCategories.Single(c => c.ID == moveToCategoryID);

				foreach(var forum in forumCategory.Forums)
				{
					forum.Category = relocationCategory;
				}
				db.SaveChanges();
			}

			forumCategory.Delete(db);

            return RedirectToAction("Index");
        }
    }
}

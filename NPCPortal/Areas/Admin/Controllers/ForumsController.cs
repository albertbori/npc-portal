﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using NPCPortal.Controllers;

namespace NPCPortal.Areas.Admin.Controllers
{
	public class ForumsController : BaseController
	{
		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.CreateForums, PermissionTypes.EditForums, PermissionTypes.DeleteForums })]
        public ActionResult Index()
        {
            return View(db.Forums.ToList());
        }

		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.CreateForums, PermissionTypes.EditForums, PermissionTypes.DeleteForums })]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Forums.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

		[NPCAuthorize(PermissionTypes.CreateForums)]
        public ActionResult Create()
		{
			ViewBag.Categories = db.ForumCategories;
            return View();
        }

		[NPCAuthorize(PermissionTypes.CreateForums)]
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "ID,Name,Description,ViewRole,PostRole,MembersOnly")] Forum forum, int categoryID)
        {
            if (ModelState.IsValid)
            {
                db.Forums.Add(forum);

				forum.Category = db.ForumCategories.Single(c => c.ID == categoryID);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(forum);
        }

		[NPCAuthorize(PermissionTypes.EditForums)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Forums.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
			}

			ViewBag.Categories = db.ForumCategories;

            return View(forum);
        }

		[NPCAuthorize(PermissionTypes.EditForums)]
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "ID,Name,Description,ViewRole,PostRole,MembersOnly")] Forum forum, int categoryID)
        {
            if (ModelState.IsValid)
            {
				var existingForum = db.Forums.Single(f => f.ID == forum.ID);

				existingForum.Name = forum.Name;
				existingForum.Description = forum.Description;
				existingForum.ViewRole = forum.ViewRole;
				existingForum.PostRole = forum.PostRole;
				existingForum.MembersOnly = forum.MembersOnly;

				existingForum.Category = db.ForumCategories.Single(c => c.ID == categoryID);

				db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(forum);
        }

		[NPCAuthorize(PermissionTypes.DeleteForums)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Forum forum = db.Forums.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
			}

			ViewBag.Forums = db.Forums.Where(f => f.ID != id.Value);

            return View(forum);
        }

		[NPCAuthorize(PermissionTypes.DeleteForums)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Forum forum = db.Forums.Find(id);
						
			if (Request.Form["CascadeOption"] == "MoveDescendants")
			{
				int moveToForumID = Convert.ToInt32(Request.Form["MoveToForumID"]);
				Forum relocationForum = db.Forums.Single(c => c.ID == moveToForumID);

				foreach (var discussion in forum.Discussions)
				{
					discussion.Forum = relocationForum;
				}
				db.SaveChanges();
			}

			forum.Delete(db);
            return RedirectToAction("Index");
        }
    }
}

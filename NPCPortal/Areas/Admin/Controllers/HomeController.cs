﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Utilities;

namespace NPCPortal.Areas.Admin.Controllers
{
	public class HomeController : BaseController
	{
        public ActionResult Index()
        {
			if (!User.CanDoAny(Role.AdminAccessPermissions))
				return new HttpUnauthorizedResult();

			ViewBag.UserCount = db.Users.Count();
			ViewBag.PostCount = db.Content.OfType<ForumPost>().Count();
			ViewBag.ImageCount = db.Files.Count(i => i.FileType.StartsWith("image"));
			ViewBag.FileCount = db.Files.Count(i => !i.FileType.StartsWith("image"));

            return View();
        }
	}
}
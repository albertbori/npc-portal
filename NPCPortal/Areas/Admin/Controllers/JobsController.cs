﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using NPCPortal.Jobs;
using NPCPortal.Models.ViewModels;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class JobsController : NPCPortal.Controllers.BaseController
    {
        // GET: /Admin/Jobs/
		[NPCAuthorize(PermissionTypes.ViewJobs)]
        public ActionResult Index()
        {
			Pager pager = new Pager(db.Jobs.Count());
			ViewBag.Pager = pager;

			var jobs = db.Jobs.OrderByDescending(j => j.DateCreated)
				.Skip((pager.CurrentPage - 1) * pager.RecordsPerPage)
				.Take(pager.RecordsPerPage);

            return View(jobs);
        }

		// GET: /Admin/Jobs/Details/5
		[NPCAuthorize(PermissionTypes.ViewJobs)]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

		// GET: /Admin/Jobs/Create
		[NPCAuthorize(PermissionTypes.RunJobs)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Admin/Jobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[ValidateAntiForgeryToken]
		[NPCAuthorize(PermissionTypes.RunJobs)]
        public ActionResult Create(IJob job)
		{
			JobManager.SubmitJob(job);

			return RedirectToAction("Index");
        }
		
		[NPCAuthorize(PermissionTypes.RunJobs)]
		public ActionResult Rerun(Guid id)
		{
			var jobRecord = db.Jobs.Find(id);			

			var job = (IJob)Newtonsoft.Json.JsonConvert.DeserializeObject(jobRecord.Arguments, Type.GetType(jobRecord.Name));

			JobManager.SubmitJob(job);

			for (int i = 0; i < 100; i++)
			{
				if(db.Jobs.Any(j => j.ID == job.ID))
				{
					return RedirectToAction("Details", new { id = job.ID });
				}
				System.Threading.Thread.Sleep(100);
			}

			return RedirectToAction("Index");
		}

		[NPCAuthorize(PermissionTypes.ViewJobs)]
		public ActionResult Clear()
		{
			foreach(var job in db.Jobs.ToArray())
			{
				db.Jobs.Remove(job);
			}
			db.SaveChanges();

			return RedirectToAction("Index");
		}
    }
}

﻿using NPCPortal.Models;
using NPCPortal.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class LogsController : NPCPortal.Controllers.BaseController
	{
		[NPCAuthorize(PermissionTypes.ViewLogs)]
        public ActionResult Index(string query = null)
        {
			var logs = db.LogMessages.Where(m => true);
			if (!String.IsNullOrEmpty(query))
			{
				foreach (string s in query.Split(new char[] { }))
				{
					logs = logs.Where(m => m.Message.Contains(s));
				}
			}

			if (!String.IsNullOrEmpty(Request["filter"]))
			{
				var filters = Request["filter"].Split(',').Select(f => (LogLevel)Enum.Parse(typeof(LogLevel), f));

				logs = logs.Where(l => filters.Any(f => f == l.Level));
			}
			
			Pager pager = new Pager(logs.Count());
			ViewBag.Pager = pager;

			logs = logs.OrderByDescending(m => m.Date).Skip((pager.CurrentPage - 1) * pager.RecordsPerPage).Take(pager.RecordsPerPage);

			return View(logs);
        }

		[NPCAuthorize(PermissionTypes.ViewLogs)]
		public ActionResult Clear()
		{
			foreach (var log in db.LogMessages.ToArray())
			{
				db.LogMessages.Remove(log);
			}
			db.SaveChanges();

			return RedirectToAction("Index");
		}
	}
}
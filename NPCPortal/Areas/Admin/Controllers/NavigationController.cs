﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class NavigationController : NPCPortal.Controllers.BaseController
    {
        // GET: /Admin/Navigation/
        public ActionResult Index()
        {
            return View(db.NavMenuItems.ToList());
        }

        // GET: /Admin/Navigation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NavMenuItem navmenuitem = db.NavMenuItems.Find(id);
            if (navmenuitem == null)
            {
                return HttpNotFound();
            }
            return View(navmenuitem);
        }

        // GET: /Admin/Navigation/Create
        public ActionResult Create()
        {
			ViewBag.MenuItems = db.NavMenuItems.OrderBy(i => i.Title);
            return View();
        }

        // POST: /Admin/Navigation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "ID,Title,URL,OpenInNewWindow,MembersOnly,ViewRole,Permission")] NavMenuItem navmenuitem, int? parentItemID)
        {
            if (ModelState.IsValid)
            {
                db.NavMenuItems.Add(navmenuitem);
				navmenuitem.Parent = db.NavMenuItems.Find(parentItemID);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(navmenuitem);
        }

        // GET: /Admin/Navigation/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NavMenuItem navmenuitem = db.NavMenuItems.Find(id);
            if (navmenuitem == null)
            {
                return HttpNotFound();
			}
			ViewBag.MenuItems = db.NavMenuItems.Where(i => i.ID != id).OrderBy(i => i.Title);
            return View(navmenuitem);
        }

        // POST: /Admin/Navigation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "ID,Title,URL,OpenInNewWindow,MembersOnly,ViewRole,Permission")] NavMenuItem navmenuitem, int? parentItemID)
        {
            if (ModelState.IsValid)
            {
				var item = db.NavMenuItems.Find(navmenuitem.ID);
				item.MembersOnly = navmenuitem.MembersOnly;
				item.OpenInNewWindow = navmenuitem.OpenInNewWindow;
				item.Parent = db.NavMenuItems.Find(parentItemID);
				item.Permission = navmenuitem.Permission;
				item.Title = navmenuitem.Title;
				item.URL = navmenuitem.URL;
				item.ViewRole = navmenuitem.ViewRole;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(navmenuitem);
        }

        // GET: /Admin/Navigation/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NavMenuItem navmenuitem = db.NavMenuItems.Find(id);
            if (navmenuitem == null)
            {
                return HttpNotFound();
            }
            return View(navmenuitem);
        }

        // POST: /Admin/Navigation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NavMenuItem navmenuitem = db.NavMenuItems.Find(id);
            db.NavMenuItems.Remove(navmenuitem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}

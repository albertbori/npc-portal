﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using Microsoft.AspNet.Identity;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class PagesController : NPCPortal.Controllers.BaseController
    {
        // GET: /Admin/Pages/
        public ActionResult Index()
        {
			return View(db.CMSPages.OfType<CMSPage>().ToList());
        }

        // GET: /Admin/Pages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CMSPage cmspage = db.CMSPages.Find(id);
            if (cmspage == null)
            {
                return HttpNotFound();
            }
            return View(cmspage);
        }

        // GET: /Admin/Pages/Create
        public ActionResult Create()
		{
			ViewBag.MenuItems = db.NavMenuItems.OrderBy(i => i.Title);
            return View();
        }

        // POST: /Admin/Pages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[ValidateAntiForgeryToken]
		[ValidateInput(false)]
        public ActionResult Create([Bind(Include="ID,ModerationStatus,DateCreated,Title,Body,MembersOnly,ViewRole,EditRole")] CMSPage cmspage, bool createNavItem, int? parentItemID)
        {
            if (ModelState.IsValid)
            {
                db.Content.Add(cmspage);
				cmspage.Author = db.Users.Find(User.Identity.GetUserId());

				db.SaveChanges();

				if (createNavItem)
				{
					NavMenuItem navItem = new NavMenuItem()
					{
						Title = cmspage.Title,
						MembersOnly = cmspage.MembersOnly,
						ViewRole = cmspage.ViewRole,
						URL = Url.Action("View", "Pages", new { id = cmspage.ID, area = "" })
					};
					if (parentItemID != null)
						navItem.Parent = db.NavMenuItems.Find(parentItemID.Value);

					db.NavMenuItems.Add(navItem);

					db.SaveChanges();
				}

                return RedirectToAction("Index");
            }

            return View(cmspage);
        }

        // GET: /Admin/Pages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			CMSPage cmspage = db.CMSPages.Find(id);
            if (cmspage == null)
            {
                return HttpNotFound();
			}
            return View(cmspage);
        }

        // POST: /Admin/Pages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[ValidateAntiForgeryToken]
		[ValidateInput(false)]
        public ActionResult Edit([Bind(Include="ID,ModerationStatus,DateCreated,Title,Body,MembersOnly,ViewRole,EditRole")] CMSPage cmspage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cmspage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cmspage);
        }

        // GET: /Admin/Pages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			CMSPage cmspage = db.CMSPages.Find(id);
            if (cmspage == null)
            {
                return HttpNotFound();
            }
            return View(cmspage);
        }

        // POST: /Admin/Pages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
			CMSPage cmspage = db.CMSPages.Find(id);
			string url = Url.Action("View", "Pages", new { id = cmspage.ID, area = "" });
			cmspage.Delete(db);

			//Remove any nav links to this item
			foreach(var navItem in db.NavMenuItems.Where(i => i.URL == url).ToArray())
			{
				db.NavMenuItems.Remove(navItem);
			}
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}

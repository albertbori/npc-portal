﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using NPCPortal.Controllers;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class RolesController : BaseController
    {
		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.CreateRoles, PermissionTypes.EditRoles, PermissionTypes.DeleteRoles })]
        public ActionResult Index()
        {
            return View(db.Roles.ToList());
        }

		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.CreateRoles, PermissionTypes.EditRoles, PermissionTypes.DeleteRoles })]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

		[NPCAuthorize(PermissionTypes.CreateRoles)]
        public ActionResult Create()
        {
            return View();
        }
		
		[NPCAuthorize(PermissionTypes.CreateRoles)]
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,Name,Permissions")] Role role)
        {
            if (ModelState.IsValid)
            {
                db.Roles.Add(role);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(role);
        }

		[NPCAuthorize(PermissionTypes.EditRoles)]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			IdentityRole role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

		[NPCAuthorize(PermissionTypes.EditRoles)]
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,Name,Permissions")] Role role)
        {
            if (ModelState.IsValid)
            {
                db.Entry(role).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(role);
        }

		[NPCAuthorize(PermissionTypes.DeleteRoles)]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			IdentityRole role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

		[NPCAuthorize(PermissionTypes.DeleteRoles)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
			IdentityRole role = db.Roles.Find(id);
            db.Roles.Remove(role);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}

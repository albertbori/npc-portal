﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Areas.Admin.Controllers
{
	public class SettingsController : BaseController
	{
		[NPCAuthorize(PermissionTypes.EditGeneralSettings)]
		public ActionResult Edit(string id, bool editSuccess = false)
		{
			Type editObjectType = SiteSetting.GetAllSiteSettingsTypes().Single(t => t.Name.ToLower() == id.ToLower());

			var siteSettings = db.SiteSettings.ToList();
			var siteSetting = siteSettings.SingleOrDefault(us => editObjectType.IsInstanceOfType(us));

			if (siteSetting == null)
			{
				siteSetting = (SiteSetting)Activator.CreateInstance(SiteSetting.GetAllSiteSettingsTypes().Single(s => s.Name.ToLower() == id.ToLower()));
			}

			if (editSuccess)
				ViewBag.SuccessMessage = "Your settings have been saved.";

			return View(siteSetting);
		}

		[ValidateInput(false)]
		[NPCAuthorize(PermissionTypes.EditGeneralSettings)]
		[HttpPost]
		public ActionResult Save(SiteSetting setting)
		{
			if (setting.ID == 0)
			{
				db.SiteSettings.Add(setting);
			}
			else
			{
				var entry = db.Entry(db.SiteSettings.Single(s => s.ID == setting.ID));
				
				MergeDisconnectedEntity(entry.Entity, setting);
			}

            if (setting is IValidatableSiteSetting)
            {
                string error;
                if (!((IValidatableSiteSetting)setting).Validate(out error))
                {
                    ViewBag.ErrorMessage = String.Format("Failed to save settings: {0}", error);
                    return View("Edit", setting);
                }
            }

			foreach(string fileKey in Request.Files)
			{
				if(Request.Files[fileKey].ContentLength > 0)
				{
					string fieldName = fileKey.Substring(0, fileKey.LastIndexOf("File"));
					string directory = Request.Form[fieldName + "Directory"];
					string fileName = Request.Form[fieldName];

					Request.Files[fileKey].SaveAs(System.IO.Path.Combine(Server.MapPath(directory), fileName));
				}
			}

			db.SaveChanges();

			NPCPortalWeb.InitSiteSettings();

			return RedirectToAction("Edit", new { id = setting.GetType().Name, editSuccess = true });
		}
	}
}
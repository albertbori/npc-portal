﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using NPCPortal.Controllers;
using System.IO;
using System.Threading;
using NPCPortal.Utilities;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class ThemesController : BaseController
	{
		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.CreateThemes, PermissionTypes.DeleteThemes })]
        public ActionResult Index()
        {
            return View(db.Themes.ToList());
        }

		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.CreateThemes, PermissionTypes.DeleteThemes })]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Theme theme = db.Themes.Find(id);
            if (theme == null)
            {
                return HttpNotFound();
            }
            return View(theme);
        }

		[NPCAuthorize(PermissionTypes.CreateThemes)]
        public ActionResult Create()
        {
            return View();
        }

		[NPCAuthorize(PermissionTypes.CreateThemes)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Name,IsActive")] Theme theme)
        {
			string themeName = String.Empty;
			string zipFileName = String.Empty;
			var themeFile = Request.Files["ThemeFile"];
			if (themeFile == null)
				ModelState.AddModelError("", "Missing Theme File");
			else
			{
				if (!themeFile.FileName.ToLower().EndsWith(".zip"))
					ModelState.AddModelError("", "Invalid theme file type. Must be of type .zip");

				zipFileName = themeFile.FileName;
				themeName = zipFileName.Remove(zipFileName.Length - 4);

				if (db.Themes.Any(t => t.Name == themeName))
					ModelState.AddModelError("", "A theme with this name already exists.");
			}

            if (ModelState.IsValid)
            {
				string themesPath = Server.MapPath("~/Themes");
				if(!Directory.Exists(themesPath))
					Directory.CreateDirectory(themesPath);

				string zipPath = zipPath = Server.MapPath("~/Themes/" + zipFileName);
				string themeDirectory = Server.MapPath("~/Themes/" + themeName);

				theme.Name = themeName;

				Request.Files["ThemeFile"].SaveAs(zipPath);
				System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, themeDirectory);

				System.IO.File.Delete(zipPath);

				if (!System.IO.File.Exists(themeDirectory + "\\Web.config"))
				{
					ModelState.AddModelError("", "Invalid Theme. Missing required file: \"Web.config\"");
				}
				if (!System.IO.File.Exists(themeDirectory + "\\_ViewStart.cshtml"))
				{
					ModelState.AddModelError("", "Invalid Theme. Missing required file: \"_ViewStart.cshtml\"");
				}
				if(!System.IO.File.Exists(themeDirectory + "\\Shared\\_Layout.cshtml"))
				{
					ModelState.AddModelError("", "Invalid Theme. Missing required file: \"Shared\\_Layout.cshtml\"");
				}

				if (!ModelState.IsValid)
				{
					FileHelpers.DeleteDirectory(themeDirectory);
					return View(theme);
				}

                db.Themes.Add(theme);
                db.SaveChanges();

				if (theme.IsActive)
					return Switch(theme.ID);

                return RedirectToAction("Index");
            }

            return View(theme);
        }

		[NPCAuthorize(PermissionTypes.DeleteThemes)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Theme theme = db.Themes.Find(id);
            if (theme == null)
            {
                return HttpNotFound();
            }
            return View(theme);
        }

		[NPCAuthorize(PermissionTypes.DeleteThemes)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Theme theme = db.Themes.Find(id);

			theme.IsActive = false;
			db.SaveChanges();

			string directory = Server.MapPath("~/Themes/" + theme.Name);
			FileHelpers.DeleteDirectory(directory);

            db.Themes.Remove(theme);
            db.SaveChanges();

			if (db.Themes.Count() > 0)
				return Switch(db.Themes.First().ID);
			else
			{
				NPCPortalWeb.ChangeTheme(null);
				return RedirectToAction("Index");
			}
        }

		[NPCAuthorize(PermissionTypes.SwitchThemes)]
		public ActionResult Switch(int id)
		{
			var themes = db.Themes;
			string activeThemeName = String.Empty;
			foreach (var theme in themes)
			{
				if (theme.ID == id)
				{
					theme.IsActive = true;
					activeThemeName = theme.Name;
				}
				else
					theme.IsActive = false;
			}
			db.SaveChanges();

			NPCPortalWeb.ChangeTheme(activeThemeName);

			return RedirectToAction("Index");
		}
    }
}

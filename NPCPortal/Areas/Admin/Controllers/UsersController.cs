﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using NPCPortal.Controllers;
using NPCPortal.Jobs;
using NPCPortal.Jobs.Emails;
using NPCPortal.Models.ViewModels;

namespace NPCPortal.Areas.Admin.Controllers
{
    public class UsersController : BaseController
    {
		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.EditUsers, PermissionTypes.DeleteUsers })]
        public ActionResult Index()
		{
			Pager pager = new Pager(db.Users.Count());
			ViewBag.Pager = pager;

			var users = db.Users.OrderBy(u => u.UserName).Skip((pager.CurrentPage - 1) * pager.RecordsPerPage).Take(pager.RecordsPerPage);

			return View(users);
        }

		[NPCAuthorize(new PermissionTypes[] { PermissionTypes.EditUsers, PermissionTypes.DeleteUsers })]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

		[NPCAuthorize(PermissionTypes.EditUsers)]
		public ActionResult Edit(string id)
		{
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
			return View(user);
		}

		[NPCAuthorize(PermissionTypes.EditUsers)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(User editUser)
		{
			if (db.Users.Any(u => u.Id != editUser.Id && u.UserName == editUser.UserName))
				ModelState.AddModelError("", "Another user already exists with this user name.");
			if (db.Users.Any(u => u.Id != editUser.Id && u.Email == editUser.Email))
				ModelState.AddModelError("", "Another user already exists with this email.");

			if (ModelState.IsValid)
			{
				var user = db.Users.Find(editUser.Id);

				user.UserName = editUser.UserName;
				user.Email = editUser.Email;
				user.UserState = editUser.UserState;

				db.SaveChanges();

				ViewBag.Success = "Your changes have been saved.";
			}

			return View(editUser);
		}

		[NPCAuthorize(PermissionTypes.DeleteUsers)]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

		[NPCAuthorize(PermissionTypes.DeleteUsers)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
			User user = db.Users.Find(id);
			user.Delete(db);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

		[NPCAuthorize(PermissionTypes.EditUsers)]
		public ActionResult UserRoles(string id)
		{
			var user = db.Users.Single(u => u.Id == id);

			ViewBag.Roles = db.Roles;

			return View(user);
		}

		[NPCAuthorize(PermissionTypes.EditUsers)]
		[HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult AddRole(string userID, string role)
		{			
			UserManager<User> userManager = new UserManager<User>(new UserStore<User>(new NPCPortalDB()));

			userManager.AddToRole(userID, role);

			return RedirectToAction("UserRoles", new { id = userID });
		}

		[NPCAuthorize(PermissionTypes.EditUsers)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult RemoveRole(string userID, string role)
		{
			UserManager<User> userManager = new UserManager<User>(new UserStore<User>(new NPCPortalDB()));

			userManager.RemoveFromRole(userID, role);

			return RedirectToAction("UserRoles", new { id = userID });
		}

		[NPCAuthorize(PermissionTypes.ApproveUsers)]
		public ActionResult Approve()
		{
			ViewBag.RecentApprovals = db.Users.Where(u => (u.UserState & UserStates.Approved) != 0).OrderByDescending(u => u.DateJoined).Take(5).ToList();
			return View(db.Users.Where(u => (u.UserState & UserStates.EmailValidated) != 0
				&& (u.UserState & UserStates.Approved) == 0 
				&& (u.UserState & UserStates.Rejected) == 0)
				.OrderBy(u => u.DateJoined));
		}


		[NPCAuthorize(PermissionTypes.ApproveUsers)]
		[HttpPost]
		public ActionResult BulkApprove()
		{
			string[] userIds = Request.Form["cbUserAction"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			var users = db.Users.Where(u => userIds.Contains(u.Id));
			if(Request["FormAction"] == "ApproveSelected")
			{
				foreach(var user in users)
				{
					user.UserState = user.UserState | UserStates.Approved;
					JobManager.SubmitJob(new WelcomeEmail() { UserID = user.Id });
				}
			}
			else if(Request["FormAction"] == "RejectSelected")
			{
				foreach (var user in users)
				{
					user.UserState = user.UserState | UserStates.Rejected;
				}
			}
			db.SaveChanges();

			return RedirectToAction("Approve");
		}
    }
}

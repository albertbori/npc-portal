﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System.Net.Mail;
using NPCPortal.Jobs;
using NPCPortal.Jobs.Emails;
using System.Text.RegularExpressions;

namespace NPCPortal.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        public AccountController()
            : this(new UserManager<User>(new UserStore<User>(new NPCPortalDB())))
		{
        }

        public AccountController(UserManager<User> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<User> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
			if(String.IsNullOrWhiteSpace(returnUrl))
				returnUrl = Request.UrlReferrer != null ? new Uri(Request.UrlReferrer.ToString()).PathAndQuery : Url.Action("Index", "Home");
			else if(returnUrl == Request.Url.ToString())
				returnUrl = Url.Action("Index", "Home");
			ViewBag.ReturnUrl = returnUrl;
			ViewBag.RequireReCaptcha = RequireReCaptcha();
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Recaptcha.RecaptchaControlMvc.CaptchaValidator]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl, bool captchaValid, string captchaErrorMessage)
		{
			if (RequireReCaptcha() && !captchaValid 
				&& NPCPortalWeb.ReCaptchaSettings != null && !String.IsNullOrEmpty(NPCPortalWeb.ReCaptchaSettings.PublicKey))
				ModelState.AddModelError("", captchaErrorMessage);

            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
					await SignInAsync(user, model.RememberMe);
					ResetLoginAttempts();
                    return RedirectToLocal(returnUrl);
                }
                else
                {
					Log.Debug("Failed login attempt for UserName: \"{0}\"", model.UserName);

					IncrementLoginAttempts();

                    ModelState.AddModelError("", "Invalid username or password.");
                }
			}

			ViewBag.RequireReCaptcha = RequireReCaptcha();

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
		[Recaptcha.RecaptchaControlMvc.CaptchaValidator]
		public async Task<ActionResult> Register(RegisterViewModel model, bool captchaValid, string captchaErrorMessage)
		{
			ValidateExtendedRegistrationForm(model, captchaValid, captchaErrorMessage);

            if (ModelState.IsValid)
            {
                var user = new User() { UserName = model.UserName, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
					SaveRegistrationQuestionAnswers(user.Id);

                    await SignInAsync(user, isPersistent: true);

					SendValidationEmail(user, user.Email);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
				: message == ManageMessageId.EmailChanged ? "Your email has been changed."
				: message == ManageMessageId.EmailValidated ? "Your new email has been validated."
				: message == ManageMessageId.ValidationSent ? "A validation email has been sent. You must validate the email address before it can be used. Please check your email (and maybe your spam folder)."
				: "";
			ViewBag.ErrorMessage = message == ManageMessageId.Error ? "An error has occurred."
				: message == ManageMessageId.EmailAlreadyExists ? "The email you specified belongs to another user."
				: "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
			string userId = User.Identity.GetUserId();
			var user = db.Users.Single(u => u.Id == userId);
						
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

			var externalIdentity = await AuthenticationManager.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);
			var email = externalIdentity.FindFirstValue(ClaimTypes.Email);

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: true);
                return RedirectToLocal(returnUrl);
            }
			else if (db.Users.Any(u => u.Email.ToLower() == email.ToLower())) //If we already have their email on file. Log them in and associate the account.
			{
				user = db.Users.Single(u => u.Email.ToLower() == email.ToLower());
				await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
				await SignInAsync(user, isPersistent: true);
				if(!user.UserState.HasFlag(UserStates.EmailValidated))
				{
					user.UserState = user.UserState | UserStates.EmailValidated;
					db.SaveChanges();
				}
				return RedirectToLocal(returnUrl);
			}
			else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName, Email = email });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
			string userId = User.Identity.GetUserId();
			var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, userId);
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }

			//Check this OAuth email address against their account
			var user = db.Users.Single(u => u.Id == userId);
			var externalIdentity = await AuthenticationManager.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);
			string oAuthEmail = externalIdentity.FindFirstValue(ClaimTypes.Email);

			if (String.IsNullOrEmpty(user.Email)) //if the user doesn't have an email set, set it and mark it valid
			{
				if (!db.Users.Any(u => u.Email.ToLower() == oAuthEmail)) //Make sure they don't double up accounts
				{
					user.Email = oAuthEmail;
					user.UserState = user.UserState | UserStates.EmailValidated;
					db.SaveChanges();
				}
			}
			else //if they already have an email, check to see if we can validate it with this account
			{
				if (user.Email.ToLower() == oAuthEmail.ToLower() && !user.UserState.HasFlag(UserStates.EmailValidated))
				{
					user.UserState = user.UserState | UserStates.EmailValidated;
					db.SaveChanges();
				}
			}

			var result = await UserManager.AddLoginAsync(userId, loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
		[ValidateAntiForgeryToken]
		[Recaptcha.RecaptchaControlMvc.CaptchaValidator]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl, bool captchaValid, string captchaErrorMessage)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
			}

			ValidateExtendedRegistrationForm(model, captchaValid, captchaErrorMessage);

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new User() { UserName = model.UserName, Email = model.Email, UserState = UserStates.EmailValidated };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
				{
					SaveRegistrationQuestionAnswers(user.Id);

                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: true);
						
						if(!NPCPortalWeb.RegistrationSettings.RequireApproval)
							SendWelcomeMessage(user);

                        return RedirectToLocal(returnUrl);
                    }
                }

                AddErrors(result);
			}


			var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
			if (loginInfo != null)
				ViewBag.LoginProvider = loginInfo.Login.LoginProvider;

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

		[Authorize]
		public ActionResult Validate(string key)
		{
			string[] result = Security.Decrypt(key).Split(new char[] { '|' });

			if (result.Count() < 2)
				return RedirectToAction("Index", "Home");

			string userID = result[0];
			string email = result[1];

			if(User.Identity.GetUserId() != userID)
				return new HttpUnauthorizedResult();

			var user = db.Users.Single(u => u.Id == userID);

			//if this is first validation, mark it.
			if (!user.UserState.HasFlag(UserStates.EmailValidated))
			{
				user.UserState = user.UserState | UserStates.EmailValidated;
				JobManager.SubmitJob(new NewUserAlertEmail() { UserID = user.Id });

				if (!NPCPortalWeb.SiteSettings.OfType<RegistrationSettings>().First().RequireApproval)
					SendWelcomeMessage(user);
			}

			//if this is a change of email, switch it.
			if (user.Email != email)
				user.Email = email;

			db.SaveChanges();

			return RedirectToAction("Manage", new { message = ManageMessageId.EmailValidated });
		}
		
		[Authorize]
		public ActionResult ChangeEmail(string email)
		{
			string userID = User.Identity.GetUserId();
			var user = db.Users.Single(u => u.Id == userID);

			if (user.Email != email || !user.UserState.HasFlag(UserStates.EmailValidated))
			{
				if (db.Users.Any(u => u.Id != user.Id && u.Email.ToLower() == email.ToLower()))
					return RedirectToAction("Manage", "Account", new { message = ManageMessageId.EmailAlreadyExists });

				SendValidationEmail(user, email);
				return RedirectToAction("Manage", new { message = ManageMessageId.ValidationSent });
			}
			
			return RedirectToAction("Manage", new { message = ManageMessageId.EmailChanged });
		}
		
		[Authorize]
		public ActionResult Preferences(string id, bool editSuccess = false)
		{
			Type editObjectType = UserSetting.GetAllUserSettingsTypes().Single(t => t.Name.ToLower() == id.ToLower());

			string userID = User.Identity.GetUserId();
			var userSettings = db.UserSettings.Where(s => s.User.Id == userID).ToList();
			var userSetting = userSettings.SingleOrDefault(us => editObjectType.IsInstanceOfType(us));

			if (userSetting == null)
			{
				userSetting = (UserSetting)Activator.CreateInstance(UserSetting.GetAllUserSettingsTypes().Single(s => s.Name.ToLower() == id.ToLower()));
			}

			if (editSuccess)
				ViewBag.SuccessMessage = "Your settings have been saved.";

			return View(userSetting);
		}

		[Authorize]
		[HttpPost]
		public ActionResult SavePreferences(UserSetting setting)
		{
			if (setting.ID == 0)
			{
				string userID = User.Identity.GetUserId();
				setting.User = db.Users.Find(userID);
				db.UserSettings.Add(setting);
			}
			else
			{
				var entry = db.Entry(db.UserSettings.Single(s => s.ID == setting.ID));

				MergeDisconnectedEntity(entry.Entity, setting);
			}

			db.SaveChanges();

			return RedirectToAction("Preferences", new { id = setting.GetType().Name, editSuccess = true });
		}

		[AllowAnonymous]
		public ActionResult ForgotPassword()
		{
			return View();
		}

		[AllowAnonymous]
		[HttpPost]
		public ActionResult ForgotPassword(string username, string email)
		{
			var user = db.Users.SingleOrDefault(u => u.UserName == username.Trim() || u.Email == email.Trim());
			if (user == null)
				ViewBag.Error = "Sorry, couldn't find any users with that info.";
			else
			{
				if (!user.ForgotPasswordDate.HasValue || user.ForgotPasswordDate.Value.AddDays(1) < DateTime.UtcNow)
				{
					user.ForgotPasswordCode = Guid.NewGuid().ToString();
					user.ForgotPasswordDate = DateTime.UtcNow;
					db.SaveChanges();
				}
				SendForgotPasswordEmail(user);
				ViewBag.Success = "We have sent a password reset email to your email address. (You might have to check your spam folder)";
			}

			return View();
		}


		[AllowAnonymous]
		public ActionResult ResetForgottenPassword(string key)
		{
			key = Security.Decrypt(key);
			var user = db.Users.SingleOrDefault(u => u.ForgotPasswordCode != null && u.ForgotPasswordCode == key);
			if (user == null)
				return new HttpUnauthorizedResult();

			if (!user.ForgotPasswordDate.HasValue || user.ForgotPasswordDate.Value.AddDays(1) < DateTime.UtcNow) //enforce forgot password code expiration in email
			{
				user.ForgotPasswordCode = null;
				user.ForgotPasswordDate = null;
				db.SaveChanges();
				return new HttpUnauthorizedResult();
			}

			ViewBag.ForgotPasswordCode = key;

			return View(new ManageUserViewModel());
		}

		[AllowAnonymous]
		[HttpPost]
		public ActionResult ResetForgottenPassword(string key, ManageUserViewModel model)
		{
			var user = db.Users.SingleOrDefault(u => u.ForgotPasswordCode != null && u.ForgotPasswordCode == key);

			if (user == null || !user.ForgotPasswordDate.HasValue || user.ForgotPasswordDate.Value.AddDays(1) < DateTime.UtcNow)
				return new HttpUnauthorizedResult();

			ModelState state = ModelState["OldPassword"];
			if (state != null)
			{
				state.Errors.Clear();
			}

			if (ModelState.IsValid)
			{
				if (UserManager.HasPassword(user.Id))
					UserManager.RemovePassword(user.Id);

				IdentityResult result = UserManager.AddPassword(user.Id, model.NewPassword);

				if (result.Succeeded)
				{
					user.ForgotPasswordCode = null;
					user.ForgotPasswordDate = null;
					db.SaveChanges();

					//Sign them in, the poor things.
					var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
					AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);

					return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
				}
				else
				{
					AddErrors(result);
				}
			}

			ViewBag.ForgotPasswordCode = key;

			return View(model);
		}

		public void ValidateExtendedRegistrationForm(ExtendedRegistrationViewModel model, bool captchaValid, string captchaErrorMessage)
		{
			if (!String.IsNullOrEmpty(NPCPortalWeb.RegistrationSettings.RegistrationPassword))
			{
				if (Request["RegistrationPassword"] != NPCPortalWeb.RegistrationSettings.RegistrationPassword)
					ModelState.AddModelError("RegistrationPassword", "Invalid Registration Password.");
			}

			if (db.Users.Any(u => u.Email.ToLower() == model.Email.ToLower()))
				ModelState.AddModelError("", "This email address is already in use.");

			if (NPCPortalWeb.RegistrationSettings.CapchaEnabled && !captchaValid)
				ModelState.AddModelError("", "Invalid captcha value.");

			//Check the registrants IP address against known bad IP's
			var ipAddress = Request.GetIPAddress();
			if (db.Users.Any(u => u.IPAddress == ipAddress
				&& ((u.UserState & UserStates.Banned) != 0 || (u.UserState & UserStates.Spammer) != 0 || (u.UserState & UserStates.Rejected) != 0)))
				ModelState.AddModelError("", "There was an error processing your request. Please try again later.");

			foreach(var question in NPCPortalWeb.RegistrationSettings.RegistrationQuestions)
			{
				if(question.Required)
				{
					if(question.Type == RegistrationQuestionTypes.TextBox || question.Type == RegistrationQuestionTypes.TextArea)
					{
						string answer = Request[question.ID.ToString()].Trim();
						if (String.IsNullOrEmpty(answer))
						{
							ModelState.AddModelError("", "The " + question.Text + " field is required.");
							continue;
						}

						if (question.Answers.Count > 0 && !question.Answers.Any(a => a.IsCorrect && a.AnswerText.ToLower() == answer.ToLower()))
							ModelState.AddModelError("", question.Text + " was not correct");
					}
					else if(question.Type == RegistrationQuestionTypes.DropDownList || question.Type == RegistrationQuestionTypes.MultiSelect)
					{
						string[] answers = Request[question.ID.ToString()].Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

						if (answers.Length == 0)
						{
							ModelState.AddModelError("", "The " + question.Text + " field is required.");
							continue;
						}

						if (answers.Any(a => question.Answers.Any(qa => qa.ID.ToString() == a && !qa.IsCorrect)))
							ModelState.AddModelError("", question.Text + " was not correct");
					}
					else if(question.Type == RegistrationQuestionTypes.CheckBox)
					{
						string answer = Request[question.ID.ToString()];

						if (String.IsNullOrEmpty(answer) || answer.Trim() == "false" )
							ModelState.AddModelError("", "The " + question.Text + " field is required.");
					}
				}
			}

			//If it's not valid, repackage up the user's answers to display them again
			if(!ModelState.IsValid)
			{
				foreach (var question in NPCPortalWeb.RegistrationSettings.RegistrationQuestions)
				{
					model.RegistrationAnswers.Add(question.ID.ToString(), Request[question.ID.ToString()]);
				}
			}
		}

		public void SaveRegistrationQuestionAnswers(string userID)
		{
			try
			{
				var user = db.Users.Find(userID);
				foreach (var question in NPCPortalWeb.RegistrationSettings.RegistrationQuestions)
				{
					var response = new RegistrationAnswer()
						{
							Question = question.Text,
							Answer = !String.IsNullOrEmpty(Request[question.ID.ToString()]) ? Request[question.ID.ToString()].Trim() : "",
							User = user
						};

					if (question.Type == RegistrationQuestionTypes.CheckBox)
						response.Answer = response.Answer != "false" ? "Yes" : "No";
					else if(question.Type == RegistrationQuestionTypes.DropDownList || question.Type == RegistrationQuestionTypes.MultiSelect)
					{
						string[] answers = response.Answer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
						response.Answer = String.Join(", ", question.Answers.Where(a => answers.Contains(a.ID.ToString())).Select(a => a.AnswerText).ToArray());
					}

					db.RegistrationAnswers.Add(response);
				}

				db.SaveChanges();
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Failed to save registration answers for user: {0}", userID);
			}
		}

        public void ResetLoginAttempts()
		{
			string ipAddress = Request.GetIPAddress();
			var attemptLog = db.LoginAttempts.SingleOrDefault(a => a.IPAddress == ipAddress);
			if(attemptLog != null)
			{
				attemptLog.FailedCount = 0;
				db.SaveChanges();
			}
        }

		public bool RequireReCaptcha()
		{
			string ipAddress = Request.GetIPAddress();
			var attempt = db.LoginAttempts.SingleOrDefault(a => a.IPAddress == ipAddress);
			if (attempt != null && attempt.FailedCount >= 5)
				return true;
			else
				return false;
		}

        public void IncrementLoginAttempts()
        {
            string ipAddress = Request.GetIPAddress();
            var attempt = db.LoginAttempts.SingleOrDefault(a => a.IPAddress == ipAddress);
            if (attempt == null)
            {
                attempt = new LoginAttempt() { IPAddress = ipAddress };
                db.LoginAttempts.Add(attempt);
			}

			attempt.FailedCount++;
			attempt.LastAttempt = DateTime.UtcNow;

            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

		public enum ManageMessageId
		{
			ChangePasswordSuccess,
			SetPasswordSuccess,
			RemoveLoginSuccess,
			Error,
			EmailChanged,
			ValidationSent,
			EmailValidated,
			EmailAlreadyExists
		}

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            else
                return RedirectToAction("Index", "Home");
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null) { }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

		public void SendValidationEmail(User user, string email)
		{
			JobManager.SubmitJob(new ValidationEmail() { UserID = user.Id, EmailAddress = email });
		}

		public void SendWelcomeMessage(User user)
		{
			JobManager.SubmitJob(new WelcomeEmail() { UserID = user.Id });
		}

		public void SendForgotPasswordEmail(User user)
		{
			JobManager.SubmitJob(new ForgotPasswordEmail() { UserID = user.Id });
		}
        #endregion
    }
}
﻿using NPCPortal.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Controllers
{
	[ValidateInput(false)]
    public class BaseController : Controller
	{
		internal NPCPortalDB db = new NPCPortalDB();

		public User CurrentUser { get; private set; }
		
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			ViewBag.ForumCategories = db.ForumCategories;
			ViewBag.MenuItems = db.NavMenuItems.Where(i => i.Parent == null).OrderBy(i => i.Title);

			if (User.Identity.IsAuthenticated)
			{
				this.CurrentUser = db.Users.Single(u => u.UserName == User.Identity.Name);
				this.CurrentUser.LastOnline = DateTime.UtcNow;
				db.SaveChanges();
				ViewBag.CurrentUser = this.CurrentUser;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
		public string RenderPartialToString(string viewName, ViewDataDictionary viewData)
		{
			string error;
			return RenderViewToString(viewName, viewData, out error, true);
		}

		public string RenderViewToString(string viewName, ViewDataDictionary viewData, out string error, bool isPartial = false)
		{
			error = String.Empty;
			if (string.IsNullOrEmpty(viewName))
				viewName = ControllerContext.RouteData.GetRequiredString("action");

			if(ControllerContext == null) //If we're calling from a job, grab the original context
			{
				var routeData = new System.Web.Routing.RouteData();
				routeData.Values.Add("controller", "Home");
				routeData.Values.Add("action", "Index");
				ControllerContext = new System.Web.Mvc.ControllerContext()
				{
					RouteData = routeData,
					HttpContext = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current)
				};
			}

			var alreadyExistingModel = ViewData.Model;
			try
			{
				using (StringWriter sw = new StringWriter())
				{
					ViewEngineResult viewResult = isPartial ? ViewEngines.Engines.FindPartialView(ControllerContext, viewName)
						: ViewEngines.Engines.FindView(ControllerContext, viewName, String.Empty);
					if (viewResult.View == null)
						throw new Exception("Missing view: " + viewName);

					ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, viewData, new TempDataDictionary(), sw);
					viewResult.View.Render(viewContext, sw);

					return sw.GetStringBuilder().ToString();
				}
			}
			catch(Exception ex)
			{
				error = ex.Message;
			}
			return null;
		}

		/// <summary>
		/// This method is used when attempting to update the database version of an entity with an entity that was bound from the view. (Note, all properties must be bound for this to work)
		/// </summary>
		/// <param name="original"></param>
		/// <param name="disconnected"></param>
		public void MergeDisconnectedEntity(object original, object disconnected)
		{
			var propertyPairs = from op in original.GetType().GetProperties().Where(p => p.CanWrite)
								join dp in disconnected.GetType().GetProperties().Where(p => p.CanWrite) on op.Name equals dp.Name
								select new { OriginalProperty = op, DisconnectedProperty = dp };

			foreach (var pair in propertyPairs)
			{
				//If is collection, compare each item
				if (pair.OriginalProperty.PropertyType != typeof(string)
						&& (pair.OriginalProperty.PropertyType.IsArray
							|| pair.OriginalProperty.PropertyType.GetInterfaces().Any(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(IEnumerable<>))))
				{
					List<object> toDelete = new List<object>();
					foreach (var originalItem in (IEnumerable)pair.OriginalProperty.GetValue(original))
					{
						bool originalfound = false;
						foreach (var disconnectedItem in (IEnumerable)pair.DisconnectedProperty.GetValue(disconnected))
						{
							if ((int)originalItem.GetType().GetProperty("ID").GetValue(originalItem) == (int)disconnectedItem.GetType().GetProperty("ID").GetValue(disconnectedItem))
							{
								originalfound = true;
								MergeDisconnectedEntity(originalItem, disconnectedItem);
							}
						}

						if (!originalfound)
							toDelete.Add(originalItem);
					}

					foreach (var deleteItem in toDelete)
					{
						db.Entry(deleteItem).State = System.Data.Entity.EntityState.Deleted;
						((IList)pair.OriginalProperty.GetValue(original)).Remove(deleteItem);
					}

					List<object> toAdd = new List<object>();
					foreach (var disconnectedItem in (IEnumerable)pair.DisconnectedProperty.GetValue(disconnected))
					{
						bool isNew = true;
						foreach (var originalItem in (IEnumerable)pair.OriginalProperty.GetValue(original))
						{
							if ((int)originalItem.GetType().GetProperty("ID").GetValue(originalItem) == (int)disconnectedItem.GetType().GetProperty("ID").GetValue(disconnectedItem))
							{
								isNew = false;
							}
						}

						if (isNew)
							toAdd.Add(disconnectedItem);
					}

					foreach (var addItem in toAdd)
					{
						((IList)pair.OriginalProperty.GetValue(original)).Add(addItem);
					}
				}
				//If single navigation property
				else if (pair.OriginalProperty.GetValue(original) != null && ObjectContext.GetObjectType(pair.OriginalProperty.PropertyType) != pair.OriginalProperty.PropertyType)
				{
					MergeDisconnectedEntity(pair.OriginalProperty.GetValue(original), pair.DisconnectedProperty.GetValue(disconnected));
				}
				else if (pair.OriginalProperty.Name != "ID")
				{
					pair.OriginalProperty.SetValue(original, pair.DisconnectedProperty.GetValue(disconnected));
				}
			}
		}
	}
}
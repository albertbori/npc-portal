﻿using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NPCPortal.Models.ViewModels;
using NPCPortal.Jobs;

namespace NPCPortal.Controllers
{
    public class ContentController : BaseController
	{
		[Authorize]
		public EmptyResult Like(string userID, int postID)
		{
			var user = db.Users.Single(u => u.Id == userID);
			var content = db.Content.Single(c => c.ID == postID);

			user.LikedContent.Add(content);

			db.SaveChanges();

			JobManager.SubmitJob(new ClearForumStatsCache() { ContentID = postID });

			return new EmptyResult();
		}

		[Authorize]
		public EmptyResult Dislike(string userID, int postID)
		{
			var user = db.Users.Single(u => u.Id == userID);
			var content = db.Content.Single(c => c.ID == postID);

			user.LikedContent.Remove(user.LikedContent.Single(c => c.ID == postID));

			db.SaveChanges();

			JobManager.SubmitJob(new ClearForumStatsCache() { ContentID = postID });

			return new EmptyResult();
		}

		[HttpPost]
		public ContentResult Preview(string markdown)
		{
			return new ContentResult() { ContentType = "text/html", Content = markdown.DisplayMarkdown() };
		}
	}
}
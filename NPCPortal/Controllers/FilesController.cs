﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using Microsoft.AspNet.Identity;
using NPCPortal.Utilities;
using NPCPortal.Models.ViewModels;

namespace NPCPortal.Controllers
{
    public class FilesController : BaseController
    {
		[NPCAuthorize(PermissionTypes.Upload)]
        public ActionResult Index(string userID)
        {
			var files = db.Files.Where(f => !f.FileType.StartsWith("image"));
			if(userID != null)
			{
				if (User.Identity.GetUserId() != userID && !User.CanDoAny(PermissionTypes.EditPosts, PermissionTypes.DeletePosts))
					return new HttpUnauthorizedResult();

				files = files.Where(f => f.Author.Id == userID);
			}

			var pager = new Pager(files.Count());
			ViewBag.Pager = pager;

			files = files.OrderBy(f => f.Title ?? f.FileName).Skip((pager.CurrentPage - 1) * pager.RecordsPerPage).Take(pager.RecordsPerPage);

			return View(files);
        }

		[NPCAuthorize(PermissionTypes.ViewGallery)]
		public ActionResult Gallery()
		{
			var images = db.Files.Where(f => f.FileType.StartsWith("image"));

			var pager = new Pager(images.Count());
			pager.RecordsPerPage = 256;
			ViewBag.Pager = pager;

			images = images.OrderByDescending(i => i.DateCreated).Skip((pager.CurrentPage - 1) * pager.RecordsPerPage).Take(pager.RecordsPerPage);

			return View(images);
		}

		[NPCAuthorize(PermissionTypes.ViewGallery)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

		[NPCAuthorize(PermissionTypes.Upload)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			File file = db.Files.Find(id);
			if (User.Identity.GetUserId() != file.Author.Id && !User.CanDo(PermissionTypes.EditPosts))
				return new HttpUnauthorizedResult();

            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

		[NPCAuthorize(PermissionTypes.Upload)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,ModerationStatus,DateCreated,Title,Description,FileName,FileSize,FileType")] File file)
		{
			var existingFile = db.Files.Single(f => f.ID == file.ID);
			if (User.Identity.GetUserId() != existingFile.Author.Id && !User.CanDo(PermissionTypes.EditPosts))
				return new HttpUnauthorizedResult();

            if (ModelState.IsValid)
            {
				existingFile.Title = file.Title;
				existingFile.Description = file.Description;
                db.SaveChanges();
				return RedirectToAction("Index", new { userID = Request["userID"] });
            }
            return View(file);
        }

		[NPCAuthorize(PermissionTypes.Upload)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			File file = db.Files.Find(id);
			if (User.Identity.GetUserId() != file.Author.Id && !User.CanDo(PermissionTypes.EditPosts))
				return new HttpUnauthorizedResult();
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

		[NPCAuthorize(PermissionTypes.Upload)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
			File file = db.Files.Find(id);
			if (User.Identity.GetUserId() != file.Author.Id && !User.CanDo(PermissionTypes.EditPosts))
				return new HttpUnauthorizedResult();
			file.Delete(db);
			return RedirectToAction("Index", new { userID = Request["userID"] });
        }

		[NPCAuthorize(PermissionTypes.Upload)]
		public ActionResult New()
		{
			return View();
		}

		[NPCAuthorize(PermissionTypes.Upload)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult New(File file)
		{
			foreach (string fileKey in Request.Files)
			{
				var uploadedFile = Request.Files[fileKey];
				if (uploadedFile.ContentLength > 0)
				{
					string fileName = uploadedFile.ContentType.StartsWith("image") ? FileStore.SaveNewImage(uploadedFile) : FileStore.SaveNewFile(uploadedFile);

					string userID = User.Identity.GetUserId();
					var author = db.Users.Single(u => u.Id == userID);

					File newFile = new File()
					{
						FileName = fileName,
						Title = file.Title,
						Description = file.Description,
						FileSize = uploadedFile.ContentLength,
						FileType = uploadedFile.ContentType,
						Author = author
					};
					db.Files.Add(newFile);
					db.SaveChanges();

					return RedirectToAction("Details", new { id = newFile.ID });
				}
			}

			ModelState.AddModelError("", "Please select a file to upload");

			return View();
		}

		[NPCAuthorize(PermissionTypes.Upload)]
		public JsonResult Upload()
		{
			string userID = User.Identity.GetUserId();
			var author = db.Users.Single(u => u.Id == userID);
			Dictionary<HttpPostedFileBase, File> newFiles = new Dictionary<HttpPostedFileBase, File>();

			foreach (string fileKey in Request.Files)
			{
				var uploadedFile = Request.Files[fileKey];
				if (uploadedFile.ContentLength > 0)
				{
					string fileName = uploadedFile.ContentType.StartsWith("image") ? FileStore.SaveNewImage(uploadedFile) : FileStore.SaveNewFile(uploadedFile);
					File newFile = new File()
					{
						FileName = fileName,
						FileSize = uploadedFile.ContentLength,
						FileType = uploadedFile.ContentType,
						Author = author
					};
					db.Files.Add(newFile);
					newFiles.Add(uploadedFile, newFile);
				}
			}

			db.SaveChanges();

			return Json(new
			{
				files = newFiles.Select(f => new
				{
					id = f.Value.ID,
					name = f.Key.FileName,
					url = FileStore.GetImageURL(f.Value.FileName)
				})
			});
		}
    }
}

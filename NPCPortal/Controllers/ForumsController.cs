﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using NPCPortal.Utilities;
using Microsoft.AspNet.Identity;
using NPCPortal.Jobs;
using NPCPortal.Models.ViewModels;

namespace NPCPortal.Controllers
{
    public class ForumsController : BaseController
    {
		[NPCAuthorize(PermissionTypes.ViewForums)]
        public ActionResult Index(int? id)
        {
			var forumCategories = db.ForumCategories.Where(c => c.ParentCategory == null);

			if(id != null)
			{
				forumCategories = db.ForumCategories.Where(c => c.ID == id);
			}

			//populate unread counts for current user
			Dictionary<int, int> unreadCounts = new Dictionary<int, int>();
			if(User.Identity.IsAuthenticated)
			{
				var user = db.Users.Find(CurrentUser.Id);
				if (user.ViewedContent.Count > 0)
				{
					foreach (var forum in db.Forums)
					{
						if(!user.ViewedContent.Any(vc => vc.Content is Discussion && (vc.Content as Discussion).Forum.ID == forum.ID))
							continue;

						//count posts for this forum, newer than 30 days ago, that I haven't seen at all (or yet)

						int unreadDiscussions = forum.Discussions
							.Where(d => d.DateCreated > DateTime.UtcNow.AddMonths(-1) && !user.ViewedContent.Any(vc => vc.Content.ID == d.ID))
							.Sum(d => d.AllReplies.Count + 1);
						int unreadReplies = user.ViewedContent
							.Where(vc => vc.Content is Discussion && (vc.Content as Discussion).Forum.ID == forum.ID)
							.Sum(vc => (vc.Content as Discussion).AllReplies.Count(r => r.DateCreated > DateTime.UtcNow.AddMonths(-1) && r.DateCreated > vc.ViewedOn));

						unreadCounts.Add(forum.ID, unreadDiscussions + unreadReplies);
					}
				}
			}
			ViewBag.UnreadCounts = unreadCounts;

			var stats = Cache.GetByID<ForumCategoryStats>(id != null ? id.Value : 0);
			if (stats == null)
			{
				stats = new ForumCategoryStats();
				if (id != null)
				{
					var topContributors = db.ForumPosts
					.Where(p => (p is Discussion && (p as Discussion).Forum.Category.ID == id) || (p is Reply && (p as Reply).Discussion.Forum.Category.ID == id))
					.GroupBy(p => p.Author)
					.Select(g => new { User = g.Key, Posts = g.Count() })
					.OrderByDescending(g => g.Posts)
					.Where(g => g.Posts > 0)
					.Take(5)
					.ToDictionary(k => k.User, v => v.Posts);

					stats.TopContributors = topContributors.Select(c => new ForumContributor() { UserID = c.Key.Id, Value = c.Value }).ToList();

					var popularContributors = db.ForumPosts
						.Where(p => (p is Discussion && (p as Discussion).Forum.Category.ID == id) || (p is Reply && (p as Reply).Discussion.Forum.Category.ID == id))
						.GroupBy(p => p.Author)
						.Select(g => new { User = g.Key, Likes = g.Sum(p => p.UsersLikeThis.Count()) })
						.OrderByDescending(g => g.Likes)
						.Where(g => g.Likes > 0)
						.Take(5)
						.ToDictionary(k => k.User, v => v.Likes);

					stats.PopularContributors = popularContributors.Select(c => new ForumContributor() { UserID = c.Key.Id, Value = c.Value }).ToList();
				}
				else
				{
					var topContributors = db.ForumPosts
						.GroupBy(p => p.Author)
						.Select(g => new { User = g.Key, Posts = g.Count() })
						.OrderByDescending(g => g.Posts)
						.Where(g => g.Posts > 0)
						.Take(5)
						.ToDictionary(k => k.User, v => v.Posts);

					stats.TopContributors = topContributors.Select(c => new ForumContributor() { UserID = c.Key.Id, Value = c.Value }).ToList();

					var popularContributors = db.ForumPosts
						.GroupBy(p => p.Author)
						.Select(g => new { User = g.Key, Likes = g.Sum(p => p.UsersLikeThis.Count()) })
						.OrderByDescending(g => g.Likes)
						.Where(g => g.Likes > 0)
						.Take(5)
						.ToDictionary(k => k.User, v => v.Likes);

					stats.PopularContributors = popularContributors.Select(c => new ForumContributor() { UserID = c.Key.Id, Value = c.Value }).ToList();
				}
				Cache.SetByID(id != null ? id.Value : 0, stats);
			}

			ViewBag.ForumStats = stats;

			return View(forumCategories);
        }

		[NPCAuthorize(PermissionTypes.ViewForums)]
		public ActionResult Forum(int id)
		{
			var forum = db.Forums.Single(f => f.ID == id);

			if ((!String.IsNullOrEmpty(forum.ViewRole) && !User.IsInRole(forum.ViewRole))
				|| (forum.MembersOnly && !User.Identity.IsAuthenticated))
				return new HttpUnauthorizedResult();			

			var stats = Cache.GetByID<ForumStats>(id);
			if (stats == null)
			{
				stats = new ForumStats();
				var topContributors = db.ForumPosts
					.Where(p => (p is Discussion && (p as Discussion).Forum.ID == id) || (p is Reply && (p as Reply).Discussion.Forum.ID == id))
					.GroupBy(p => p.Author)
					.Select(g => new { User = g.Key, Posts = g.Count() })
					.OrderByDescending(g => g.Posts)
					.Where(g => g.Posts > 0)
					.Take(5)
					.ToDictionary(k => k.User, v => v.Posts);

				stats.TopContributors = topContributors.Select(c => new ForumContributor() { UserID = c.Key.Id, Value = c.Value }).ToList();

				var popularContributors = db.ForumPosts
					.Where(p => (p is Discussion && (p as Discussion).Forum.ID == id) || (p is Reply && (p as Reply).Discussion.Forum.ID == id))
					.GroupBy(p => p.Author)
					.Select(g => new { User = g.Key, Likes = g.Sum(p => p.UsersLikeThis.Count()) })
					.OrderByDescending(g => g.Likes)
					.Where(g => g.Likes > 0)
					.Take(5)
					.ToDictionary(k => k.User, v => v.Likes);

				stats.PopularContributors = popularContributors.Select(c => new ForumContributor() { UserID = c.Key.Id, Value = c.Value }).ToList();

				Cache.SetByID(id, stats);
			}

			ViewBag.Forum = forum;
			ViewBag.ForumStats = stats;

			return View(forum);
		}

		[NPCAuthorize(PermissionTypes.ViewForums)]
		public ActionResult Discussion(int id)
		{
			var discussion = db.ForumPosts.Find(id) as Discussion;

			if (discussion == null) //oopsies! the post no longer exists for some reason!
				return RedirectToAction("Index", "Home");

			if ((!String.IsNullOrEmpty(discussion.Forum.ViewRole) && !User.IsInRole(discussion.Forum.ViewRole))
				|| (discussion.Forum.MembersOnly && !User.Identity.IsAuthenticated))
				return new HttpUnauthorizedResult();

			if (User.Identity.IsAuthenticated)
			{
				var user = db.Users.Find(User.Identity.GetUserId());
				//Updated user's viewed content
				var viewedDiscussion = user.ViewedContent.SingleOrDefault(v => v.Content.ID == id);
				if (viewedDiscussion != null)
				{
					viewedDiscussion.ViewedOn = DateTime.UtcNow;
					viewedDiscussion.ViewCount++;
				}
				else
					user.ViewedContent.Add(new ViewedContent() { Content = discussion });

				//If the user is not the author, increment view count
				if(user.Id != discussion.Author.Id && !Request.Browser.Crawler)
					discussion.ViewCount++;
			}
			else //if the user's not authenticated, increment view count
				discussion.ViewCount++;
			db.SaveChanges();

			ViewBag.Forum = discussion.Forum;

			ViewBag.AllForums = db.Forums.OrderBy(f => f.Name);

			return View(discussion);
		}

		[NPCAuthorize(PermissionTypes.Flag)]
		public PartialViewResult FlagPost(int postID)
		{
			var post = db.ForumPosts.Single(p => p.ID == postID);
			return PartialView("_FlagPost", post);
		}

		[HttpPost]
		[NPCAuthorize(PermissionTypes.Flag)]
		[ValidateAntiForgeryToken]
		public ActionResult ConfirmFlagPost(int postID)
		{
			var post = db.ForumPosts.Single(p => p.ID == postID);

			post.ModerationStatus = ModerationStatuses.Flagged;

			db.SaveChanges();

			int discussionID = post is Discussion ? ((Discussion)post).ID : ((Reply)post).Discussion.ID;

			return RedirectToAction("Discussion", new { id = discussionID });
		}

		[NPCAuthorize(PermissionTypes.DeletePosts)]
		public PartialViewResult DeletePost(int postID)
		{
			var post = db.ForumPosts.Single(p => p.ID == postID);
			return PartialView("_DeletePost", post);
		}

		[NPCAuthorize(PermissionTypes.DeletePosts)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult ConfirmDeletePost(int postID)
		{
			var post = db.ForumPosts.Single(p => p.ID == postID);
			bool isDiscussion = post is Discussion;
			int redirectID = isDiscussion ? ((Discussion)post).Forum.ID : ((Reply)post).Discussion.ID;

			post.Delete(db);

			if(isDiscussion)
				return RedirectToAction("Forum", new { id = redirectID });
			else
				return RedirectToAction("Discussion", new { id = redirectID });

		}

		[NPCAuthorize(new[] { PermissionTypes.CreatePosts, PermissionTypes.EditPosts })]
		public PartialViewResult Reply(int replyToID)
		{
			var inReplyTo = db.ForumPosts.Single(p => p.ID == replyToID);
			var forum = inReplyTo is Discussion ? ((Discussion)inReplyTo).Forum : ((Reply)inReplyTo).Discussion.Forum;

			if ((!String.IsNullOrEmpty(forum.PostRole) && User.IsInRole(forum.PostRole))
				|| (forum.MembersOnly && !User.Identity.IsAuthenticated))
				throw new UnauthorizedAccessException();

			var reply = new Reply()
			{
				InReplyTo = inReplyTo
			};

			return PartialView("_EditReply", reply);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[NPCAuthorize(new[] { PermissionTypes.CreatePosts, PermissionTypes.EditPosts })]
		public ActionResult SaveReply(Reply reply, int inReplyToID)
		{
			var inReplyTo = db.ForumPosts.Single(p => p.ID == inReplyToID);
			var forum = inReplyTo is Discussion ? ((Discussion)inReplyTo).Forum : ((Reply)inReplyTo).Discussion.Forum;

			if ((!String.IsNullOrEmpty(forum.PostRole) && !User.IsInRole(forum.PostRole))
				|| (forum.MembersOnly && !User.Identity.IsAuthenticated))
				throw new UnauthorizedAccessException("You do not have permission to access this resource.");

			bool isNew = reply.ID == 0;
			if (isNew)
			{
				reply.Author = db.Users.Single(u => u.UserName == User.Identity.Name);
				reply.InReplyTo = inReplyTo;
				reply.Discussion = inReplyTo is Discussion ? (Discussion)inReplyTo : ((Reply)inReplyTo).Discussion;
				db.ForumPosts.Add(reply);
			}
			else
			{
				var editReply = db.ForumPosts.OfType<Reply>().Single(r => r.ID == reply.ID);
				editReply.Text = reply.Text;
				reply = editReply;
			}

			SaveUploadedFiles(reply);

			db.SaveChanges();

			if (isNew)
				JobManager.SubmitJob(new ProcessNewReply() { ReplyID = reply.ID });

			if (!String.IsNullOrEmpty(Request["watchDiscussion"]) && Request["watchDiscussion"] != "false")
			{
				var user = db.Users.Find(User.Identity.GetUserId());
				if(!user.WatchedDiscussions.Any(wd => wd.ID == reply.Discussion.ID))
				{
					user.WatchedDiscussions.Add(reply.Discussion);
					db.SaveChanges();
				}
			}

			return Redirect(Url.Action("Discussion", new { id = reply.Discussion.ID }) + "#" + reply.ID);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[NPCAuthorize(new[] { PermissionTypes.CreatePosts, PermissionTypes.EditPosts })]
		public ActionResult SaveDiscussion(Discussion discussion, int forumID)
		{
			var forum = db.Forums.Find(forumID);

			if ((!String.IsNullOrEmpty(forum.PostRole) && !User.IsInRole(forum.PostRole))
				|| (forum.MembersOnly && !User.Identity.IsAuthenticated))
				return new HttpUnauthorizedResult();

			bool isNew = discussion.ID == 0;
			if (isNew)
			{
				if (!User.CanDo(PermissionTypes.CreatePosts))
					return new HttpUnauthorizedResult();

				discussion.Forum = forum;
				discussion.Author = db.Users.Single(u => u.UserName == User.Identity.Name);
				db.ForumPosts.Add(discussion);
			}
			else
			{
				var editDiscussion = db.ForumPosts.OfType<Discussion>().Single(d => d.ID == discussion.ID);

				if (User.Identity.GetUserId() != editDiscussion.Author.Id && !User.CanDo(PermissionTypes.EditPosts))
					return new HttpUnauthorizedResult();

				editDiscussion.Title = discussion.Title;
				editDiscussion.Text = discussion.Text;
				editDiscussion.IsFeatured = discussion.IsFeatured;
				editDiscussion.IsLocked = discussion.IsLocked;
				editDiscussion.IsSticky = discussion.IsSticky;
				discussion = editDiscussion;
			}

			SaveUploadedFiles(discussion);

			db.SaveChanges();

			if (isNew)
				JobManager.SubmitJob(new ProcessNewDiscussion() { ContentID = discussion.ID });

			if (!String.IsNullOrEmpty(Request["watchDiscussion"]) && Request["watchDiscussion"] != "false")
			{
				var user = db.Users.Find(User.Identity.GetUserId());
				if (!user.WatchedDiscussions.Any(wd => wd.ID == discussion.ID))
				{
					user.WatchedDiscussions.Add(discussion);
					db.SaveChanges();
				}
			}

			return RedirectToAction("Discussion", new { id = discussion.ID });
		}
		
		[NPCAuthorize(new [] { PermissionTypes.CreatePosts, PermissionTypes.EditPosts })]
		public PartialViewResult EditPost(int postID)
		{
			var post = db.ForumPosts.Single(p => p.ID == postID);

			if (User.Identity.GetUserId() != post.Author.Id && !User.CanDo(PermissionTypes.EditPosts))
				throw new UnauthorizedAccessException();

			if (post is Reply)
				return PartialView("_EditReply", (Reply)post);
			else
			{
				ViewBag.Forum = ((Discussion)post).Forum;
				return PartialView("_EditDiscussion", (Discussion)post);
			}
		}

		[NPCAuthorize(PermissionTypes.Upload)]
		private void SaveUploadedFiles(ForumPost post)
		{
			if (String.IsNullOrEmpty(Request["UploadedFileIDs"]))
				return;

			var fileIDs = Request["UploadedFileIDs"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => Convert.ToInt32(s));

			foreach(int fileID in fileIDs)
			{
				post.Attachments.Add(db.Files.Single(f => f.ID == fileID));
			}

			db.SaveChanges();
		}

		[NPCAuthorize(PermissionTypes.MoveDiscussions)]
		public ActionResult MoveDiscussion(int id, int forumID)
		{
			var discussion = db.ForumPosts.Find(id) as Discussion;
			discussion.Forum = db.Forums.Find(forumID);
			db.SaveChanges();

			return RedirectToAction("Discussion", new { id = id });
		}

		[Authorize]
		public EmptyResult WatchDiscussion(int id)
		{
			var discussion = db.ForumPosts.Find(id) as Discussion;
			if(discussion != null)
			{
				var user = db.Users.Find(User.Identity.GetUserId());
				user.WatchedDiscussions.Add(discussion);
				db.SaveChanges();
			}

			return new EmptyResult();
		}

		[Authorize]
		public EmptyResult UnWatchDiscussion(int id)
		{
			var discussion = db.ForumPosts.Find(id) as Discussion;
			if (discussion != null)
			{
				var user = db.Users.Find(User.Identity.GetUserId());
				user.WatchedDiscussions.Remove(discussion);
				db.SaveChanges();
			}

			return new EmptyResult();
		}
    }
}

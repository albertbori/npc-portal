﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NPCPortal.Models.ViewModels;

namespace NPCPortal.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var featuredDiscussions = db.ForumPosts.OfType<Discussion>().Where(d => d.IsFeatured);

			if (User.Identity.IsAuthenticated)
			{
				var userRoles = db.Users.Find(User.Identity.GetUserId()).Roles.Select(r => r.Role.Name.ToLower());

				featuredDiscussions = featuredDiscussions
					.Where(d => (String.IsNullOrEmpty(d.Forum.ViewRole) || userRoles.Contains(d.Forum.ViewRole))
						&& (String.IsNullOrEmpty(d.Forum.Category.RestrictedToRole) || userRoles.Contains(d.Forum.Category.RestrictedToRole)));
			}
			else
			{
				featuredDiscussions = featuredDiscussions.Where(d => !d.Forum.MembersOnly);
			}

			Pager pager = new Pager(featuredDiscussions.Count());
			pager.RecordsPerPage = 10;
			ViewBag.Pager = pager;
			featuredDiscussions = featuredDiscussions.OrderByDescending(d => d.DateCreated).Skip((pager.CurrentPage - 1) * pager.RecordsPerPage).Take(pager.RecordsPerPage);

			string userID = User.Identity.GetUserId();

			var viewableForums = db.Forums.Where(f =>
				(!f.MembersOnly && !f.Category.MembersOnly && String.IsNullOrEmpty(f.ViewRole) && String.IsNullOrEmpty(f.Category.RestrictedToRole))
				|| ((f.MembersOnly || f.Category.MembersOnly) && User.Identity.IsAuthenticated)
				|| (!String.IsNullOrEmpty(f.ViewRole) && db.Users.Any(u => u.Id == userID && u.Roles.Any(r => r.Role.Name == f.ViewRole)))
				|| (!String.IsNullOrEmpty(f.Category.RestrictedToRole) && db.Users.Any(u => u.Id == userID && u.Roles.Any(r => r.Role.Name == f.Category.RestrictedToRole)))
				);

			var maxHistory = DateTime.UtcNow.AddMonths(-3);
			@ViewBag.RecentActivity = db.Content.OfType<ForumPost>()
				.Where(p => p.DateCreated > maxHistory &&
					(p is Discussion && viewableForums.Any(f => f.ID == (p as Discussion).Forum.ID))
					|| (p is Reply && viewableForums.Any(f => f.ID == (p as Reply).Discussion.Forum.ID))
					)
				.OrderByDescending(p => p.DateCreated).Take(5);

            return View(featuredDiscussions);
        }

		public EmptyResult TestError()
		{
			throw new Exception("Test Error!");
		}

		public EmptyResult StressTest(int workers = 100)
		{
			NPCPortal.Jobs.JobManager.SubmitJob(new NPCPortal.Jobs.StressTest() { StressWorkerCount = workers });

			return new EmptyResult();
		}
    }
}
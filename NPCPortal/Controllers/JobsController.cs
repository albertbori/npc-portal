﻿using NPCPortal.Jobs;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Controllers
{
    public class JobsController : Controller
    {
		public EmptyResult Run(string key, Guid id)
		{
			Server.ScriptTimeout = int.MaxValue;

			var inputStream = Request.InputStream;
			inputStream.Position = 0;
			using (var reader = new System.IO.StreamReader(inputStream))
			{
				string headers = Request.Headers.ToString();
				string body = reader.ReadToEnd();
				string rawRequest = string.Format(
					"Url:{3}\nBody:\n{0}{1}{1}{2}", headers, Environment.NewLine, body, Request.Url.ToString()
				);
				Log.Debug("Recieved job processing request:\n{0}", rawRequest);
			}

			if (String.IsNullOrEmpty(key))
			{
				Log.Warning("Job endpoint error: Missing auth key.");
				throw new UnauthorizedAccessException();
			}
			string[] keyValues = Security.Decrypt(key).Split(new char[] { '|' });
			string authKey = keyValues[0];
			DateTime requestDate = new DateTime(Convert.ToInt64(keyValues[1]), DateTimeKind.Utc);
			if (authKey != NPCPortalWeb.JobAuthKey)
			{
				Log.Warning("Job endpoint error: Invalid auth key");
				throw new UnauthorizedAccessException();
			}
			DateTime now = DateTime.UtcNow;
			if (requestDate > now || requestDate < now.AddMinutes(-1)) //If the request date is not between now and one minute ago
			{
				Log.Warning("Job endpoint error: Invalid time stamp");
				throw new UnauthorizedAccessException();
			}
			
			using (NPCPortalDB db = new NPCPortalDB())
			{
				var jobRecord = db.Jobs.Find(id);
				IJob job = Newtonsoft.Json.JsonConvert.DeserializeObject(jobRecord.Arguments, jobRecord.Type) as IJob;
				try
				{
					if (jobRecord == null)
						throw new Exception("Could not find job in the database");

					jobRecord.Status = JobStatuses.Running;
					db.SaveChanges();

					jobRecord.ResultMessage = job.Run();

					jobRecord.Status = JobStatuses.Completed;
					jobRecord.DateCompleted = DateTime.UtcNow;
					db.SaveChanges();
				}
				catch (Exception ex)
				{
					Log.Error(ex, String.Format("Error running JobType: {0} Arguments: {1}", Request["AssemblyQualifiedName"], job == null ? "null" : Newtonsoft.Json.JsonConvert.SerializeObject(job)));

					jobRecord.Status = JobStatuses.Failed;
					db.SaveChanges();
				}
			}

			return new EmptyResult();
		}
	}
}
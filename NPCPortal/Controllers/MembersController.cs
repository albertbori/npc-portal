﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NPCPortal.Utilities;
using NPCPortal.Models;
using NPCPortal.Models.ViewModels;

namespace NPCPortal.Controllers
{
    public class MembersController : BaseController
    {
		[NPCAuthorize(PermissionTypes.ViewMembers)]
        public ActionResult Index()
        {
			var userStats = from c in db.Content.OfType<ForumPost>()
							   group c by c.Author into g
							   select new { Author = g.Key, Posts = g.Count(), Likes = g.Sum(p => p.UsersLikeThis.Count()) };

			var memberData = from u in db.Users
							 where (u.UserState & UserStates.Banned) == 0 && (u.UserState & UserStates.Spammer) == 0 && (u.UserState & UserStates.Rejected) == 0
							 join s in userStats on u.Id equals s.Author.Id into us
							 from s in us.DefaultIfEmpty()
							 select new { User = u, Posts = s.Posts != null ? s.Posts : 0, Likes = s.Likes != null ? s.Likes : 0 };

			var pager = new Pager(memberData.Count());
			pager.RecordsPerPage = 24;
			ViewBag.Pager = pager;

			memberData = memberData.OrderBy(u => u.User.Profile.DisplayName ?? u.User.UserName).Skip((pager.CurrentPage - 1) * pager.RecordsPerPage).Take(pager.RecordsPerPage);

			var memberResult = memberData
				.ToList()
				.Select(g => new { User = g.User, Posts = g.Posts, Likes = g.Likes }.ToExpando());

			return View(memberResult);
        }

		[NPCAuthorize(PermissionTypes.ViewMembers)]
		public ActionResult MemberProfile(string id)
		{
			if (!User.CanDo(Models.PermissionTypes.ViewMembers))
				return new HttpUnauthorizedResult();

			var user = db.Users.Single(u => u.Id == id);

			//If the user doesn't have a profile, set one so that defaults can show
			if (user.Profile == null)
				user.Profile = new UserProfile();

			return View(user);
		}

		[Authorize]
		public ActionResult EditProfile(string id)
		{
			if (User.Identity.GetUserId() != id && !User.CanDo(Models.PermissionTypes.EditProfiles))
				return new HttpUnauthorizedResult();

			var user = db.Users.Single(u => u.Id == id);
			return View(user);
		}		

		[HttpPost]
		[Authorize]
		public ActionResult EditProfile(User user)
		{
			if (User.Identity.GetUserId() != user.Id && !User.CanDo(Models.PermissionTypes.EditProfiles))
				return new HttpUnauthorizedResult();

			var existingUser = db.Users.Single(u => u.Id == user.Id);

			if (!String.IsNullOrEmpty(user.DisplayName) && db.UserProfiles.Any(p => p.ID != user.Profile.ID && p.DisplayName.ToLower() == user.Profile.DisplayName.ToLower()))
			{
				ModelState.AddModelError("", "A user with this display name already exists.");
				return View(existingUser);
			}

			if (user.Profile.ID == 0)
			{
				db.UserProfiles.Add(user.Profile);
				existingUser.Profile = user.Profile;
			}
			else
			{
				existingUser.Profile.Bio = user.Profile.Bio;
				existingUser.Profile.DisplayName = user.Profile.DisplayName;
				existingUser.Profile.ShowEmailAddress = user.Profile.ShowEmailAddress;
				existingUser.Profile.TagLine = user.Profile.TagLine;
			}

			//correct profile name:
			if (String.IsNullOrEmpty(existingUser.Profile.DisplayName))
				existingUser.Profile.DisplayName = null;

			var upload = Request.Files["AvatarFile"];
			if(upload.ContentLength > 0)
			{
				if (existingUser.Profile.Avatar != null)
				{
					existingUser.Profile.Avatar = existingUser.Profile.Avatar;
					FileStore.DeleteImage(existingUser.Profile.Avatar.FileName);
				}
				else
					existingUser.Profile.Avatar = new File();

				long fileSize = upload.InputStream.Length;
				string fileType = upload.ContentType;
				string fileName = FileStore.SaveNewImage(upload);

				existingUser.Profile.Avatar.Author = existingUser;
				existingUser.Profile.Avatar.FileName = fileName;
				existingUser.Profile.Avatar.FileSize = fileSize;
				existingUser.Profile.Avatar.FileType = fileType;
			}

			db.SaveChanges();

			Cache.RemoveByID<DisplayUser>(user.Id);

			return RedirectToAction("MemberProfile", new { id = user.Id });
		}

		public JsonResult Search(string q)
		{
			var userID = User.Identity.GetUserId();
			//Get user's by their handles (taking display name over user name where possible)
			var searchableUsers = db.Users.Select(u => new
			{
				id = u.Id,
				name = u.Profile.DisplayName ?? u.UserName,
				image = u.Profile.Avatar
			});

			//match them based on the query
			var results = searchableUsers.Where(u => u.id != userID && u.name.Contains(q)).ToList();

			//format the results the way typeahead.js wants it
			var data = results.Select(u => new {
				value = u.name, 
				tokens = u.name.Split(new char[] { ' ' }),
				id = u.id,
				profileImageUrl = u.image != null ? u.image.GetThumbnailURL(ThumbnailTypes.Avatar) : "" 
			});

			return Json(data.ToArray(), JsonRequestBehavior.AllowGet);
		}

		public PartialViewResult AvatarTooltip(string userID)
		{
			var avatarTooltip = Cache.GetByID<AvatarTooltip>(userID);
			if (avatarTooltip == null) {
				var user = db.Users.Find(userID);
				avatarTooltip = new AvatarTooltip()
				{
					PostCount = user.AuthoredContent.Where(c => c is ForumPost).Count(),
					LikesCount = user.AuthoredContent.Sum(c => c.UsersLikeThis.Count()),
					DateJoined = user.DateJoined,
					Roles = String.Join(", ", user.Roles.Select(r => r.Role.Name.MakeSingular()).ToArray())
				};
				Cache.SetByID(userID, avatarTooltip);
			}
			return PartialView("_AvatarTooltip", avatarTooltip);
		}
	}
}
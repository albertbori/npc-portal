﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Models;
using Microsoft.AspNet.Identity;
using NPCPortal.Utilities;
using NPCPortal.Jobs;

namespace NPCPortal.Controllers
{
    public class MessagesController : BaseController
    {
		protected override void OnActionExecuted(ActionExecutedContext filterContext)
		{
			base.OnActionExecuted(filterContext);

			string userID = User.Identity.GetUserId();
			ViewBag.UnreadCount = db.Messages.Count(m => m.DateSent != null && m.To.Any(r => r.Recipient.Id == userID && r.DateRead == null));
		}

		[NPCAuthorize(PermissionTypes.ViewMessages)]
        public ActionResult Index()
		{
			string userID = User.Identity.GetUserId();

			var messages = db.Messages
				.Where(m => m.InReplyTo == null && m.DateSent != null && (m.To.Any(r => r.Recipient.Id == userID && !r.Deleted) || m.Replies.Any(r => r.To.Any(t => t.Recipient.Id == userID && !t.Deleted))))
				.OrderByDescending(m => m.Replies.OrderByDescending(r => r.DateSent).FirstOrDefault().DateSent ?? m.DateSent)
				.Select(m => new
				{
					Message = m,
					From = m.Replies.OrderByDescending(r => r.DateSent).FirstOrDefault().Author ?? m.Author,
					IsUnread = m.To.Any(r => r.Recipient.Id == userID && r.DateRead == null) || m.Replies.Any(r => r.To.Any(t => t.Recipient.Id == userID && t.DateRead == null)),
					ReplyCount = m.Replies.Count(),
					Date = m.Replies.OrderByDescending(r => r.DateSent).FirstOrDefault().DateSent ?? m.DateSent
				}).ToList();
			
			return View(messages.Select(g => g.ToExpando()));
        }

		[NPCAuthorize(PermissionTypes.ViewMessages)]
		public ActionResult Drafts()
		{
			string userID = User.Identity.GetUserId();
			return View(db.Messages.Where(m => m.DateSent == null && m.Author.Id == userID).OrderByDescending(m => m.DateCreated));
		}

		[NPCAuthorize(PermissionTypes.SendMessages)]
		public ActionResult Sent()
		{
			string userID = User.Identity.GetUserId();

			var messages = db.Messages
				.Where(m => m.InReplyTo == null && m.DateSent != null && (m.Author.Id == userID || m.Replies.Any(r => r.Author.Id == userID)))
				.OrderByDescending(m => m.Replies.OrderByDescending(r => r.DateSent).FirstOrDefault().DateSent ?? m.DateSent)
				.Select(m => new
				{
					Message = m,
					From = m.Replies.OrderByDescending(r => r.DateSent).FirstOrDefault().Author ?? m.Author,
					IsUnread = m.To.Any(r => r.Recipient.Id == userID && r.DateRead == null) || m.Replies.Any(r => r.To.Any(t => t.Recipient.Id == userID && t.DateRead == null)),
					ReplyCount = m.Replies.Count(),
					Date = m.Replies.OrderByDescending(r => r.DateSent).FirstOrDefault().DateSent ?? m.DateSent
				}).ToList();

			return View(messages.Select(g => g.ToExpando()));
		}

		[NPCAuthorize(PermissionTypes.ViewMessages)]
        public ActionResult View(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Message message = db.Messages.Find(id);

            if (message == null)
                return HttpNotFound();

			string userID = User.Identity.GetUserId();

			var recipients = db.MessageRecipients.Where(r => r.Recipient.Id == userID && (r.Message.ID == id || r.Message.InReplyTo.ID == id));

			if (recipients.Count() == 0 && message.Author.Id != userID)
				return new HttpUnauthorizedResult();

			if (recipients.Any(r => r.DateRead == null))
			{
				foreach (var recipient in recipients)
				{
					if (recipient.DateRead == null)
						recipient.DateRead = DateTime.UtcNow;
				}
				db.SaveChanges();
			}

			if (message.InReplyTo != null)
				return View(message.InReplyTo);

            return View(message);
        }

		[NPCAuthorize(PermissionTypes.SendMessages)]
        public ActionResult New(int? id)
        {
			if (id.HasValue)
				return View(db.Messages.Find(id));

            return View();
        }

		[NPCAuthorize(PermissionTypes.SendMessages)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(Message message)
        {
			bool send = Request["Submit"] == "Send";
			string[] recipientIDs = Request["hfRecipients"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			if (send && recipientIDs.Count() == 0)
					ModelState.AddModelError("", "Please select at least one recipient");

            if (ModelState.IsValid)
            {
				if (message.ID > 0)
				{
					var draft = db.Messages.Find(message.ID);
					draft.Subject = message.Subject;
					draft.Body = message.Body;
					message = draft;
				}
				else
				{
					db.Messages.Add(message);
				}

				message.Author = db.Users.Find(CurrentUser.Id);

				//reset recipients (in case of draft change)
				foreach(var recipient in message.To.ToArray())
				{
					db.MessageRecipients.Remove(recipient);
				}

				foreach (var userID in recipientIDs)
				{
					message.To.Add(new MessageRecipient()
						{
							Recipient = db.Users.Find(userID),
							Message = message
						});
				}

				if (send)
					message.DateSent = DateTime.UtcNow;

				db.SaveChanges();

				if (send)
				{
					JobManager.SubmitJob(new NPCPortal.Jobs.Emails.MessageAlertEmail() { MessageID = message.ID });
					return RedirectToAction("Index");
				}
				else
					return RedirectToAction("Drafts");
            }

            return View(message);
        }

		[NPCAuthorize(PermissionTypes.SendMessages)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Reply(int inReplyToID, string body)
		{
			var parentMessage = db.Messages.Find(inReplyToID);
			Message message = new Message();
			message.InReplyTo = parentMessage;
			message.Subject = "RE: " + parentMessage.Subject;
			message.Body = body;
			message.Author = db.Users.Find(CurrentUser.Id);
			message.DateSent = DateTime.UtcNow;

			if (parentMessage.Author.Id != CurrentUser.Id)
			{
				message.To.Add(new MessageRecipient()
				{
					Recipient = parentMessage.Author
				});
			}

			foreach (var recipient in parentMessage.To.Where(r => r.Recipient.Id != message.Author.Id))
			{
				message.To.Add(new MessageRecipient()
				{
					Recipient = recipient.Recipient,
					Message = message
				});
			}

			db.Messages.Add(message);
			db.SaveChanges();

			JobManager.SubmitJob(new NPCPortal.Jobs.Emails.MessageAlertEmail() { MessageID = message.ID });
			
			return RedirectToAction("View", new { id = inReplyToID });
		}

		[NPCAuthorize(PermissionTypes.ViewMessages)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			Message message = db.Messages.Find(id);

            if (message == null)
                return HttpNotFound();
			
			string userID = User.Identity.GetUserId();

			var recipients = db.MessageRecipients.Where(r => r.Recipient.Id == userID && (r.Message.ID == id || r.Message.InReplyTo.ID == id));
			if (recipients.Count() == 0)
				return new HttpUnauthorizedResult();

            return View(message);
        }

		[NPCAuthorize(PermissionTypes.ViewMessages)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
			string userID = User.Identity.GetUserId();

			var recipients = db.MessageRecipients.Where(r => r.Recipient.Id == userID && (r.Message.ID == id || r.Message.InReplyTo.ID == id));

			if (recipients.Count() == 0)
				return new HttpUnauthorizedResult();

			foreach (var recipient in recipients)
				recipient.Deleted = true;

            db.SaveChanges();
            return RedirectToAction("Index");
        }

		[NPCAuthorize(PermissionTypes.SendMessages)]
		public ActionResult DeleteDraft(int id)
		{
			Message message = db.Messages.Find(id);
			string userID = User.Identity.GetUserId();

			if (message.Author.Id != userID)
				return new HttpUnauthorizedResult();

			message.Delete(db);

			return RedirectToAction("Drafts");
		}
    }
}

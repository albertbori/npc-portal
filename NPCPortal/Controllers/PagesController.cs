﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Controllers
{
    public class PagesController : BaseController
    {
		public ActionResult View(int id)
		{
			var page = db.CMSPages.Find(id);

			if (page == null)
				return RedirectToAction("Index", "Home");

			if((page.MembersOnly && !User.Identity.IsAuthenticated)
				|| (!String.IsNullOrEmpty(page.ViewRole) && !User.IsInRole(page.ViewRole)))
			{
				return new HttpUnauthorizedResult();
			}

			return View(page);
		}
	}
}
﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NPCPortal.Utilities;
using System.Text.RegularExpressions;
using NPCPortal.Jobs;

namespace NPCPortal
{
    public class NPCPortalWeb : System.Web.HttpApplication
	{
		public static string CurrentHost { get; set; }
		public static string JobAuthKey { get; set; }
		public static string CurrentTheme { get; private set; }
		private static bool? CustomSiteDomainValid { get; set; }

		public static IEnumerable<SiteSetting> SiteSettings { get; private set; }
		public static RegistrationSettings RegistrationSettings { get; private set; }
		public static GeneralSettings GeneralSettings { get { return SiteSettings.OfType<GeneralSettings>().FirstOrDefault() ?? new GeneralSettings(); } }
		public static EmailSettings EmailSettings { get { return SiteSettings.OfType<EmailSettings>().FirstOrDefault() ?? new EmailSettings(); } }
		public static ReCaptchaSettings ReCaptchaSettings { get { return SiteSettings.OfType<ReCaptchaSettings>().FirstOrDefault() ?? new ReCaptchaSettings(); } }
		public static AnalyticsSettings AnalyticsSettings { get { return SiteSettings.OfType<AnalyticsSettings>().FirstOrDefault() ?? new AnalyticsSettings(); } }

		public NPCPortalWeb()
		{
			this.BeginRequest += NPCPortalWeb_BeginRequest;
		}

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			ModelBinders.Binders.DefaultBinder = new NPCPortal.NPCModelBinder();

			Database.SetInitializer(new MigrateDatabaseToLatestVersion<NPCPortalDB, NPCPortal.Migrations.Configuration>());

			using (NPCPortalDB db = new NPCPortalDB())
			{
				var currentTheme = db.Themes.SingleOrDefault(t => t.IsActive);
				if(currentTheme != null)
					CurrentTheme = currentTheme.Name;
			}
			System.Web.Mvc.ViewEngines.Engines.Clear();
			System.Web.Mvc.ViewEngines.Engines.Add(new ThemeViewEngine(CurrentTheme));

			InitSiteSettings();

			JobAuthKey = Guid.NewGuid().ToString();

			//standardize error message classes
			var field = typeof(HtmlHelper).GetField("ValidationSummaryCssClassName", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
			field.SetValue(null, "alert alert-danger");

			//Kick off scheduler service
			JobScheduler.Start();
        }

		protected void Application_Error()
		{
			try
			{
				Exception unhandledException = Server.GetLastError();

				Log.Error(unhandledException, "An unhandled exception has occurred while processing this request");
			}
			catch {} //while logging is important, it's not worth throwing a new error over, if logging fails
		}

		void NPCPortalWeb_BeginRequest(object sender, EventArgs e)
		{
			Request.RequestContext.HttpContext.Items["theme"] = CurrentTheme;

			if(String.IsNullOrEmpty(CurrentHost))
				CurrentHost = (Request.Url.Port == 80 || Request.Url.Port == 443) ? Request.Url.Host : Request.Url.Authority;

			string siteDomain = GeneralSettings.SiteDomain;
			if (!String.IsNullOrEmpty(siteDomain) && this.Request.Url.Host != siteDomain)
			{
				string newPath = Regex.Replace(this.Request.Url.ToString(), @"(https?://)[^/:]+", "$1" + siteDomain, RegexOptions.IgnoreCase);

				//Validate domain before switching over (in case they messed up. don't want to hose their whole website)
				if(CustomSiteDomainValid == null)
				{
					Log.Debug("Setting custom site domain: {0}", siteDomain);
					var request = System.Net.WebRequest.Create(newPath);
					try
					{
						request.GetResponse();
						CustomSiteDomainValid = true;
						Log.Info("Custom site domain set to: {0}", siteDomain);
					}
					catch(Exception ex)
					{
						Log.Warning("Failed to set custom site domain: {0}\n{1}", siteDomain, ex.ToString());
						CustomSiteDomainValid = false;
					}
				}

				if(CustomSiteDomainValid.Value)
					Response.RedirectPermanent(newPath);
			}
		}

		public static void ChangeTheme(string newTheme)
		{
			CurrentTheme = newTheme;
			System.Web.Mvc.ViewEngines.Engines.Clear();
			System.Web.Mvc.ViewEngines.Engines.Add(new ThemeViewEngine(newTheme));
		}

		public static void InitSiteSettings()
		{
			CustomSiteDomainValid = null;
			using (NPCPortalDB db = new NPCPortalDB())
			{
				SiteSettings = db.SiteSettings.ToList();
				RegistrationSettings = db.SiteSettings.Include("RegistrationQuestions.Answers").OfType<RegistrationSettings>().FirstOrDefault() ?? new RegistrationSettings();
			}

			var recaptchaSettings = SiteSettings.OfType<ReCaptchaSettings>().FirstOrDefault();
			if(recaptchaSettings != null)
			{
				Recaptcha.RecaptchaControlMvc.PrivateKey = recaptchaSettings.PrivateKey;
				Recaptcha.RecaptchaControlMvc.PublicKey = recaptchaSettings.PublicKey;
			}
		}
		public static TimeZoneInfo GetCurrentTimeZone()
		{
			//Load correct time zone for server
			TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(NPCPortalWeb.GeneralSettings.TimeZone);

			//Try to load the correct time zone for user (if authenticated)
			var user = HttpContext.Current.GetCurrentUser();
			if (user != null && user.Settings.OfType<NPCPortal.Models.UserGeneralSettings>().Any())
				timeZone = TimeZoneInfo.FindSystemTimeZoneById(user.Settings.OfType<NPCPortal.Models.UserGeneralSettings>().First().TimeZone);

			return timeZone;
		}
    }
}

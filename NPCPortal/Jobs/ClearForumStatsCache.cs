﻿using NPCPortal.Models;
using NPCPortal.Models.ViewModels;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs
{
	public class ClearForumStatsCache : IJob
	{
		public Guid ID { get; set; }

		public int ContentID { get; set; }

		public string Run()
		{
			using (var db = new NPCPortalDB())
			{
				var content = db.Content.Find(ContentID);
				Cache.RemoveByID<AvatarTooltip>(content.Author.Id);

				Forum forum = null;
				if (content is Discussion)
				{
					forum = (content as Discussion).Forum;
				}
				else if (content is Reply)
				{
					forum = (content as Reply).Discussion.Forum;
				}
				if (forum != null)
				{
					Cache.RemoveByID<ForumStats>(forum.ID);
					Cache.RemoveByID<ForumCategoryStats>(0); //Clear the root forum stats view

					var parentCategory = forum.Category;
					while (parentCategory != null)
					{
						Cache.RemoveByID<ForumCategoryStats>(parentCategory.ID);
						parentCategory = parentCategory.ParentCategory;
					}
				}
			}

			return String.Format("Cache cleared for content id: {0}", ContentID);
		}
	}
}
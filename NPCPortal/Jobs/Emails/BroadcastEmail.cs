﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs.Emails
{
	public class BroadcastEmail : IJob
	{
		public Guid ID { get; set; }
		public string Role { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
		public string RecipientUserId { get; set; }

		public string Run()
		{
			using (NPCPortalDB db = new NPCPortalDB())
			{
				string viewError;
				BaseController controller = new BaseController();

				var user = db.Users.Find(this.RecipientUserId);
				var viewData = new ViewDataDictionary();
				viewData["Message"] = this.Body;

				string html = controller.RenderViewToString("~/Views/Emails/BroadcastMessage.cshtml", viewData, out viewError);

				if (!String.IsNullOrEmpty(viewError))
					throw new Exception("Error retrieving email html: " + viewError);

				Email.Send(user.DisplayName, user.Email, this.Subject, html);

				return "Broadcast Email sent to: " + user.Email;
			}
		}
	}
}
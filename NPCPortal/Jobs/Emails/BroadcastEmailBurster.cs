﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs.Emails
{
	public class BroadcastEmailBurster : IJob
	{
		public Guid ID { get; set; }
		public string Role { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
		public string TestRecipientUserId { get; set; }

		public string Run()
		{
			int recipientCount = 0;
			if (!String.IsNullOrEmpty(this.TestRecipientUserId))
			{
				JobManager.SubmitJob(new BroadcastEmail()
				{
					Role = this.Role,
					Subject = this.Subject,
					Body = this.Body,
					RecipientUserId = this.TestRecipientUserId
				});

				recipientCount = 1;
			}
			else
			{
				using (NPCPortalDB db = new NPCPortalDB())
				{
					var recipients = db.Users.Where(u => (u.UserState & (UserStates.Banned)) == 0 && u.Email != null && (u.UserState & (UserStates.EmailValidated)) != 0);
					if (!String.IsNullOrEmpty(this.Role))
						recipients = recipients.Where(u => u.Roles.Any(r => r.Role.Name == this.Role));

					var jobs = new List<IJob>();
					foreach (var userId in recipients.Select(u => u.Id))
					{
						jobs.Add(new BroadcastEmail()
						{
							Role = this.Role,
							Subject = this.Subject,
							Body = this.Body,
							RecipientUserId = userId
						});
					}

					JobManager.SubmitJobs(jobs);

					recipientCount = recipients.Count();
				}
			}

			return String.Format("{0} BroadcastEmail jobs submitted", recipientCount);
		}
	}
}
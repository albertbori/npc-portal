﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs.Emails
{
	public class MessageAlertEmail : IJob
	{
		public Guid ID { get; set; }
		public int MessageID { get; set; }

		public string Run()
		{
			using (NPCPortalDB db = new NPCPortalDB())
			{
				var message = db.Messages.Find(this.MessageID);

				int recipientCount = 0;
				foreach (var recipient in message.To.Select(r => r.Recipient))
				{
					var settings = recipient.Settings.OfType<UserEmailSettings>().FirstOrDefault();
					if (settings == null || settings.EnableMessageAlerts)
					{
						string siteName = NPCPortalWeb.SiteSettings.OfType<GeneralSettings>().First().SiteName;

						string viewError;
						BaseController controller = new BaseController();
						ViewDataDictionary viewData = new ViewDataDictionary(message);
						viewData["siteName"] = siteName;
						viewData["recipient"] = recipient;
						string html = controller.RenderViewToString("~/Views/Emails/MessageAlert.cshtml", viewData, out viewError);

						if (!String.IsNullOrEmpty(viewError))
							throw new Exception("Error retrieving email html: " + viewError);

						Email.Send(recipient.DisplayName, recipient.Email, siteName + " - New Message Alert", html);
					}
					recipientCount++;
				}

				return String.Format("Message Alert Email sent to {0} recipients", recipientCount);
			}
		}

		private class FakePrincipal : System.Security.Principal.IPrincipal, System.Security.Principal.IIdentity
		{
			public System.Security.Principal.IIdentity Identity
			{
				get { return this; }
			}

			public bool IsInRole(string role)
			{
				throw new NotImplementedException();
			}

			public string AuthenticationType { get; set; }

			public bool IsAuthenticated { get; set; }

			public string Name { get; set; }
		}
	}
}
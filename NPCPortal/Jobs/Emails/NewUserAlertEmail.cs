﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs.Emails
{
	public class NewUserAlertEmail : IJob
	{
		public Guid ID { get; set; }

		public string UserID { get; set; }

		public string Run()
		{
			var registrationSettings = NPCPortalWeb.SiteSettings.OfType<RegistrationSettings>().First();
			if (registrationSettings.RequireApproval)
			{
				using (NPCPortalDB db = new NPCPortalDB())
				{
					var emailSettings = NPCPortalWeb.SiteSettings.OfType<EmailSettings>().FirstOrDefault();
					if (emailSettings != null)
					{
						var users = db.Users.Where(u => u.Roles.Any(r => ((r.Role as Role).Permissions & PermissionTypes.ApproveUsers) != 0));

						var generalSettings = NPCPortalWeb.SiteSettings.OfType<GeneralSettings>().First();
						string viewError;
						BaseController controller = new BaseController();
						ViewDataDictionary viewData = new ViewDataDictionary(db.Users.Find(this.UserID));
						viewData["siteName"] = generalSettings.SiteName;
						string messageBody = controller.RenderViewToString("~/Views/Emails/NewUserAlert.cshtml", viewData, out viewError);

						if (!String.IsNullOrEmpty(viewError))
							throw new Exception("Error retrieving email html: " + viewError);

						foreach (var user in users)
						{
							Email.Send(user.DisplayName, user.Email, "New User Alert - " + generalSettings.SiteName, messageBody);
						}

						return String.Format("New User Alert was sent to {0} recipients", users.Count());
					}
					else
						return "No emails were sent. EmailSettings was null";
				}
			}
			else
				return "No emails were sent. RegistrationSettings Doesn't require new user approval.";
		}
	}
}
﻿using Microsoft.AspNet.Identity.EntityFramework;
using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs.Emails
{
	[Serializable]
	public class SendNewsEmail : IJob
	{
		public Guid ID { get; set; }

		public int NewsContentID { get; set; }

		public string UserID { get; set; }
		
		public string Run()
		{
			using(NPCPortalDB db = new NPCPortalDB())
			{
				var news = (Discussion)db.Content.Find(this.NewsContentID);
				var user = db.Users.Find(this.UserID);

				if (String.IsNullOrEmpty(news.Forum.ViewRole) || user.IsInRole(news.Forum.ViewRole))
				{
					string siteName = NPCPortalWeb.SiteSettings.OfType<GeneralSettings>().First().SiteName;

					BaseController controller = new BaseController();

					string viewError;
					ViewDataDictionary viewData = new ViewDataDictionary(news);
					viewData.Add("CurrentUser", user);
					string html = controller.RenderViewToString("~/Views/Emails/NewsUpdate.cshtml", viewData, out viewError);

					if (!String.IsNullOrEmpty(viewError))
						throw new Exception("Error retrieving email html: " + viewError);

					var settings = user.Settings.OfType<UserGeneralSettings>().FirstOrDefault();
					string timeZoneID = settings == null ? NPCPortalWeb.SiteSettings.OfType<GeneralSettings>().First().TimeZone : settings.TimeZone;
					string date = TimeZoneInfo.ConvertTimeFromUtc(news.DateCreated, TimeZoneInfo.FindSystemTimeZoneById(timeZoneID)).ToShortDateString();
					Email.Send(user.DisplayName, user.Email, siteName + " - News - " + date, html);

					return "News Email sent to: " + user.Email;
				}
				else
					return "Email was not set to : " + user.Email + ". User not authorized to view this news.";
			}
		}
	}
}
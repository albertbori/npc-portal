﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs.Emails
{
	[Serializable]
	public class SendNewsEmailBurster : IJob
	{
		public Guid ID { get; set; }

		public int NewsContentID { get; set; }

		public string Run()
		{
			using (NPCPortalDB db = new NPCPortalDB())
			{
				var recipients = db.Users.Where(u => (u.UserState & (UserStates.Banned)) == 0 && u.Email != null && (u.UserState & (UserStates.EmailValidated)) != 0 && 
					(u.Settings.Count() == 0 || u.Settings.OfType<UserEmailSettings>().Any(s => s.EnableNewsAndUpdates)));

				var jobs = new List<IJob>();
				foreach (var user in recipients)
				{
					jobs.Add(new SendNewsEmail() { NewsContentID = this.NewsContentID, UserID = user.Id });
				}
				JobManager.SubmitJobs(jobs);

				return String.Format("{0} SendNewsEmail jobs submitted", recipients.Count());
			}
		}
	}
}
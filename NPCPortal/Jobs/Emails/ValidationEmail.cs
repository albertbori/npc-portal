﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs.Emails
{
	public class ValidationEmail : IJob
	{
		public Guid ID { get; set; }
		public string UserID { get; set; }
		public string EmailAddress { get; set; }

		public string Run()
		{
			using (NPCPortalDB db = new NPCPortalDB())
			{
				var user = db.Users.Find(this.UserID);

				string siteName = NPCPortalWeb.SiteSettings.OfType<GeneralSettings>().First().SiteName;

				string viewError;
				BaseController controller = new BaseController();
				ViewDataDictionary viewData = new ViewDataDictionary(user);
				viewData["key"] = Security.Encrypt(user.Id + "|" + this.EmailAddress);
				string html = controller.RenderViewToString("~/Views/Emails/EmailValidation.cshtml", viewData, out viewError);

				if (!String.IsNullOrEmpty(viewError))
					throw new Exception("Error retrieving email html: " + viewError);

				Email.Send(user.DisplayName, this.EmailAddress, siteName + " - Email Validation", html);

				return "Validation Email sent to: " + this.EmailAddress;
			}
		}
	}
}
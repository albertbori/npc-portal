﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs
{
	public class WatchedDiscussionEmail : IJob
	{
		public Guid ID { get; set; }
		public string UserID { get; set; }
		public int DiscussionID { get; set; }

		public string Run()
		{
			using(NPCPortalDB db = new NPCPortalDB())
			{
				var recipient = db.Users.Find(this.UserID);
				var discussion = db.ForumPosts.Find(this.DiscussionID) as Discussion;
				if (discussion.AllReplies.OrderByDescending(d => d.DateCreated).First().Author.Id != recipient.Id)
				{
					string subject = String.Format("{0} - New Reply on '{1}'", NPCPortalWeb.GeneralSettings.SiteName, discussion.Title.Display(recipient));

					Email.Send(recipient.DisplayName, recipient.Email, subject, "~/Views/Emails/WatchedDiscussion.cshtml",
						discussion, new Dictionary<string, object>() { { "recipient", recipient } });

					return "Watched Discussion Email sent to: " + recipient.Email;
				}
				else
					return "No email sent. The update was for this user's own post.";
			}
		}
	}
}
﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs
{
	public class WatchedDiscussionEmailBurster : IJob
	{
		public Guid ID { get; set; }
		public int DiscussionID { get; set; }

		public string Run()
		{
			using(NPCPortalDB db = new NPCPortalDB())
			{
				var discussion = db.ForumPosts.Find(this.DiscussionID) as Discussion;
				var recipients = discussion.WatchingUsers
					.Where(u => !u.Settings.OfType<UserEmailSettings>().Any() ||
						u.Settings.OfType<UserEmailSettings>().Any(s => s.EnableWatchedThreadNotifications == EmailFrequency.Instant));
				var jobs = new List<IJob>();
				foreach(var user in recipients)
				{
					jobs.Add(new WatchedDiscussionEmail()
					{
						DiscussionID = this.DiscussionID,
						UserID = user.Id
					});
				}
				JobManager.SubmitJobs(jobs);

				return String.Format("{0} WatchedDiscussionEmail jobs submitted.", recipients.Count());
			}
		}
	}
}
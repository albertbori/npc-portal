﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs
{
	public class WatchedDiscussionRollupEmail : IJob
	{
		public Guid ID { get; set; }
		public string UserID { get; set; }
		public int MinutesBack { get; set; }

		public string Run()
		{
			using(NPCPortalDB db = new NPCPortalDB())
			{				
				DateTime sinceWhen = DateTime.UtcNow.AddMinutes(this.MinutesBack * -1);
				var recipient = db.Users.Find(this.UserID);
				var unread = recipient.WatchedDiscussions
					.Where(d => d.AllReplies.Any(r => r.Author.Id != recipient.Id && r.DateCreated > sinceWhen)
						&& (!recipient.ViewedContent.Any(vc => vc.Content.ID == d.ID) 
							|| recipient.ViewedContent.Any(vc => vc.Content.ID == d.ID && vc.ViewedOn > d.LastActivityDate)));

				if(unread.Count() > 0)
				{
					string subject = String.Format("{0} - Watched Discussion Updates", NPCPortalWeb.GeneralSettings.SiteName);

					Email.Send(recipient.DisplayName, recipient.Email, subject,
						"~/Views/Emails/WatchedDiscussionRollup.cshtml", unread, 
						new Dictionary<string, object>() { 
							{ "recipient", recipient },
							{ "sinceWhen", sinceWhen }
						});

					var viewData = new ViewDataDictionary(unread);
					viewData["recipient"] = recipient;

					string viewError;
					BaseController controller = new BaseController();
					string html = controller.RenderViewToString("~/Views/Emails/WatchedDiscussionRollup.cshtml", viewData, out viewError);

					if (!String.IsNullOrEmpty(viewError))
						throw new Exception("Error retrieving email html: " + viewError);

					Email.Send(recipient.DisplayName, recipient.Email, subject, html);

					return "Watched Discussion Rollup Email sent to: " + recipient.Email;
				}
				else
					return "Nothing found for: " + recipient.Email;
			}
		}
	}
}
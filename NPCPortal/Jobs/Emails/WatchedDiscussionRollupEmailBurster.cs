﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs
{
	public class WatchedDiscussionRollupEmailBurster : IJob
	{
		public Guid ID { get; set; }

		public EmailFrequency EmailFrequency { get; set; }

		public string Run()
		{
			using(NPCPortalDB db = new NPCPortalDB())
			{
				var recipients = db.Users.Where(u => u.Settings.OfType<UserEmailSettings>().Any(s => s.EnableWatchedThreadNotifications == this.EmailFrequency));

				var jobs = new List<IJob>();
				foreach(var user in recipients)
				{
					jobs.Add(new WatchedDiscussionRollupEmail() { UserID = user.Id });
				}
				JobManager.SubmitJobs(jobs);

				return String.Format("{0} WatchedDiscussionRollupEmail jobs submitted.", recipients.Count());
			}
		}
	}
}
﻿using NPCPortal.Controllers;
using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Jobs.Emails
{
	public class WelcomeEmail : IJob
	{
		public Guid ID { get; set; }

		public string UserID { get; set; }

		public string Run()
		{
			using (NPCPortalDB db = new NPCPortalDB())
			{
				var user = db.Users.Find(this.UserID);

				var emailSettings = NPCPortalWeb.SiteSettings.OfType<EmailSettings>().FirstOrDefault();
				if (emailSettings != null)
				{
					string welcomeSubject = emailSettings.WelcomeMessageSubject;
					string welcomeMessage = emailSettings.WelcomeMessageBody;
					if (!String.IsNullOrEmpty(welcomeSubject) && !String.IsNullOrEmpty(welcomeMessage))
					{
						string viewError;
						BaseController controller = new BaseController();
						ViewDataDictionary viewData = new ViewDataDictionary(user);
						viewData["Message"] = welcomeMessage;
						string welcomeMessageHtml = controller.RenderViewToString("~/Views/Emails/WelcomeMessage.cshtml", viewData, out viewError);

						if (!String.IsNullOrEmpty(viewError))
							throw new Exception("Error retrieving email html: " + viewError);

						Email.Send(user.DisplayName, user.Email, welcomeSubject, welcomeMessageHtml);

						return "Welcome email sent to: " + user.Email;
					}
					else
						return "No email was sent: Welcome Subject or Message was empty";
				}
				else
					return "No email was sent: EmailSettings was null";
			}
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs
{
	public interface IJob
	{
		Guid ID { get; set; }
		/// <summary>
		/// Runs the code for the given job
		/// </summary>
		/// <returns>A job result string or null</returns>
		string Run();
	}
}
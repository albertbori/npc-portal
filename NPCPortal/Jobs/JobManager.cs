﻿using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

namespace NPCPortal.Jobs
{
	public static class JobManager
	{
		public static Guid SubmitJob(IJob job)
		{
			job.ID = Guid.NewGuid();

			JobRunner runner = new JobRunner(RunJob);
			runner.BeginInvoke(job, null, null);

			return job.ID;
		}

		private static void RunJob(IJob job)
		{
			string jobURL = String.Empty;
			try
			{
				using (NPCPortalDB db = new NPCPortalDB())
				{
					Job jobRecord = new Job(job);
					db.Jobs.Add(jobRecord);
					db.SaveChanges();
									
					jobURL = String.Format("http://{0}/Jobs/Run?key={1}&id={2}",
						NPCPortalWeb.CurrentHost,
						Security.Encrypt(NPCPortalWeb.JobAuthKey + "|" + DateTime.UtcNow.Ticks.ToString()).URLEncode(),
						jobRecord.ID);

					HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(jobURL);
					request.Method = "POST";
					request.ContentType = "application/json";
					request.Accept = "application/json";
					request.ContentLength = 0;

					using (var response = request.GetResponse())
					{
						//Do nothing, let it dispose
					}
				}
			}
			catch(Exception ex)
			{
				if (ex is WebException)
				{
					StreamReader sr = new StreamReader(((WebException)ex).Response.GetResponseStream());
					Log.Error(ex, "Error calling job endpoint: {0}\nJob Data:\n{1}\nResponse:\n{2}", jobURL, Newtonsoft.Json.JsonConvert.SerializeObject(job), sr.ReadToEnd());
				}
				else
					Log.Error(ex, "Error calling job endpoint: {0}\nJob Data:\n{1}", jobURL, Newtonsoft.Json.JsonConvert.SerializeObject(job));

				using(NPCPortalDB db = new NPCPortalDB())
				{
					var jobRecord = db.Jobs.SingleOrDefault(j => j.ID == job.ID);
					if(jobRecord != null)
					{
						jobRecord.Status = JobStatuses.Failed;
						jobRecord.ResultMessage = "Failed to call job endpoint";
						db.SaveChanges();
					}
				}
			}
		}

		private delegate void JobRunner(IJob job);

		/// <summary>
		/// Bursts out a collection of jobs in such a way that won't bring the site down
		/// </summary>
		/// <param name="job"></param>
		public static void SubmitJobs(IEnumerable<IJob> jobs)
		{ 
			JobBurster burster = new JobBurster(BurstJobs);
			burster.BeginInvoke(jobs, null, null);
		}

		private static void BurstJobs(IEnumerable<IJob> jobs)
		{
			foreach(var job in jobs)
			{
				SubmitJob(job);
				Thread.Sleep(1000); //Sleep for a second as to not overwhelm things.
			}
		}

		private delegate void JobBurster(IEnumerable<IJob> job);
	}
}
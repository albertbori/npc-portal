﻿using NPCPortal.Models;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace NPCPortal.Jobs
{
	public static class JobScheduler
	{
		public static void Start()
		{
			try
			{
				Runner runner = new Runner(Run);
				runner.BeginInvoke(null, null);
				using (NPCPortalDB db = new NPCPortalDB())
				{
					Log.Info("Job scheduler started for the following jobs:\n" + String.Join("\n", db.ScheduledJobs.ToList().Select(j => j.ToString()).ToArray()));
				}
			}
			catch(Exception ex)
			{
				Log.Error(ex, "Job Scheduler failed to start");
			}
		}

		private static void Run()
		{
			while (true)
			{
				try
				{
					using (NPCPortalDB db = new NPCPortalDB())
					{
						var lapsedJobs = db.ScheduledJobs.ToList().Where(j => j.LastRun == null || j.LastRun.Value.Add(j.Interval) < DateTime.UtcNow);
						foreach (var scheduledJob in lapsedJobs)
						{
							Log.Debug("Running scheduled job:\n{0}", Newtonsoft.Json.JsonConvert.SerializeObject(scheduledJob));
							var job = Newtonsoft.Json.JsonConvert.DeserializeObject(scheduledJob.Arguments, Type.GetType(scheduledJob.Name)) as IJob;
							JobManager.SubmitJob(job);
							scheduledJob.LastRun = DateTime.UtcNow;
							db.SaveChanges();
						}
					}
				}
				catch (Exception ex)
				{
					Log.Error(ex, "Job Scheduler encountered a critical error.");
				}

				Thread.Sleep(60000);
			}
		}

		private delegate void Runner();
	}
}
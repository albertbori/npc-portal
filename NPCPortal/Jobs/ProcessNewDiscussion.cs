﻿using NPCPortal.Jobs.Emails;
using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPCPortal.Utilities;
using NPCPortal.Models.ViewModels;

namespace NPCPortal.Jobs
{
	[Serializable]
	public class ProcessNewDiscussion : IJob
	{
		public Guid ID { get; set; }
		public int ContentID { get; set; }

		public string Run()
		{
			JobManager.SubmitJob(new ClearForumStatsCache() { ContentID = ContentID });

			List<string> results = new List<string>();
			using(NPCPortalDB db = new NPCPortalDB())
			{
				var discussion = db.Content.Find(this.ContentID) as Discussion;
				if(discussion.IsFeatured)
				{
					JobManager.SubmitJob(new SendNewsEmailBurster() { NewsContentID = discussion.ID });
					results.Add("SendNewsEmailBurster job submitted");
				}
			}

			return String.Join(", ", results.ToArray());
		}
	}
}
﻿using NPCPortal.Models;
using NPCPortal.Models.ViewModels;
using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs
{
	public class ProcessNewReply : IJob
	{
		public Guid ID { get; set; }
		public int ReplyID { get; set; }

		public string Run()
		{
			JobManager.SubmitJob(new ClearForumStatsCache() { ContentID = ReplyID });
			using (NPCPortalDB db = new NPCPortalDB())
			{
				var reply = db.ForumPosts.Find(this.ReplyID) as Reply;
				JobManager.SubmitJob(new WatchedDiscussionEmailBurster() { DiscussionID = reply.Discussion.ID });
			}

			return "Processed new reply: " + this.ReplyID;
		}
	}
}
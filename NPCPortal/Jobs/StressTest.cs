﻿using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Jobs
{
	public class StressTest : IJob
	{
		public Guid ID { get; set; }

		public int StressWorkerCount { get; set; }

		public string Run()
		{
			List<IJob> jobs = new List<IJob>();
			int count = 0;
			for (int i = 0; i < this.StressWorkerCount; i++)
			{
				JobManager.SubmitJob(new StressTestWorker());
				count++;
			}

			return "Started " + count + " StressTestWorker jobs";
		}
	}

	public class StressTestWorker : IJob
	{
		public Guid ID { get; set; }
		public string Run()
		{
			Log.Debug("Ran Stress Worker");
			return "Ran";
		}
	}
}
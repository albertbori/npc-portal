namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ModerationStatus = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Author_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.Author_Id)
                .Index(t => t.Author_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserName = c.String(),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        Email = c.String(),
                        ConfirmedEmail = c.Boolean(),
                        DisplayName = c.String(),
                        DateJoined = c.DateTime(),
                        UserStatus = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Avatar_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.Avatar_ID)
                .Index(t => t.Avatar_ID);
            
            CreateTable(
                "dbo.Forums",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        ViewRole = c.String(),
                        PostRole = c.String(),
                        Category_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ForumCategories", t => t.Category_ID)
                .Index(t => t.Category_ID);
            
            CreateTable(
                "dbo.ForumCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        RestrictedToRole = c.String(),
                        ParentCategory_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ForumCategories", t => t.ParentCategory_ID)
                .Index(t => t.ParentCategory_ID);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.LoginProvider, t.ProviderKey })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SiteSettings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EmailEnabled = c.Boolean(),
                        MailServer = c.String(),
                        Port = c.Int(),
                        IsSMTP = c.Boolean(),
                        UserName = c.String(),
                        Password = c.String(),
                        SiteUrl = c.String(),
                        ModerationEnabled = c.Boolean(),
                        RequireModeration = c.Boolean(),
                        CommunityModerationEnabled = c.Boolean(),
                        AutomaticModerationEnabled = c.Boolean(),
                        ReputationSystemEnabled = c.Boolean(),
                        InvitationOnly = c.Boolean(),
                        RegistrationPassword = c.String(),
                        CapchaEnabled = c.Boolean(),
                        RequireEmailValidation = c.Boolean(),
                        RequireApproval = c.Boolean(),
                        PointsPerPost = c.Int(),
                        PointsPerLike = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ReputationPermissions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PermissionType = c.Int(nullable: false),
                        MinimumReputation = c.Int(nullable: false),
                        PermissionsSettings_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SiteSettings", t => t.PermissionsSettings_ID)
                .Index(t => t.PermissionsSettings_ID);
            
            CreateTable(
                "dbo.RolePermissions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PermissionType = c.Int(nullable: false),
                        Role = c.String(),
                        AllowAnonymous = c.Boolean(nullable: false),
                        PermissionsSettings_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SiteSettings", t => t.PermissionsSettings_ID)
                .Index(t => t.PermissionsSettings_ID);
            
            CreateTable(
                "dbo.RegistrationQuestions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        RegistrationSettings_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SiteSettings", t => t.RegistrationSettings_ID)
                .Index(t => t.RegistrationSettings_ID);
            
            CreateTable(
                "dbo.RegistrationQuestionAnswers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        IsCorrect = c.Boolean(nullable: false),
                        RegistrationQuestion_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RegistrationQuestions", t => t.RegistrationQuestion_ID)
                .Index(t => t.RegistrationQuestion_ID);
            
            CreateTable(
                "dbo.ForumPostAttachments",
                c => new
                    {
                        ForumPost_ID = c.Int(nullable: false),
                        File_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ForumPost_ID, t.File_ID })
                .ForeignKey("dbo.ForumPosts", t => t.ForumPost_ID)
                .ForeignKey("dbo.Files", t => t.File_ID)
                .Index(t => t.ForumPost_ID)
                .Index(t => t.File_ID);
            
            CreateTable(
                "dbo.LikedContent",
                c => new
                    {
                        User_ID = c.String(nullable: false, maxLength: 128),
                        Content_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_ID, t.Content_ID })
                .ForeignKey("dbo.AspNetUsers", t => t.User_ID, cascadeDelete: true)
                .ForeignKey("dbo.Contents", t => t.Content_ID, cascadeDelete: true)
                .Index(t => t.User_ID)
                .Index(t => t.Content_ID);
            
            CreateTable(
                "dbo.WatchedDiscussions",
                c => new
                    {
                        User_ID = c.String(nullable: false, maxLength: 128),
                        Discussion_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_ID, t.Discussion_ID })
                .ForeignKey("dbo.AspNetUsers", t => t.User_ID, cascadeDelete: true)
                .ForeignKey("dbo.Discussions", t => t.Discussion_ID, cascadeDelete: true)
                .Index(t => t.User_ID)
                .Index(t => t.Discussion_ID);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        FileName = c.String(),
                        FileSize = c.Int(nullable: false),
                        FileType = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contents", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.ForumPosts",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contents", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.Replies",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Discussion_ID = c.Int(),
                        InReplyTo_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ForumPosts", t => t.ID)
                .ForeignKey("dbo.Discussions", t => t.Discussion_ID)
                .ForeignKey("dbo.ForumPosts", t => t.InReplyTo_ID)
                .Index(t => t.ID)
                .Index(t => t.Discussion_ID)
                .Index(t => t.InReplyTo_ID);
            
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Forum_ID = c.Int(),
                        Title = c.String(),
                        IsFeatured = c.Boolean(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ForumPosts", t => t.ID)
                .ForeignKey("dbo.Forums", t => t.Forum_ID)
                .Index(t => t.ID)
                .Index(t => t.Forum_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Discussions", "Forum_ID", "dbo.Forums");
            DropForeignKey("dbo.Discussions", "ID", "dbo.ForumPosts");
            DropForeignKey("dbo.Replies", "InReplyTo_ID", "dbo.ForumPosts");
            DropForeignKey("dbo.Replies", "Discussion_ID", "dbo.Discussions");
            DropForeignKey("dbo.Replies", "ID", "dbo.ForumPosts");
            DropForeignKey("dbo.ForumPosts", "ID", "dbo.Contents");
            DropForeignKey("dbo.Files", "ID", "dbo.Contents");
            DropForeignKey("dbo.RegistrationQuestions", "RegistrationSettings_ID", "dbo.SiteSettings");
            DropForeignKey("dbo.RegistrationQuestionAnswers", "RegistrationQuestion_ID", "dbo.RegistrationQuestions");
            DropForeignKey("dbo.RolePermissions", "PermissionsSettings_ID", "dbo.SiteSettings");
            DropForeignKey("dbo.ReputationPermissions", "PermissionsSettings_ID", "dbo.SiteSettings");
            DropForeignKey("dbo.Contents", "Author_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.WatchedDiscussions", "Discussion_ID", "dbo.Discussions");
            DropForeignKey("dbo.WatchedDiscussions", "User_ID", "dbo.AspNetUsers");
            DropForeignKey("dbo.LikedContent", "Content_ID", "dbo.Contents");
            DropForeignKey("dbo.LikedContent", "User_ID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Avatar_ID", "dbo.Files");
            DropForeignKey("dbo.Forums", "Category_ID", "dbo.ForumCategories");
            DropForeignKey("dbo.ForumCategories", "ParentCategory_ID", "dbo.ForumCategories");
            DropForeignKey("dbo.ForumPostAttachments", "File_ID", "dbo.Files");
            DropForeignKey("dbo.ForumPostAttachments", "ForumPost_ID", "dbo.ForumPosts");
            DropIndex("dbo.Discussions", new[] { "Forum_ID" });
            DropIndex("dbo.Discussions", new[] { "ID" });
            DropIndex("dbo.Replies", new[] { "InReplyTo_ID" });
            DropIndex("dbo.Replies", new[] { "Discussion_ID" });
            DropIndex("dbo.Replies", new[] { "ID" });
            DropIndex("dbo.ForumPosts", new[] { "ID" });
            DropIndex("dbo.Files", new[] { "ID" });
            DropIndex("dbo.RegistrationQuestions", new[] { "RegistrationSettings_ID" });
            DropIndex("dbo.RegistrationQuestionAnswers", new[] { "RegistrationQuestion_ID" });
            DropIndex("dbo.RolePermissions", new[] { "PermissionsSettings_ID" });
            DropIndex("dbo.ReputationPermissions", new[] { "PermissionsSettings_ID" });
            DropIndex("dbo.Contents", new[] { "Author_Id" });
            DropIndex("dbo.WatchedDiscussions", new[] { "Discussion_ID" });
            DropIndex("dbo.WatchedDiscussions", new[] { "User_ID" });
            DropIndex("dbo.LikedContent", new[] { "Content_ID" });
            DropIndex("dbo.LikedContent", new[] { "User_ID" });
            DropIndex("dbo.AspNetUserClaims", new[] { "User_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Avatar_ID" });
            DropIndex("dbo.Forums", new[] { "Category_ID" });
            DropIndex("dbo.ForumCategories", new[] { "ParentCategory_ID" });
            DropIndex("dbo.ForumPostAttachments", new[] { "File_ID" });
            DropIndex("dbo.ForumPostAttachments", new[] { "ForumPost_ID" });
            DropTable("dbo.Discussions");
            DropTable("dbo.Replies");
            DropTable("dbo.ForumPosts");
            DropTable("dbo.Files");
            DropTable("dbo.WatchedDiscussions");
            DropTable("dbo.LikedContent");
            DropTable("dbo.ForumPostAttachments");
            DropTable("dbo.RegistrationQuestionAnswers");
            DropTable("dbo.RegistrationQuestions");
            DropTable("dbo.RolePermissions");
            DropTable("dbo.ReputationPermissions");
            DropTable("dbo.SiteSettings");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.ForumCategories");
            DropTable("dbo.Forums");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Contents");
        }
    }
}

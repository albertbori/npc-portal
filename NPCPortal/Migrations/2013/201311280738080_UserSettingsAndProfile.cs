namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserSettingsAndProfile : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Avatar_ID", "dbo.Files");
            DropIndex("dbo.AspNetUsers", new[] { "Avatar_ID" });
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(),
                        Bio = c.String(),
                        TagLine = c.String(),
                        ShowEmailAddress = c.Boolean(nullable: false),
                        Avatar_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Files", t => t.Avatar_ID)
                .Index(t => t.Avatar_ID);
            
            CreateTable(
                "dbo.UserSettings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EnableSystemAlerts = c.Boolean(),
                        EnableWatchedThreadNotifications = c.Boolean(),
                        EnableNewUserNotifications = c.Boolean(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);

			AddColumn("dbo.AspNetUsers", "LastOnline", c => c.DateTime(
				nullable: false, 
				defaultValueSql: String.Format("N'{0}'", new System.Data.SqlTypes.SqlDateTime(DateTime.UtcNow).ToSqlString())));
            AddColumn("dbo.AspNetUsers", "FailedLoginAttempts", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.AspNetUsers", "Profile_ID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Profile_ID");
            AddForeignKey("dbo.AspNetUsers", "Profile_ID", "dbo.UserProfiles", "ID");
            DropColumn("dbo.AspNetUsers", "DisplayName");
            DropColumn("dbo.AspNetUsers", "Avatar_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Avatar_ID", c => c.Int());
            AddColumn("dbo.AspNetUsers", "DisplayName", c => c.String());
            DropForeignKey("dbo.UserSettings", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Profile_ID", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfiles", "Avatar_ID", "dbo.Files");
            DropIndex("dbo.UserSettings", new[] { "User_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "Profile_ID" });
            DropIndex("dbo.UserProfiles", new[] { "Avatar_ID" });
            DropColumn("dbo.AspNetUsers", "Profile_ID");
            DropColumn("dbo.AspNetUsers", "FailedLoginAttempts");
            DropColumn("dbo.AspNetUsers", "LastOnline");
            DropTable("dbo.UserSettings");
            DropTable("dbo.UserProfiles");
            CreateIndex("dbo.AspNetUsers", "Avatar_ID");
            AddForeignKey("dbo.AspNetUsers", "Avatar_ID", "dbo.Files", "ID");
        }
    }
}

namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailSupport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteSettings", "UseSSL", c => c.Boolean());
            AddColumn("dbo.SiteSettings", "SiteName", c => c.String());
            DropColumn("dbo.SiteSettings", "IsSMTP");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SiteSettings", "IsSMTP", c => c.Boolean());
            DropColumn("dbo.SiteSettings", "SiteName");
            DropColumn("dbo.SiteSettings", "UseSSL");
        }
    }
}

namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserStateRework : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserState", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.SiteSettings", "SiteDomain", c => c.String());
            AddColumn("dbo.SiteSettings", "ForceSSL", c => c.Boolean());
            DropColumn("dbo.AspNetUsers", "ConfirmedEmail");
            DropColumn("dbo.AspNetUsers", "UserStatus");
            DropColumn("dbo.SiteSettings", "SiteUrl");

			Sql("update SiteSettings set ForceSSL = 0 where Discriminator = 'GeneralSettings'");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SiteSettings", "SiteUrl", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserStatus", c => c.Int());
            AddColumn("dbo.AspNetUsers", "ConfirmedEmail", c => c.Boolean());
            DropColumn("dbo.SiteSettings", "ForceSSL");
            DropColumn("dbo.SiteSettings", "SiteDomain");
            DropColumn("dbo.AspNetUsers", "UserState");
        }
    }
}

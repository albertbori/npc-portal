namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Logging : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogMessages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Level = c.Int(nullable: false),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.SiteSettings", "MinimumLogLevel", c => c.Int());
            AddColumn("dbo.SiteSettings", "MaxLogHistoryDays", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "MaxLogHistoryDays");
            DropColumn("dbo.SiteSettings", "MinimumLogLevel");
            DropTable("dbo.LogMessages");
        }
    }
}

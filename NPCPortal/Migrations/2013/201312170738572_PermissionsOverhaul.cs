namespace NPCPortal.Migrations
{
	using NPCPortal.Models;
	using System;
	using System.Data.Entity.Migrations;
    
    public partial class PermissionsOverhaul : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RolePermissions", "PermissionsSettings_ID", "dbo.SiteSettings");
            DropIndex("dbo.RolePermissions", new[] { "PermissionsSettings_ID" });
            AddColumn("dbo.AspNetRoles", "Permissions", c => c.Long());
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.SiteSettings", "AnonymousPermissions", c => c.Long());
            AddColumn("dbo.SiteSettings", "MemberPermissions", c => c.Long());
            AlterColumn("dbo.ReputationPermissions", "PermissionType", c => c.Long(nullable: false));
			DropTable("dbo.RolePermissions");

			//Remove the old object to prevent null conflict issues (since it's a TPH schema)
			Sql("delete from ReputationPermissions");
			Sql("delete from SiteSettings where Discriminator = 'PermissionsSettings'");

			//Upgrade existing roles to have correct data
			PermissionTypes adminPermissions = 0;
			foreach (PermissionTypes permission in Role.AdminAccessPermissions)
			{
				adminPermissions |= permission;
			}
			Sql("update AspNetRoles set Discriminator = 'Role'");
			Sql("update AspNetRoles set Permissions = 0 where Permissions is null");
			Sql("update AspNetRoles set Permissions = " + (int)adminPermissions + " where Name = 'Administrators'");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RolePermissions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PermissionType = c.Int(nullable: false),
                        Role = c.String(),
                        AllowAnonymous = c.Boolean(nullable: false),
                        PermissionsSettings_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            AlterColumn("dbo.ReputationPermissions", "PermissionType", c => c.Int(nullable: false));
            DropColumn("dbo.SiteSettings", "MemberPermissions");
            DropColumn("dbo.SiteSettings", "AnonymousPermissions");
            DropColumn("dbo.AspNetRoles", "Discriminator");
            DropColumn("dbo.AspNetRoles", "Permissions");
            CreateIndex("dbo.RolePermissions", "PermissionsSettings_ID");
            AddForeignKey("dbo.RolePermissions", "PermissionsSettings_ID", "dbo.SiteSettings", "ID");
        }
    }
}

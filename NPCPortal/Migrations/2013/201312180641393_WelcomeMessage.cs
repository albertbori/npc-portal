namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WelcomeMessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteSettings", "WelcomeMessageSubject", c => c.String());
            AddColumn("dbo.SiteSettings", "WelcomeMessageBody", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "WelcomeMessageBody");
            DropColumn("dbo.SiteSettings", "WelcomeMessageSubject");
        }
    }
}

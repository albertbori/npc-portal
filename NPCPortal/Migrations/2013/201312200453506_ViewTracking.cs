namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ViewTracking : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ViewedDiscussions", "Discussion_ID", "dbo.Discussions");
            DropForeignKey("dbo.ViewedDiscussions", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ViewedDiscussions", new[] { "Discussion_ID" });
            DropIndex("dbo.ViewedDiscussions", new[] { "User_Id" });
            CreateTable(
                "dbo.ViewedContent",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ViewedOn = c.DateTime(nullable: false),
                        ViewCount = c.Int(nullable: false),
                        Content_ID = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contents", t => t.Content_ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Content_ID)
                .Index(t => t.User_Id);
            
            AddColumn("dbo.Discussions", "ViewCount", c => c.Int(nullable: false));
            DropTable("dbo.ViewedDiscussions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ViewedDiscussions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ViewedOn = c.DateTime(nullable: false),
                        Discussion_ID = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.ViewedContent", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ViewedContent", "Content_ID", "dbo.Contents");
            DropIndex("dbo.ViewedContent", new[] { "User_Id" });
            DropIndex("dbo.ViewedContent", new[] { "Content_ID" });
            DropColumn("dbo.Discussions", "ViewCount");
            DropTable("dbo.ViewedContent");
            CreateIndex("dbo.ViewedDiscussions", "User_Id");
            CreateIndex("dbo.ViewedDiscussions", "Discussion_ID");
            AddForeignKey("dbo.ViewedDiscussions", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ViewedDiscussions", "Discussion_ID", "dbo.Discussions", "ID");
        }
    }
}

// <auto-generated />
namespace NPCPortal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class UTCDates : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UTCDates));
        
        string IMigrationMetadata.Id
        {
            get { return "201312230536058_UTCDates"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

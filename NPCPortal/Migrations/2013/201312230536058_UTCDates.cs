namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UTCDates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserSettings", "TimeZone", c => c.String());
            AddColumn("dbo.UserSettings", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.SiteSettings", "TimeZone", c => c.String());

			Sql(String.Format("update SiteSettings set TimeZone = '{0}' where Discriminator = 'GeneralSettings'", TimeZoneInfo.Local.Id));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "TimeZone");
            DropColumn("dbo.UserSettings", "Discriminator");
            DropColumn("dbo.UserSettings", "TimeZone");
        }
    }
}

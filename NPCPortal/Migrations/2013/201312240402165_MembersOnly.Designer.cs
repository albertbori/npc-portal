// <auto-generated />
namespace NPCPortal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class MembersOnly : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MembersOnly));
        
        string IMigrationMetadata.Id
        {
            get { return "201312240402165_MembersOnly"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

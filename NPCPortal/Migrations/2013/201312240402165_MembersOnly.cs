namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MembersOnly : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Forums", "MembersOnly", c => c.Boolean(nullable: false, defaultValue: false));
			AddColumn("dbo.ForumCategories", "MembersOnly", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ForumCategories", "MembersOnly");
            DropColumn("dbo.Forums", "MembersOnly");
        }
    }
}

// <auto-generated />
namespace NPCPortal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class Favicon : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Favicon));
        
        string IMigrationMetadata.Id
        {
            get { return "201312240621590_Favicon"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

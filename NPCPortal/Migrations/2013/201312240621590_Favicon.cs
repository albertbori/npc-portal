namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Favicon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteSettings", "Favicon", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "Favicon");
        }
    }
}

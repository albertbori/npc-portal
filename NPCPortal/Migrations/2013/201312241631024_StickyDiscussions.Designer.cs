// <auto-generated />
namespace NPCPortal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class StickyDiscussions : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(StickyDiscussions));
        
        string IMigrationMetadata.Id
        {
            get { return "201312241631024_StickyDiscussions"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

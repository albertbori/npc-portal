namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StickyDiscussions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discussions", "IsSticky", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Discussions", "IsSticky");
        }
    }
}

namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForgotPassword : DbMigration
    {
        public override void Up()
        {
			AddColumn("dbo.AspNetUsers", "ForgotPasswordCode", c => c.String(nullable: true));
            AddColumn("dbo.AspNetUsers", "ForgotPasswordDate", c => c.DateTime(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ForgotPasswordDate");
            DropColumn("dbo.AspNetUsers", "ForgotPasswordCode");
        }
    }
}

namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewsAndUpdates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        Arguments = c.String(),
                        Status = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateCompleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.UserSettings", "EnableNewsAndUpdates", c => c.Boolean());

			Sql("update UserSettings set EnableNewsAndUpdates = 1 where Discriminator = 'UserEmailSettings'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserSettings", "EnableNewsAndUpdates");
            DropTable("dbo.Jobs");
        }
    }
}

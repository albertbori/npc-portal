namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Messages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MessageRecipients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DateRead = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                        Message_ID = c.Int(),
                        Recipient_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Messages", t => t.Message_ID)
                .ForeignKey("dbo.AspNetUsers", t => t.Recipient_Id)
                .Index(t => t.Message_ID)
                .Index(t => t.Recipient_Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        InReplyTo_ID = c.Int(),
                        Subject = c.String(),
                        Body = c.String(),
                        DateSent = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contents", t => t.ID)
                .ForeignKey("dbo.Messages", t => t.InReplyTo_ID)
                .Index(t => t.ID)
                .Index(t => t.InReplyTo_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "InReplyTo_ID", "dbo.Messages");
            DropForeignKey("dbo.Messages", "ID", "dbo.Contents");
            DropForeignKey("dbo.MessageRecipients", "Recipient_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.MessageRecipients", "Message_ID", "dbo.Messages");
            DropIndex("dbo.Messages", new[] { "InReplyTo_ID" });
            DropIndex("dbo.Messages", new[] { "ID" });
            DropIndex("dbo.MessageRecipients", new[] { "Recipient_Id" });
            DropIndex("dbo.MessageRecipients", new[] { "Message_ID" });
            DropTable("dbo.Messages");
            DropTable("dbo.MessageRecipients");
        }
    }
}

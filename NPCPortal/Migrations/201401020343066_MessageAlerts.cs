namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MessageAlerts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserSettings", "EnableMessageAlerts", c => c.Boolean());

			Sql("update UserSettings set EnableMessageAlerts = 1 where Discriminator = 'UserEmailSettings'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserSettings", "EnableMessageAlerts");
        }
    }
}

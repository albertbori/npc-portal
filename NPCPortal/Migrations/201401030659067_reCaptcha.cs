namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reCaptcha : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteSettings", "PublicKey", c => c.String());
            AddColumn("dbo.SiteSettings", "PrivateKey", c => c.String());
            AddColumn("dbo.SiteSettings", "Theme", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "Theme");
            DropColumn("dbo.SiteSettings", "PrivateKey");
            DropColumn("dbo.SiteSettings", "PublicKey");
        }
    }
}

namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RegistrationApprovals : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IPAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IPAddress");
        }
    }
}

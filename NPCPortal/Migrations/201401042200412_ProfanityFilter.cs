namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProfanityFilter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserSettings", "FilterProfanity", c => c.Boolean());
            AddColumn("dbo.SiteSettings", "FilterProfanity", c => c.Boolean());
			Sql("update UserSettings set FilterProfanity = 1 where Discriminator = 'UserGeneralSettings'");
			Sql("update SiteSettings set FilterProfanity = 1 where Discriminator = 'ModerationSettings'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "FilterProfanity");
            DropColumn("dbo.UserSettings", "FilterProfanity");
        }
    }
}

// <auto-generated />
namespace NPCPortal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class CustomBanner : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CustomBanner));
        
        string IMigrationMetadata.Id
        {
            get { return "201401080735140_CustomBanner"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

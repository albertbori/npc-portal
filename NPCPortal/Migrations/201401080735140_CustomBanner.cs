namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomBanner : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteSettings", "BannerImage", c => c.String());
            AddColumn("dbo.SiteSettings", "ShowBannerImageEverywhere", c => c.Boolean());
            AddColumn("dbo.SiteSettings", "BannerHTML", c => c.String());
            AddColumn("dbo.SiteSettings", "ShowBannerHTMLEverywhere", c => c.Boolean());

			Sql("update SiteSettings set ShowBannerImageEverywhere = 0, ShowBannerHTMLEverywhere = 0 where Discriminator = 'GeneralSettings'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "ShowBannerHTMLEverywhere");
            DropColumn("dbo.SiteSettings", "BannerHTML");
            DropColumn("dbo.SiteSettings", "ShowBannerImageEverywhere");
            DropColumn("dbo.SiteSettings", "BannerImage");
        }
    }
}

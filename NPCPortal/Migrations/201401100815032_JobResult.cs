namespace NPCPortal.Migrations
{
	using NPCPortal.Models;
	using System;
	using System.Data.Entity.Migrations;
    
    public partial class JobResult : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "ResultMessage", c => c.String());

			Sql(String.Format("update Jobs set Status = {0} where Status = {1}", (int)JobStatuses.Failed, (int)JobStatuses.Pending));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jobs", "ResultMessage");
        }
    }
}

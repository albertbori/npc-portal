namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BannerRefactor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteSettings", "ShowBannerEverywhere", c => c.Boolean());
            DropColumn("dbo.SiteSettings", "ShowBannerImageEverywhere");
            DropColumn("dbo.SiteSettings", "ShowBannerHTMLEverywhere");

			Sql("update SiteSettings set ShowBannerEverywhere = 0 where Discriminator = 'GeneralSettings'");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SiteSettings", "ShowBannerHTMLEverywhere", c => c.Boolean());
            AddColumn("dbo.SiteSettings", "ShowBannerImageEverywhere", c => c.Boolean());
            DropColumn("dbo.SiteSettings", "ShowBannerEverywhere");
        }
    }
}

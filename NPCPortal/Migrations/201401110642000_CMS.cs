namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CMS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NavMenuItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        URL = c.String(),
                        OpenInNewWindow = c.Boolean(nullable: false),
                        MembersOnly = c.Boolean(nullable: false),
                        ViewRole = c.String(),
                        Permission = c.Long(),
                        Parent_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.NavMenuItems", t => t.Parent_ID)
                .Index(t => t.Parent_ID);
            
            CreateTable(
                "dbo.CMSPages",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Title = c.String(),
                        Body = c.String(),
                        MembersOnly = c.Boolean(nullable: false),
                        ViewRole = c.String(),
                        EditRole = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contents", t => t.ID)
                .Index(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CMSPages", "ID", "dbo.Contents");
            DropForeignKey("dbo.NavMenuItems", "Parent_ID", "dbo.NavMenuItems");
            DropIndex("dbo.CMSPages", new[] { "ID" });
            DropIndex("dbo.NavMenuItems", new[] { "Parent_ID" });
            DropTable("dbo.CMSPages");
            DropTable("dbo.NavMenuItems");
        }
    }
}

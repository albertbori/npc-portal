namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CMSHTML : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CMSPages", "IsHTML", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CMSPages", "IsHTML");
        }
    }
}

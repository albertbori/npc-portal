namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RegistrationQuestions : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RegistrationQuestions", "RegistrationSettings_ID", "dbo.SiteSettings");
            DropIndex("dbo.RegistrationQuestions", new[] { "RegistrationSettings_ID" });
            CreateTable(
                "dbo.RegistrationAnswers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        Answer = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            AddColumn("dbo.RegistrationQuestions", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.RegistrationQuestions", "Required", c => c.Boolean(nullable: false));
            AddColumn("dbo.RegistrationQuestionAnswers", "AnswerText", c => c.String());
            CreateIndex("dbo.RegistrationQuestions", "RegistrationSettings_ID");
            AddForeignKey("dbo.RegistrationQuestions", "RegistrationSettings_ID", "dbo.SiteSettings", "ID");
            DropColumn("dbo.RegistrationQuestionAnswers", "Text");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RegistrationQuestionAnswers", "Text", c => c.String());
            DropForeignKey("dbo.RegistrationQuestions", "RegistrationSettings_ID", "dbo.SiteSettings");
            DropForeignKey("dbo.RegistrationAnswers", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.RegistrationQuestions", new[] { "RegistrationSettings_ID" });
            DropIndex("dbo.RegistrationAnswers", new[] { "User_Id" });
            DropColumn("dbo.RegistrationQuestionAnswers", "AnswerText");
            DropColumn("dbo.RegistrationQuestions", "Required");
            DropColumn("dbo.RegistrationQuestions", "Type");
            DropTable("dbo.RegistrationAnswers");
            CreateIndex("dbo.RegistrationQuestions", "RegistrationSettings_ID");
            AddForeignKey("dbo.RegistrationQuestions", "RegistrationSettings_ID", "dbo.SiteSettings", "ID");
        }
    }
}

namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Analytics : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteSettings", "Snippet", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteSettings", "Snippet");
        }
    }
}

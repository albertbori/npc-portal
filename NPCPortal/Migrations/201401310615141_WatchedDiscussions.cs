namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WatchedDiscussions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScheduledJobs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Arguments = c.String(),
                        TimeTicks = c.Long(nullable: false),
                        LastRun = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            AlterColumn("dbo.UserSettings", "EnableWatchedThreadNotifications", c => c.Int());
            DropColumn("dbo.UserSettings", "EnableNewUserNotifications");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserSettings", "EnableNewUserNotifications", c => c.Boolean());
            AlterColumn("dbo.UserSettings", "EnableWatchedThreadNotifications", c => c.Boolean());
            DropTable("dbo.ScheduledJobs");
        }
    }
}

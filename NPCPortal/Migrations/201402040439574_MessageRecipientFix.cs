namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MessageRecipientFix : DbMigration
    {
        public override void Up()
        {
			Sql("delete from MessageRecipients where Message_ID is null");
        }
        
        public override void Down()
        {
        }
    }
}

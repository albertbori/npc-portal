namespace NPCPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LoginSecurity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoginAttempts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IPAddress = c.String(),
                        FailedCount = c.Int(nullable: false),
                        LastAttempt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropColumn("dbo.AspNetUsers", "FailedLoginAttempts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "FailedLoginAttempts", c => c.Int());
            DropTable("dbo.LoginAttempts");
        }
    }
}

namespace NPCPortal.Migrations
{
	using Microsoft.AspNet.Identity;
	using Microsoft.AspNet.Identity.EntityFramework;
	using NPCPortal.Models;
	using System;
	using System.Collections.ObjectModel;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;

	internal sealed class Configuration : DbMigrationsConfiguration<NPCPortal.Models.NPCPortalDB>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
		}

		protected override void Seed(NPCPortal.Models.NPCPortalDB db)
		{
			RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new NPCPortalDB()));

			if(db.Roles.Count() == 0)
			{
				PermissionTypes adminPermissions = 0;
				foreach (PermissionTypes permission in Enum.GetValues(typeof(PermissionTypes)).Cast<PermissionTypes>())
				{
					adminPermissions |= permission;
				}
				roleManager.Create(new Role("Administrators") { Permissions = adminPermissions });
				roleManager.Create(new Role("Moderators") { Permissions = PermissionTypes.ModeratePosts });
			}

			if (db.Users.Count() == 0)
			{
				UserManager<User> userManager = new UserManager<User>(new UserStore<User>(new NPCPortalDB()));

				userManager.Create(
					new User()
					{
						UserName = "Administrator",
						DateJoined = DateTime.UtcNow,
						Profile = new UserProfile() { DisplayName ="Admin Strator" }
					},
					"zeropants!");

				var admin = db.Users.First();

				userManager.AddToRole(admin.Id, "Administrators");
			}

			if(db.SiteSettings.OfType<PermissionsSettings>().Count() == 0)
			{
				PermissionsSettings permissions = new PermissionsSettings()
				{
					AnonymousPermissions = PermissionTypes.ViewForums,
					MemberPermissions = PermissionTypes.CreatePosts | PermissionTypes.Flag | PermissionTypes.Upload | PermissionTypes.ViewFiles | PermissionTypes.ViewGallery | PermissionTypes.ViewMembers | PermissionTypes.Vote
				};

				db.SiteSettings.Add(permissions);
			}

			if(db.SiteSettings.OfType<RegistrationSettings>().Count() == 0)
			{
				db.SiteSettings.Add(new RegistrationSettings());
			}

			if(db.SiteSettings.OfType<LogSettings>().Count() == 0)
			{
				db.SiteSettings.Add(new LogSettings() { MinimumLogLevel = LogLevel.Info, MaxLogHistoryDays = 30 });
			}
			
			if(db.ForumCategories.Count() == 0 && db.Forums.Count() == 0)
			{
				var admin = db.Users.First();

				db.ForumCategories.Add(new Models.ForumCategory()
					{
						Name = "Public Forums",
						Description = "These forums will be viewable by all folks",
						Forums = new Collection<Forum>() {
							new Forum() {
								Name = "News",
								Description = "This forum is for news for anyone.",
								Discussions = new Collection<Discussion>() {
									new Discussion()
									{
										Author = admin,
										Title = "Welcome to the new No Pants Guild Site!",
										Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pulvinar vehicula ipsum quis varius. Suspendisse potenti. Nulla facilisi. Nunc tempus odio quis nulla ornare, vitae dignissim diam pulvinar. Maecenas consequat, urna id luctus mattis, metus est porttitor libero, eget rhoncus diam sem sit amet nibh. Maecenas id dui justo. Suspendisse non sapien et leo luctus volutpat.",
										IsFeatured = true
									}
								}
							},
							new Forum() {
								Name = "Recruitment",
								Description = "This forum is for guild applications, etc."
							}
						}
					});

				db.ForumCategories.Add(new Models.ForumCategory()
					{
						Name = "Member Forums",
						Description = "These forums will be viewable by members only",
						Forums = new Collection<Forum>() {
							new Forum() {
								Name = "Raiding",
								Description = "This forum is for raiding discussion"
							}
						}
					});
			}

			if(db.NavMenuItems.Count() == 0)
			{
				db.NavMenuItems.Add(new NavMenuItem()
				{
					Title = "Forums",
					URL = "/Forums/",
					Permission = PermissionTypes.ViewForums
				});
				db.NavMenuItems.Add(new NavMenuItem()
				{
					Title = "Members",
					URL = "/Members/",
					Permission = PermissionTypes.ViewMembers
				});
				db.NavMenuItems.Add(new NavMenuItem()
				{
					Title = "Gallery",
					URL = "/Files/Gallery/",
					Permission = PermissionTypes.ViewGallery
				});
				db.NavMenuItems.Add(new NavMenuItem()
				{
					Title = "Files",
					URL = "/Files/",
					Permission = PermissionTypes.ViewFiles
				});
			}

			if(db.ScheduledJobs.Count() == 0)
			{
				var frequentContentRollup = new NPCPortal.Jobs.WatchedDiscussionRollupEmailBurster() { EmailFrequency = EmailFrequency.Frequent };
				db.ScheduledJobs.Add(new ScheduledJob()
				{
					Name = frequentContentRollup.GetType().AssemblyQualifiedName,
					Arguments = Newtonsoft.Json.JsonConvert.SerializeObject(frequentContentRollup),
					Interval = new TimeSpan(1, 0, 0)
				});

				var dailyContentRollup = new NPCPortal.Jobs.WatchedDiscussionRollupEmailBurster() { EmailFrequency = EmailFrequency.Daily };
				db.ScheduledJobs.Add(new ScheduledJob()
				{
					Name = dailyContentRollup.GetType().AssemblyQualifiedName,
					Arguments = Newtonsoft.Json.JsonConvert.SerializeObject(dailyContentRollup),
					Interval = new TimeSpan(1, 0, 0, 0)
				});
			}

			//  This method will be called after migrating to the latest version.

			//  You can use the DbSet<T>.AddOrUpdate() helper extension method 
			//  to avoid creating duplicate seed data. E.g.
			//
			//    context.People.AddOrUpdate(
			//      p => p.FullName,
			//      new Person { FullName = "Andrew Peters" },
			//      new Person { FullName = "Brice Lambson" },
			//      new Person { FullName = "Rowan Miller" }
			//    );
			//
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Models
{
	public class DropdownListAttribute : System.Attribute
	{
		public IEnumerable<SelectListItem> Items { get; internal set; }
		public DropdownListAttribute(IEnumerable<SelectListItem> selectListItems)
		{
			Items = selectListItems;
		}

		public DropdownListAttribute(IEnumerable<string> selectListItems)
		{
			Items = selectListItems.Select(i => new SelectListItem() { Text = i, Value = i });
		}

		public DropdownListAttribute(IEnumerable<object> items, string textPropertyName, string valuePropertyName)
		{
			Items = items.Select(i => new SelectListItem() 
			{
				Text = i.GetType().GetProperty(textPropertyName).GetValue(i).ToString(),
				Value = i.GetType().GetProperty(valuePropertyName).GetValue(i).ToString() 
			});
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class FileUploadAttribute : System.Attribute
	{
		public string RelativeDirectory { get; private set; }
		public string FileName { get; private set; }
		public string[] FileExtensions { get; private set; }

		/// <summary>
		/// Flags file-related property with the relative directory for saving
		/// </summary>
		/// <param name="relativeDirectory">ex: "~/SomeRelativePath"</param>
		public FileUploadAttribute(string relativeDirectory)
		{
			this.RelativeDirectory = relativeDirectory;
		}

		/// <summary>
		/// Flags file-related property with the relative directory for saving as well as a static file name to save the file as.
		/// </summary>
		/// <param name="relativeDirectory">ex: "~/SomeRelativePath"</param>
		/// <param name="fileName">A file name with or without extension. ex: anything[.jpg]</param>
		public FileUploadAttribute(string relativeDirectory, string fileName)
			: this(relativeDirectory)
		{
			this.FileName = fileName;
		}

		/// <summary>
		/// Flags file-related property with the relative directory for saving as well as an option to generate a GUID file name for the file to save as.
		/// </summary>
		/// <param name="relativeDirectory">ex: "~/SomeRelativePath"</param>
		/// <param name="autogenerateFileName"></param>
		public FileUploadAttribute(string relativeDirectory, bool autogenerateFileName)
			: this(relativeDirectory)
		{
			if (autogenerateFileName)
				this.FileName = Guid.NewGuid().ToString();
		}

		/// <summary>
		/// Flags file-related property with the relative directory for saving as well as 
		/// </summary>
		/// <param name="relativeDirectory">ex: "~/SomeRelativePath"</param>
		/// <param name="fileName">A file name with or without extension. ex: anything[.jpg]</param>
		/// <param name="acceptableFileExtensions">ex new string[] { ".jpg", ".gif", ".png" }</param>
		public FileUploadAttribute(string relativeDirectory, string fileName, string[] acceptableFileExtensions)
			: this(relativeDirectory, fileName)
		{
			this.FileExtensions = acceptableFileExtensions;
		}

		/// <summary>
		/// Flags file-related property with the relative directory for saving as well as 
		/// </summary>
		/// <param name="relativeDirectory">ex: "~/SomeRelativePath"</param>
		/// <param name="acceptableFileExtensions">ex new string[] { ".jpg", ".gif", ".png" }</param>
		public FileUploadAttribute(string relativeDirectory, string[] acceptableFileExtensions)
			: this(relativeDirectory)
		{
			this.FileExtensions = acceptableFileExtensions;
		}
	}
}
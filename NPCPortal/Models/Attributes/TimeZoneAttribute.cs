﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Models
{
	public class TimeZoneAttribute : DropdownListAttribute
	{
		public TimeZoneAttribute() :
			base(TimeZoneInfo.GetSystemTimeZones(), "DisplayName", "Id") { }
	}
}
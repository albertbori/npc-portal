﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Table("CMSPages")]
	public class CMSPage : Content
	{
		public string Title { get; set; }
		public string Body { get; set; }

		[Display(Name = "Members Only")]
		public bool MembersOnly { get; set; }

		[Display(Name = "View Role")]
		public string ViewRole { get; set; }

		[Display(Name = "Edit Role")]
		public string EditRole { get; set; }

		[Display(Name = "HTML Formatted")]
		public bool IsHTML { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    public class Content
    {
        public int ID { get; set; }
        public virtual User Author { get; set; }

		[Display(Name = "Moderation Status")]
		public ModerationStatuses ModerationStatus { get; set; }

		[Display(Name = "Date Created")]
		[DateTimeKind(DateTimeKind.Utc)]
        public DateTime DateCreated { get; set; }

        private ICollection<User> _usersLikeThis;
        public virtual ICollection<User> UsersLikeThis
        {
            get { return _usersLikeThis ?? (_usersLikeThis = new Collection<User>()); }
            set { _usersLikeThis = value; }
        }
		public Content()
		{
			this.DateCreated = DateTime.UtcNow;
		}

		public virtual void Delete(NPCPortalDB db)
		{
			foreach(var userLiked in this.UsersLikeThis.ToArray())
			{
				this.UsersLikeThis.Remove(userLiked);
			}

			foreach(var viewedContent in db.ViewedContent.Where(c => c.Content.ID == this.ID).ToArray())
			{
				db.ViewedContent.Remove(viewedContent);
			}
			db.SaveChanges();

			db.Content.Remove(this);
			db.SaveChanges();
		}
    }
}
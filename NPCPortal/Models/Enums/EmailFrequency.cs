﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public enum EmailFrequency
	{
		[Description("Do not notify me")]
		None = 1,
		[Description("Notify me immediately")]
		Instant = 2,
		[Description("Notify me frequently")]
		Frequent = 3,
		[Description("Notify me daily")]
		Daily = 4
	}
}
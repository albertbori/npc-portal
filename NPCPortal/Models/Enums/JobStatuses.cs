﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPCPortal.Models
{
	public enum JobStatuses
	{
		Pending = 0,
		Running = 1,
		Holding = 2,
		Completed = 3,
		Failed = 4
	}
}

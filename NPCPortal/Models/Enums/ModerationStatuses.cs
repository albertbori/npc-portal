﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPCPortal.Models
{
    public enum ModerationStatuses
    {
        Unmoderated = 1,
        Approved = 2,
        Flagged = 3,
        Rejected = 4,
		Spam = 5
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NPCPortal.Models
{
	[Flags]
	public enum PermissionTypes : long
	{
		[Description("View Forums")]
		ViewForums = 1L << 0,
		[Description("Flag Posts")]
		Flag = 1L << 1,
		[Description("Create Posts")]
		CreatePosts = 1L << 2,
		[Description("Edit Posts")]
		EditPosts = 1L << 3,
		[Description("Delete Posts")]
		DeletePosts = 1L << 4,
		[Description("Moderate Posts")]
		ModeratePosts = 1L << 5,
		[Description("View Files")]
		ViewFiles = 1L << 6,
		[Description("View Gallery")]
		ViewGallery = 1L << 7,
		[Description("Upload Files and Images")]
		Upload = 1L << 8,
		[Description("Vote")]
		Vote = 1L << 9,
		[Description("Create Forums")]
		CreateForums = 1L << 10,
		[Description("Edit Forums")]
		EditForums = 1L << 11,
		[Description("Delete Forums")]
		DeleteForums = 1L << 12,
		[Description("View Members")]
		ViewMembers = 1L << 13,
		[Description("Edit Members Profiles")]
		EditProfiles = 1L << 14,
		[Description("Edit Users")]
		EditUsers = 1L << 15,
		[Description("Delete Users")]
		DeleteUsers = 1L << 16,
		[Description("Create Roles")]
		CreateRoles = 1L << 17,
		[Description("Edit Roles")]
		EditRoles = 1L << 18,
		[Description("Delete Roles")]
		DeleteRoles = 1L << 19,
		[Description("Create Themes")]
		CreateThemes = 1L << 20,
		[Description("Switch Themes")]
		SwitchThemes = 1L << 21,
		[Description("Delete Themes")]
		DeleteThemes = 1L << 22,
		[Description("Send Broadcasts")]
		SendBroadcasts = 1L << 23,
		[Description("View Logs")]
		ViewLogs = 1L << 24,
		[Description("Edit General Settings")]
		EditGeneralSettings = 1L << 25,
		[Description("Feature Discussions")]
		FeatureDiscussions = 1L << 26,
		[Description("Lock Discussions")]
		LockDiscussions = 1L << 27,
		[Description("Sticky Discussions")]
		StickyDiscussions = 1L << 28,
		[Description("View Jobs")]
		ViewJobs = 1L << 29,
		[Description("Run Jobs")]
		RunJobs = 1L << 30,
		[Description("View Messages")]
		ViewMessages = 1L << 31,
		[Description("Send Messages")]
		SendMessages = 1L << 32,
		[Description("Move Discussions")]
		MoveDiscussions = 1L << 33,
		[Description("Approve Users")]
		ApproveUsers = 1L << 34,
		[Description("Create Pages")]
		CreatePages = 1L << 35,
		[Description("Edit Pages")]
		EditPages = 1L << 36,
		[Description("Delete Pages")]
		DeletePages = 1L << 37,
		[Description("Edit Navigation")]
		EditNav = 1L << 38
	}
}

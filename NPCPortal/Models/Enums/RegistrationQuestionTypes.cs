﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public enum RegistrationQuestionTypes
	{
		[Description("Dropdown")]
		DropDownList,
		[Description("List")]
		MultiSelect,
		[Description("Text")]
		TextBox,
		[Description("Essay")]
		TextArea,
		[Description("Yes/No")]
		CheckBox
	}
}
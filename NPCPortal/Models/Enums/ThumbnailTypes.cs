﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public enum ThumbnailTypes
	{
		Avatar,
		Thumbnail,
		WideThumbnail
	}	
}
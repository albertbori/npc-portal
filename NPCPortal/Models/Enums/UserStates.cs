﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Flags]
	public enum UserStates
	{
		Unknown = 0,
		[Description("Email Validated")]
		EmailValidated = 1 << 0,
		Approved = 1 << 1,
		Banned = 1 << 2,
		Spammer = 1 << 3,
		Rejected = 1 << 4
	}
}
﻿using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    [Table("Files")]
    public class File : Content
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public long FileSize { get; set; }
        public string FileType { get; set; }

        private ICollection<ForumPost> _attachedToForumPosts { get; set; }
        public virtual ICollection<ForumPost> AttachedToForumPosts
        {
            get { return _attachedToForumPosts ?? (_attachedToForumPosts = new Collection<ForumPost>()); }
            set { _attachedToForumPosts = value; }
        }

		public bool IsImage
		{
			get { return this.FileType != null && this.FileType.ToLower().StartsWith("image"); }
		}

		public string URL
		{
			get { return this.IsImage ? FileStore.GetImageURL(this.FileName) : FileStore.GetFileURL(this.FileName); }
		}

		public string GetThumbnailURL(ThumbnailTypes thumbnailType)
		{
			if (!this.IsImage)
				return null; //TODO: get some default images for known file-types (zip, 

			return FileStore.GetThumbnailURL(this.FileName, thumbnailType);
		}

		public override void Delete(NPCPortalDB db)
		{
			if (this.IsImage)
				FileStore.DeleteImage(this.FileName);
			else
				FileStore.DeleteFile(this.FileName);

			var avatarProfiles = db.UserProfiles.Where(p => p.Avatar != null && p.Avatar.ID == this.ID).ToArray();
			foreach(var profile in avatarProfiles)
			{
				profile.Avatar = null;
			}

			var attachedTo = this.AttachedToForumPosts.ToArray();
			foreach(var forumPost in attachedTo)
			{
				forumPost.Attachments.Remove(this);
			}
			
			db.SaveChanges();

			base.Delete(db);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    [Table("Discussions")]
    public class Discussion : ForumPost
    {
        public string Title { get; set; }

		[Display(Name="Featured")]
		public bool IsFeatured { get; set; }

		[Display(Name = "Locked")]
		public bool IsLocked { get; set; }

		[Display(Name = "Sticky")]
		public bool IsSticky { get; set; }
		public int ViewCount { get; set; }

        public virtual Forum Forum { get; set; }

        private ICollection<User> _watchingUsers;
        public virtual ICollection<User> WatchingUsers
        {
            get { return _watchingUsers ?? (_watchingUsers = new Collection<User>()); }
            set { _watchingUsers = value; }
        }

		private ICollection<Reply> _allReplies;
		public virtual ICollection<Reply> AllReplies
		{
			get { return _allReplies ?? (_allReplies = new Collection<Reply>()); }
			set { _allReplies = value; }
		}

		public Discussion()
		{
			this.ViewCount = 1;
		}

		public DateTime LastActivityDate
		{
			get
			{
				DateTime lastActivity = this.AllReplies.OrderByDescending(r => r.DateCreated).Select(r => r.DateCreated).FirstOrDefault();
				if (lastActivity < this.DateCreated)
					lastActivity = this.DateCreated;
				return lastActivity;
			}
		}
    }
}
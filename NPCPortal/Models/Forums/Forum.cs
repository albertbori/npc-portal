﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    [Table("Forums")]
    public class Forum
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
		[Display(Name="View Role")]
        public string ViewRole { get; set; }
		[Display(Name="Post Role")]
		public string PostRole { get; set; }

		[Display(Name = "Members Only")]
		public bool MembersOnly { get; set; }

        public virtual ForumCategory Category { get; set; }
        
        private ICollection<Discussion> _discussions { get; set; }
        public virtual ICollection<Discussion> Discussions
        {
            get { return _discussions ?? (_discussions = new Collection<Discussion>()); }
            set { _discussions = value; }
        }

		public void Delete(NPCPortalDB db)
		{
			foreach(var discussion in this.Discussions.ToArray())
			{
				discussion.Delete(db);
			}

			db.Forums.Remove(this);

			db.SaveChanges();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    public class ForumCategory
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [Display(Name="Restricted To Role")]
        public string RestrictedToRole { get; set; }

		[Display(Name = "Members Only")]
		public bool MembersOnly { get; set; }

        private ICollection<Forum> _forums { get; set; }
        public virtual ICollection<Forum> Forums
        {
            get { return _forums ?? (_forums = new Collection<Forum>()); }
            set { _forums = value; }
        }

		[Display(Name="Parent Category")]
        public virtual ForumCategory ParentCategory { get; set; }

        private ICollection<ForumCategory> _childCategories { get; set; }
        public virtual ICollection<ForumCategory> ChildCategories
        {
            get { return _childCategories ?? (_childCategories = new Collection<ForumCategory>()); }
            set { _childCategories = value; }
        }

		public void Delete(NPCPortalDB db)
		{
			foreach(var forum in this.Forums.ToArray())
			{
				forum.Delete(db);
			}

			db.ForumCategories.Remove(this);

			db.SaveChanges();
		}
    }
}
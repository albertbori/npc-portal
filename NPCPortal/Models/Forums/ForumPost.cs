﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    [Table("ForumPosts")]
    public abstract class ForumPost : Content
    {
		public string Text { get; set; }

        private ICollection<Reply> _replies;
		public virtual ICollection<Reply> Replies
        {
			get { return _replies ?? (_replies = new Collection<Reply>()); }
            set { _replies = value; }
        }

        private ICollection<File> _attachments;
        public virtual ICollection<File> Attachments
        {
            get { return _attachments ?? (_attachments = new Collection<File>()); }
            set { _attachments = value; }
        }

		public override void Delete(NPCPortalDB db)
		{
			var files = this.Attachments.ToArray();
			foreach (var file in files)
			{
				db.Files.Remove(file);
			}

			var replies = this.Replies.ToArray();
			foreach (var reply in replies)
			{
				reply.Delete(db);
			}

			base.Delete(db);
		}
    }
}
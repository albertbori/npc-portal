﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Table("Replies")]
	public class Reply : ForumPost
	{
		public virtual Discussion Discussion { get; set; }
		public virtual ForumPost InReplyTo { get; set; }
	}
}
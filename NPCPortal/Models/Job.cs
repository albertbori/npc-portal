﻿using NPCPortal.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class Job
	{
		[Key]
		public Guid ID { get; set; }

		public string Name { get; set; }

		[MaxLength]
		public string Arguments { get; set; }

		public JobStatuses Status { get; set; }

		[Display(Name = "Result Message")]
		public string ResultMessage { get; set; }

		[Display(Name="Date Created")]
		public DateTime DateCreated { get; set; }

		[Display(Name = "Date Completed")]

		public DateTime? DateCompleted { get; set; }

		public Type Type
		{
			get { return Type.GetType(this.Name); }
		}

		public Job()
		{
			this.DateCreated = DateTime.UtcNow;
			this.Status = JobStatuses.Pending;
		}

		public Job(IJob job) : this()
		{
			this.ID = job.ID;
			this.Name = job.GetType().AssemblyQualifiedName;
			this.Arguments = Newtonsoft.Json.JsonConvert.SerializeObject(job);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class LogMessage
	{
		public int ID { get; set; }
		[DateTimeKind(DateTimeKind.Utc)]
		public DateTime Date { get; set; }
		public LogLevel Level { get; set; }
		public string Message { get; set; }
	}
}
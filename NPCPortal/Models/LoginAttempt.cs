﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    public class LoginAttempt
    {
        public int ID { get; set; }
        public string IPAddress { get; set; }
        public int FailedCount { get; set; }
        public DateTime LastAttempt { get; set; }

        public LoginAttempt()
        {
            this.LastAttempt = DateTime.Now;
        }
    }
}
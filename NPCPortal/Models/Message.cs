﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Table("Messages")]
	public class Message : Content
	{
		public string Subject { get; set; }
		public string Body { get; set; }
		public DateTime? DateSent { get; set; }

		private ICollection<MessageRecipient> _to;
		public virtual ICollection<MessageRecipient> To
		{
			get { return _to ?? (_to = new Collection<MessageRecipient>()); }
			set { _to = value; }
		}
		public virtual Message InReplyTo { get; set; }

		private ICollection<Message> _replies;
		public virtual ICollection<Message> Replies
		{
			get { return _replies ?? (_replies = new Collection<Message>()); }
			set { _replies = value; }
		}

		public override void Delete(NPCPortalDB db)
		{
			foreach (var recipient in this.To.ToArray())
			{
				db.MessageRecipients.Remove(recipient);
			}
			db.SaveChanges();

			foreach(var reply in this.Replies.ToArray())
			{
				reply.Delete(db);
			}

			base.Delete(db);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class MessageRecipient
	{
		public int ID { get; set; }
		public DateTime? DateRead { get; set; }
		public bool Deleted { get; set; }
		public virtual User Recipient { get; set; }
		public virtual Message Message { get; set; }
	}
}
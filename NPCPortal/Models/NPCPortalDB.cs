﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
    public class NPCPortalDB : IdentityDbContext<User>
    {
		public DbSet<NavMenuItem> NavMenuItems { get; set; }
		public DbSet<CMSPage> CMSPages { get; set; }
        public DbSet<Content> Content { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Forum> Forums { get; set; }
        public DbSet<ForumCategory> ForumCategories { get; set; }
		public DbSet<ForumPost> ForumPosts { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<LoginAttempt> LoginAttempts { get; set; }
		public DbSet<LogMessage> LogMessages { get; set; }
		public DbSet<Message> Messages { get; set; }
		public DbSet<MessageRecipient> MessageRecipients { get; set; }
		public DbSet<ScheduledJob> ScheduledJobs { get; set; }
		public DbSet<SiteSetting> SiteSettings { get; set; }
		public DbSet<Theme> Themes { get; set; }
		public DbSet<UserProfile> UserProfiles { get; set; }
		public DbSet<UserSetting> UserSettings { get; set; }
		public DbSet<ViewedContent> ViewedContent { get; set; }
		public DbSet<RegistrationAnswer> RegistrationAnswers { get; set; }

        public NPCPortalDB()
            : base("NPCPortal")
		{
			//Wire up all DateTimes with the DateTimeKindAttribute to get pulled out as UTC
			((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized +=
				(sender, e) => DateTimeKindAttribute.Apply(e.Entity);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //configure many-to-many relationship for User Likes
            modelBuilder.Entity<User>()
                .HasMany(u => u.LikedContent)
                .WithMany(c => c.UsersLikeThis)
                .Map(x =>
                {
                    x.ToTable("LikedContent");
                    x.MapLeftKey("User_ID");
                    x.MapRightKey("Content_ID");
                });

            //prevent cyclical dependency between content and user
            modelBuilder.Entity<Content>()
                .HasRequired(c => c.Author)
                .WithMany(u => u.AuthoredContent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reply>()
                .HasOptional(p => p.InReplyTo)
                .WithMany(p => p.Replies);

            //configure many-to-many relationship for forum post file attachments
            modelBuilder.Entity<ForumPost>()
                .HasMany(p => p.Attachments)
                .WithMany(f => f.AttachedToForumPosts)
                .Map(x =>
                {
                    x.ToTable("ForumPostAttachments");
                    x.MapLeftKey("ForumPost_ID");
                    x.MapRightKey("File_ID");
                });

            modelBuilder.Entity<User>()
                .HasMany(u => u.WatchedDiscussions)
                .WithMany(d => d.WatchingUsers)
                .Map(x =>
                {
                    x.ToTable("WatchedDiscussions");
                    x.MapLeftKey("User_ID");
                    x.MapRightKey("Discussion_ID");
                });
        }
    }
}
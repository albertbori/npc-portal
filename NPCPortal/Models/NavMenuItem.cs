﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class NavMenuItem
	{
		public int ID { get; set; }
		public string Title { get; set; }
		public string URL { get; set; }

		[Display(Name = "Open link in new window")]
		public bool OpenInNewWindow { get; set; }

		[Display(Name = "Members Only")]
		public bool MembersOnly { get; set; }

		[Display(Name = "View Role")]
		public string ViewRole { get; set; }

		public PermissionTypes? Permission { get; set; }

		public virtual NavMenuItem Parent { get; set; }

		private ICollection<NavMenuItem> _children;
		public virtual ICollection<NavMenuItem> Children
		{
			get { return _children ?? (_children = new Collection<NavMenuItem>()); }
			set { _children = value; }
		}
	}
}
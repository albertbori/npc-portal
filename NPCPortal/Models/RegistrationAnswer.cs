﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class RegistrationAnswer
	{
		public int ID { get; set; }
		public string Question { get; set; }
		public string Answer { get; set; }

		public virtual User User { get; set; }
	}
}
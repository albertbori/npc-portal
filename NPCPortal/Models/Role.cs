﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class Role : IdentityRole
	{
		public PermissionTypes Permissions { get; set; }

		public Role() { }
		public Role(string roleName) : base(roleName) { }

		public static readonly PermissionTypes[] AdminAccessPermissions = new PermissionTypes[]
		{
			PermissionTypes.ApproveUsers,
			PermissionTypes.CreateForums,
			PermissionTypes.CreateRoles,
			PermissionTypes.CreateThemes,
			PermissionTypes.DeleteForums,
			PermissionTypes.DeleteRoles,
			PermissionTypes.DeleteThemes,
			PermissionTypes.DeleteUsers,
			PermissionTypes.EditForums,
			PermissionTypes.EditGeneralSettings,
			PermissionTypes.EditRoles,
			PermissionTypes.EditUsers,
			PermissionTypes.ModeratePosts,
			PermissionTypes.RunJobs,
			PermissionTypes.SendBroadcasts,
			PermissionTypes.SwitchThemes,
			PermissionTypes.ViewLogs,
			PermissionTypes.ViewJobs,
			PermissionTypes.CreatePages,
			PermissionTypes.EditPages,
			PermissionTypes.DeletePages,
			PermissionTypes.EditNav
		};
	}
}
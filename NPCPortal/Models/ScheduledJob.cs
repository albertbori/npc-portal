﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class ScheduledJob
	{
		public int ID { get; set; }
		public string Name { get; set; }

		[MaxLength]
		public string Arguments { get; set; }

		public long TimeTicks { get; set; }

		[NonSerialized]
		private TimeSpan _interval;
		[NotMapped]
		[Display(Name = "Interval")]
		public TimeSpan Interval
		{
			get
			{
				if (_interval == default(TimeSpan))
					_interval = TimeSpan.FromTicks(this.TimeTicks);
				return _interval;
			}
			set
			{
				_interval = value;
				this.TimeTicks = value.Ticks;
			}
		}

		[Display(Name = "Last Run")]

		public DateTime? LastRun { get; set; }

		public Type Type
		{
			get { return Type.GetType(this.Name); }
		}

		public override string ToString()
		{
			return "Name: " + this.Name + " Arguments: " + this.Arguments + " Interval: " + this.Interval.ToString();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("Analytics Settings")]
	public class AnalyticsSettings : SiteSetting
	{
		[DataType(DataType.MultilineText)]
		[Display(Name="Tracking Snippet", Description="Copy/paste the tracking snippet that was given to you by your analytics service in here. This is normally a little bit of javascript.")]
		public string Snippet { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace NPCPortal.Models
{
	[Description("Email Settings")]
	public class EmailSettings : SiteSetting, IValidatableSiteSetting
    {
		[Display(Name="Enable Email")]
		public bool EmailEnabled { get; set; }
		[Display(Name = "Mail Server")]
		public string MailServer { get; set; }
		public int Port { get; set; }
		[Display(Name = "Use SSL Connection")]
		public bool UseSSL { get; set; }
        [Display(Name = "Outbound Email Address")]
        public string OutboundEmailAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
		[Display(Name = "Welcome Message Subject")]
		public string WelcomeMessageSubject { get; set; }
		[Display(Name = "Welcome Message Body", Description = "This message is sent to new users. Supports raw HTML. (Be sure to escape any special HTML characters if necessary: <, > and &). Leave blank to disable welcome messages.")]
		[DataType(DataType.MultilineText)]
		public string WelcomeMessageBody { get; set; }

        #region IValidatableSiteSetting
        public bool Validate(out string error)
        {
            try
            {
                using (var db = new NPCPortalDB())
                {
                    var user = db.Users.Single(u => u.UserName == HttpContext.Current.User.Identity.Name);

                    MailMessage message = new MailMessage(new MailAddress(OutboundEmailAddress, "No-Reply"), new MailAddress(user.Email, user.DisplayName));
                    message.Subject = "Test Message";
                    message.Body = "<h3>Test Message</h3>";
                    message.IsBodyHtml = true;
                    message.BodyEncoding = Encoding.UTF8;

                    var client = new SmtpClient();
                    client.Credentials = new NetworkCredential(UserName, Password);
                    client.Port = Port;
                    client.Host = MailServer;
                    client.EnableSsl = UseSSL;

                    if (client != null)
                    {
                        client.Send(message);
                    }
                }
            }
            catch(Exception ex)
            {
                error = ex.Message;
                return false;
            }
            error = null;
            return true;
        }
        #endregion
    }
}
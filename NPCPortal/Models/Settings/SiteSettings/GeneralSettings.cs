﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("Site Settings")]
	public class GeneralSettings : SiteSetting
	{
		[Display(Name="Site Name", Description="This will show up in various locations around the site as well as in email messages.")]
		public string SiteName { get; set; }

		[Display(Name="Site Domain", Description="If specified, will force all requests to this domain. (eg \"www.mydomain.com\")")]
		public string SiteDomain { get; set; }

		[Display(Name = "Force SSL", Description = "If true, will force all requests to use the \"https\" protocol. (Requires a SSL certificate for your website domain to be installed in IIS.)")]
		public bool ForceSSL { get; set; }

		[Display(Name = "Time Zone", Description = "This will be used to display the time accurately for the visitors to this site. Users will be able to select their own time zones.")]
		[TimeZone]
		public string TimeZone { get; set; }

		[FileUpload("~/UserFiles", "favicon", new string[] { ".ico" })]
		public string Favicon { get; set; }

		[FileUpload("~/UserFiles", "Banner", new string[] { ".jpg", ".jpeg", ".png", ".gif", ".tiff", ".bmp" })]
		[Display(Name="Banner Image", Description="Once uploaded, you can copy the link to the image and use it in your Banner HTML.")]
		public string BannerImage { get; set; }

		[Display(Name="Banner HTML", Description="This will override the default banner to whatever HTML you provide.")]
		[DataType(DataType.MultilineText)]
		public string BannerHTML { get; set; }

		[Display(Name = "Show Banner Everywhere", Description = "If this is checked, the Banner HTML will show at the top of every page on the site.")]
		public bool ShowBannerEverywhere { get; set; }

		public GeneralSettings()
		{
			TimeZone = TimeZoneInfo.Local.Id; //Initialize to local server's time. Can change in admin site.
		}
	}
}
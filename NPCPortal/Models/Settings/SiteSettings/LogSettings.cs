﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("Log Settings")]
	public class LogSettings : SiteSetting
	{
		[Display(Name="Minimum Log Level", Description="This setting will cause all log messages with a lower log level to be discarded. Priority order is Error > Warning > Info > Debug.")]
		public LogLevel MinimumLogLevel { get; set; }
		[Display(Name="Maximum Log History", Description="Maximum number of days to keep log messages stored before they are removed automatically.")]
		public int MaxLogHistoryDays { get; set; }
	}
}
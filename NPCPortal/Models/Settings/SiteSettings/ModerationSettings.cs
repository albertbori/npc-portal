﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("Moderation Settings")]
	public class ModerationSettings : SiteSetting
	{
		/// <summary>
		/// Allows moderators to moderate content using the moderation tools
		/// </summary>
		public bool ModerationEnabled { get; set; }
		/// <summary>
		/// Prevents content from being shown until the content is moderated
		/// </summary>
		public bool RequireModeration { get; set; }
		/// <summary>
		/// Allows users to flag content as spam or inappropriate
		/// </summary>
		public bool CommunityModerationEnabled { get; set; }
		/// <summary>
		/// Robots (bayesian algorythm) will attempt to flag spammy messages.
		/// </summary>
		public bool AutomaticModerationEnabled { get; set; }
		[Display(Name="Automatically Filter Profanity", Description="This setting will cause all user-submitted profanities to be automatically replaced with tame words. This can be overridden based on each user's discretion.")]
		public bool FilterProfanity { get; set; }

		public ModerationSettings()
		{
			this.FilterProfanity = true;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("Permissions Settings")]
	public class PermissionsSettings : SiteSetting
	{
		[Display(Name="Anonymous Permissions", Description="All visitors to the site (authenticated or not) will be able to perform these actions.")]
		public PermissionTypes AnonymousPermissions { get; set; }

		[Display(Name = "Member Permissions", Description = "All authenticated users to the site will be able to perform these actions. (See \"Roles\" for custom permission sets for various users.)")]
		public PermissionTypes MemberPermissions { get; set; }

		/// <summary>
		/// This enables reputation-based permissions (overridden by roles)
		/// </summary>
		[Display(Name = "Enable Reputation System", Description="If enabled, users will be granted authority to perform actions as defined in the Reputation Permissions below.")]
		public bool ReputationSystemEnabled { get; set; }

		private ICollection<ReputationPermission> _reputationPermissions;
		[Display(Name = "Reputation Permissions")]
		public virtual ICollection<ReputationPermission> ReputationPermissions
		{
			get { return _reputationPermissions ?? (_reputationPermissions = new Collection<ReputationPermission>()); }
			set { _reputationPermissions = value; }
		}
	}
}
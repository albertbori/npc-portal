﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("reCaptcha Settings")]
	public class ReCaptchaSettings : SiteSetting
	{
		[Display(Name = "Public Key", Description = "The public key for the reCaptcha. (Get it from www.google.com/recaptcha)")]
		public string PublicKey { get; set; }

		[Display(Name = "Private Key", Description = "The private key for the reCaptcha. (Get it from www.google.com/recaptcha)")]
		public string PrivateKey { get; set; }

		[Display(Name = "UI Theme", Description = "The reCaptcha theme definitions can be found at developers.google.com/recaptcha/docs/customization")]
		public ReCaptchaThemes Theme { get; set; }
	}
}
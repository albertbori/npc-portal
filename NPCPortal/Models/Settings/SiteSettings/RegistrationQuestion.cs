﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace NPCPortal.Models
{
	public class RegistrationQuestion
	{
		public int ID { get; set; }

		[Display(Name = "Question Text")]
		public string Text { get; set; }
		public RegistrationQuestionTypes Type { get; set; }
		public bool Required { get; set; }

		private ICollection<RegistrationQuestionAnswer> _answers { get; set; }
		public virtual ICollection<RegistrationQuestionAnswer> Answers
		{
			get { return _answers ?? (_answers = new Collection<RegistrationQuestionAnswer>()); }
			set { _answers = value; }
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace NPCPortal.Models
{
	public class RegistrationQuestionAnswer
	{
		public int ID { get; set; }
		[Display(Name = "Answer Text")]
		public string AnswerText { get; set; }

		[Display(Name="Correct")]
		public bool IsCorrect { get; set; }

		public RegistrationQuestionAnswer()
		{
			this.IsCorrect = true;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace NPCPortal.Models
{
	[Description("Registration Settings")]
	public class RegistrationSettings : SiteSetting
	{
		[Display(Name="Invitation Only", Description="This setting enables the invitation system and prevents registration without a valid invitation.")]
		public bool InvitationOnly { get; set; }

		[Display(Name = "Registration Password", Description = "This setting requires prospective registrants to supply this password in order to be able to register. If left blank, no password will be required.")]
		public string RegistrationPassword { get; set; }

		[Display(Name = "Enable Captcha", Description = "This setting requires an anti-robot visual test for prospective registrants. (Requires reCaptcha to be configured)")]
		public bool CapchaEnabled { get; set; }

		[Display(Name = "Require Email Validation", Description = "This setting requires registrants to validate their email before becoming fully privelaged users.")]
		public bool RequireEmailValidation { get; set; }

		[Display(Name = "Require Approval", Description = "This setting will cause for site administrators to be notified when someone signs up. The administrator must then approve each registration in order for the prospective registrant to become a fully privelaged user.")]
		public bool RequireApproval { get; set; }

		private ICollection<RegistrationQuestion> _registrationQuestions;
		[Display(Name = "Registration Questions", Description = "These are additional questions that prospective registrants can answer in order to register.")]
		public virtual ICollection<RegistrationQuestion> RegistrationQuestions
		{
			get { return _registrationQuestions ?? (_registrationQuestions = new Collection<RegistrationQuestion>()); }
			set { _registrationQuestions = value; }
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace NPCPortal.Models
{
	[Description("Reputation Permissions")]
	public class ReputationPermission
	{
		public int ID { get; set; }
		[Display(Name = "Minimum Reputation")]
		public int MinimumReputation { get; set; }
		[Display(Name = "Reputation Type")]
		public PermissionTypes PermissionType { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("Reputation Settings")]
	public class ReputationSettings : SiteSetting
	{
		public int PointsPerPost { get; set; }
		public int PointsPerLike { get; set; }
	}
}
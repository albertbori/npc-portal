﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public abstract class SiteSetting
	{
		[Display(AutoGenerateField=false)]
		public int ID { get; set; }

		/// <summary>
		/// Returns all of the SiteSetting Types defined in this assembly and related assemblies
		/// </summary>
		/// <returns></returns>
		public static IEnumerable<Type> GetAllSiteSettingsTypes()
		{
			var baseType = typeof(SiteSetting);
			var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.StartsWith("NPCPortal")).ToList();
			var types = assemblies
				.SelectMany(a => a.GetTypes())
				.Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(baseType))
				.OrderBy(t => t.Name);

			return types;
		}
	}

    public interface IValidatableSiteSetting
    {
        bool Validate(out string error);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public class Theme
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public bool IsActive { get; set; }
	}
}
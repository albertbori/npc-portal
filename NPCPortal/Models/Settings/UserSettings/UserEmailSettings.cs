﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("Email Preferences")]
	public class UserEmailSettings : UserSetting
	{
		[Display(Name="System Alerts", Description="General website notifications, account service emails, etc. (Very few emails)")]
		public bool EnableSystemAlerts { get; set; }
		[Display(Name = "Watched Discussions", Description = "Be notified when someone replies to a discussion you are watching.")]
		public EmailFrequency EnableWatchedThreadNotifications { get; set; }
		[Display(Name = "News and Updates", Description = "Be notified when a user posts a new news article on the home page.")]
		public bool EnableNewsAndUpdates { get; set; }
		[Display(Name = "Message Alerts", Description = "Be notified when a user sends you a direct message on the site.")]
		public bool EnableMessageAlerts { get; set; }

		public UserEmailSettings()
		{
			this.EnableSystemAlerts = true;
			this.EnableWatchedThreadNotifications = EmailFrequency.Instant;
			this.EnableNewsAndUpdates = true;
			this.EnableMessageAlerts = true;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Description("General Preferences")]
	public class UserGeneralSettings : UserSetting
	{
		[Display(Name = "Time Zone", Description = "Selecting your timezone will help show you the correct date and time information throughout the site.")]
		[TimeZone]
		public string TimeZone { get; set; }

		[Display(Name = "Filter Profanity", Description = "Enabling this setting will cause all user-generated content to filter out profane words.")]
		public bool FilterProfanity { get; set; }
		public UserGeneralSettings()
		{
			this.TimeZone = TimeZoneInfo.Local.Id;
			this.FilterProfanity = true;
		}
	}
}
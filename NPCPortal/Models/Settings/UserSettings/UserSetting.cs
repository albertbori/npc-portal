﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	public abstract class UserSetting
	{
		[Display(AutoGenerateField = false)]
		public int ID { get; set; }

		[Display(AutoGenerateField = false)]
		[ScaffoldColumn(false)]
		public virtual User User { get; set; }

		/// <summary>
		/// Returns all of the UserSetting Types defined in this assembly and related assemblies
		/// </summary>
		/// <returns></returns>
		public static IEnumerable<Type> GetAllUserSettingsTypes()
		{
			var baseType = typeof(UserSetting);
			var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.StartsWith("NPCPortal")).ToList();
			var types = assemblies
				.SelectMany(a => a.GetTypes())
				.Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(baseType))
				.OrderBy(t => t.Name);

			return types;
		}
	}
}
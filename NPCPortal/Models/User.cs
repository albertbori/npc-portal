﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NPCPortal.Utilities;
using System.Linq;

namespace NPCPortal.Models
{
    public class User : IdentityUser
    {
		public string Email { get; set; }

		[Display(Name = "Date Joined")]
		[DateTimeKind(DateTimeKind.Utc)]
		public DateTime DateJoined { get; set; }
		[Display(Name = "Last Online")]
		[DateTimeKind(DateTimeKind.Utc)]
		public DateTime LastOnline { get; set; }
		[Display(Name = "Status")]
		public UserStates UserState { get; set; }
		public string ForgotPasswordCode { get; set; }
		public DateTime? ForgotPasswordDate { get; set; }
		public virtual UserProfile Profile { get; set; }
		public string IPAddress { get; set; }

        private ICollection<Content> _authoredContent;
        public virtual ICollection<Content> AuthoredContent
        {
            get { return _authoredContent ?? (_authoredContent = new Collection<Content>()); }
            set { _authoredContent = value; }
        }
        private ICollection<Content> _likedContent;
        public virtual ICollection<Content> LikedContent
        {
            get { return _likedContent ?? (_likedContent = new Collection<Content>()); }
            set { _likedContent = value; }
        }
        private ICollection<Discussion> _watchedDiscussions;
        public virtual ICollection<Discussion> WatchedDiscussions
        {
            get { return _watchedDiscussions ?? (_watchedDiscussions = new Collection<Discussion>()); }
            set { _watchedDiscussions = value; }
        }
		private ICollection<UserSetting> _settings;
		public virtual ICollection<UserSetting> Settings
		{
			get { return _settings ?? (_settings = new Collection<UserSetting>()); }
			set { _settings = value; }
		}
		private ICollection<ViewedContent> _viewedContent;
		public virtual ICollection<ViewedContent> ViewedContent
		{
			get { return _viewedContent ?? (_viewedContent = new Collection<ViewedContent>()); }
			set { _viewedContent = value; }
		}

		private ICollection<MessageRecipient> _receivedMessages;
		public virtual ICollection<MessageRecipient> ReceivedMessages
		{
			get { return _receivedMessages ?? (_receivedMessages = new Collection<MessageRecipient>()); }
			set { _receivedMessages = value; }
		}

		private ICollection<RegistrationAnswer> _registrationAnswers;
		public virtual ICollection<RegistrationAnswer> RegistrationAnswers
		{
			get { return _registrationAnswers ?? (_registrationAnswers = new Collection<RegistrationAnswer>()); }
			set { _registrationAnswers = value; }
		}

		public User()
		{
			this.DateJoined = DateTime.UtcNow;
			this.LastOnline = DateTime.UtcNow;
			try
			{
				if (System.Web.HttpContext.Current != null)
					this.IPAddress = System.Web.HttpContext.Current.Request.GetIPAddress();
			}
			catch { } //This throws when accessing the request throws a "not available in this context" error on get
		}

		public int Reputation
		{
			get { return 0; /*TODO: Calculate rating based on ReputationSettings values*/ }
		}

		public string DisplayName
		{
			get
			{
				if(this.Profile != null && !String.IsNullOrEmpty(this.Profile.DisplayName))
					return this.Profile.DisplayName;

				return this.UserName;
			}
		}

		public void Delete(NPCPortalDB db)
		{
			foreach (var like in this.LikedContent.ToArray())
			{
				this.LikedContent.Remove(like);
			}

			foreach (var authored in this.AuthoredContent.ToArray())
			{
				authored.Delete(db);
			}

			foreach (var watched in this.WatchedDiscussions.ToArray())
			{
				this.WatchedDiscussions.Remove((Discussion)watched);
			}

			foreach (var viewedContent in this.ViewedContent.ToArray())
			{
				db.ViewedContent.Remove(viewedContent);
			}

			foreach (var setting in this.Settings.ToArray())
			{
				db.UserSettings.Remove(setting);
			}

			foreach (var recipient in this.ReceivedMessages.ToArray())
			{
				db.MessageRecipients.Remove(recipient);
			}

			foreach(var userAnswer in this.RegistrationAnswers.ToArray())
			{
				db.RegistrationAnswers.Remove(userAnswer);
			}

			if(this.Profile != null)
				this.Profile.Delete(db);

			db.SaveChanges();

			db.Users.Remove(this);

			db.SaveChanges();
		}

		public bool IsInRole(string role)
		{
			if(String.IsNullOrEmpty(role))
				return true;

			string[] roles = role.Split(new char[] { ',', ' '}, StringSplitOptions.RemoveEmptyEntries);
			return this.Roles.Any(r => roles.Contains(r.Role.Name));
		}
	}
}
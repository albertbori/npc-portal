﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Table("UserProfiles")]
	public class UserProfile
	{
		public int ID { get; set; }
		public string DisplayName { get; set; }
		public virtual File Avatar { get; set; }
		public string Bio { get; set; }
		[Display(Name="Tag Line")]
		public string TagLine { get; set; }
		public bool ShowEmailAddress { get; set; }

		public void Delete(NPCPortalDB db)
		{
			if (this.Avatar != null)
			{
				this.Avatar.Delete(db);
			}
		}
	}
}
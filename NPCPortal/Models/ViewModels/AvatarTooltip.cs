﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models.ViewModels
{
	public class AvatarTooltip
	{
		public int PostCount { get; set; }
		public int LikesCount { get; set; }
		public DateTime DateJoined { get; set; }
		public string Roles { get; set; }
	}
}
﻿using NPCPortal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models.ViewModels
{
	public class DisplayUser
	{
		public string Id { get; set; }
		public string DisplayName { get; set; }
		public DateTime DateJoined { get; set; }
		public string AvatarURL { get; set; }

		public static DisplayUser GetFromCache(string userId)
		{
			var displayUser = Cache.GetByID<DisplayUser>(userId);
			if (displayUser == null) {
				using(var db = new NPCPortalDB())
				{
					var user = db.Users.Find(userId);
					displayUser = new DisplayUser()
					{
						DisplayName = user.DisplayName,
						DateJoined = user.DateJoined,
						AvatarURL = user.Profile != null ? user.Profile.Avatar != null ? user.Profile.Avatar.URL : null : null
					};
				}
				Cache.SetByID(userId, displayUser);
			}
			
			return displayUser;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPCPortal.Models.ViewModels
{
	public class ForumStats
	{
		public List<ForumContributor> TopContributors { get; set; }
		public List<ForumContributor> PopularContributors { get; set; }
	}

	public class ForumContributor
	{
		public string UserID { get; set; }
		public int Value { get; set; }
	}
}
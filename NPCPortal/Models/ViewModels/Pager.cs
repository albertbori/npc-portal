﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace NPCPortal.Models.ViewModels
{
	public class Pager
	{
		public int CurrentPage { get; set; }
		public int TotalRecords { get; set; }
		public int RecordsPerPage { get; set; }
		public int PagesPerSet { get; set; }
		/// <summary>
		/// Requires "{page}" macro in order to work correctly
		/// </summary>
		public string PageUrlFormat { get; set; }
		
		public int CurrentSet
		{
			get { return ((CurrentPage - 1) / PagesPerSet) + 1; }
		}

		public int PagesInThisSet
		{
			get { return TotalPages - ((CurrentSet - 1) * PagesPerSet) >= PagesPerSet ? PagesPerSet : TotalPages - ((CurrentSet - 1) * PagesPerSet); }
		}

		public int TotalPages
		{
			get { return ((TotalRecords - 1) / RecordsPerPage) + 1; }
		}

		public int TotalSets
		{
			get { return ((TotalPages - 1) / PagesPerSet) + 1; }
		}

		public bool IsFirstSet
		{
			get { return CurrentSet <= 1; }
		}

		public bool IsLastSet
		{
			get { return CurrentSet == TotalSets; }
		}

		public int FirstPageInSet
		{
			get { return ((CurrentSet - 1) * PagesPerSet) + 1; }
		}

		public int PreviousSetPage
		{
			get { return FirstPageInSet - 1; }
		}

		public int NextSetPage
		{
			get { return FirstPageInSet + PagesPerSet; }
		}

		public Pager(int totalRecords) : this()
		{
			this.TotalRecords = totalRecords;
		}

		public Pager()
		{
			//Set defaults
			PagesPerSet = 10;
			RecordsPerPage = 25;

			int currentPage;
			if (int.TryParse(HttpContext.Current.Request.Params["page"], out currentPage))
				this.CurrentPage = currentPage;
			else
				this.CurrentPage = 1;

			if (!String.IsNullOrEmpty(HttpContext.Current.Request.Url.Query))
			{
				string pagePattern = @"(?<=[\?&]page=)[\d]+";
				if (Regex.IsMatch(HttpContext.Current.Request.Url.ToString(), pagePattern, RegexOptions.IgnoreCase))
				{
					PageUrlFormat = Regex.Replace(HttpContext.Current.Request.Url.ToString(), pagePattern, "{page}", RegexOptions.IgnoreCase);
				}
				else
					PageUrlFormat = HttpContext.Current.Request.Url.ToString() + "&page={page}";
			}
			else
				PageUrlFormat = HttpContext.Current.Request.Url.ToString() + "?page={page}";
		}

		public string GetPageURL(int pageNumber)
		{
			return this.PageUrlFormat.Replace("{page}", pageNumber.ToString())
				.Replace("%7Bpage%7D", pageNumber.ToString());
		}
	}
}
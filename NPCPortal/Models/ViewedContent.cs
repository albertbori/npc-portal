﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NPCPortal.Models
{
	[Table("ViewedContent")]
	public class ViewedContent
	{
		public int ID { get; set; }
		public virtual User User { get; set; }
		public virtual Content Content { get; set; }
		[DateTimeKind(DateTimeKind.Utc)]
		public DateTime ViewedOn { get; set; }
		public int ViewCount { get; set; }

		public ViewedContent()
		{
			this.ViewedOn = DateTime.UtcNow;
			this.ViewCount = 1;
		}
	}
}
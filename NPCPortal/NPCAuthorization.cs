﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPCPortal.Utilities;

namespace NPCPortal
{
	public class NPCAuthorize : AuthorizeAttribute
	{
		private PermissionTypes[] _permissionTypes;

		/// <summary>
		/// Authorizes users based on provided permission. Can be a combination of permissions using Flags (bitmask).
		/// </summary>
		/// <param name="permissionTypes"></param>
		public NPCAuthorize(PermissionTypes permissionTypes)
		{
			_permissionTypes = new PermissionTypes[] { permissionTypes };
		}

		/// <summary>
		/// Authorizes users based on permissions. If a user matches any of the supplied permissions, they will pass.
		/// </summary>
		/// <param name="permissionTypes"></param>
		public NPCAuthorize(PermissionTypes[] permissionTypes)
		{
			_permissionTypes = permissionTypes;
		}

		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			return httpContext.User.CanDoAny(_permissionTypes);			
		}
	}
}
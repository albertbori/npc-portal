﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NPCPortal
{
	public class NPCModelBinder : DefaultModelBinder
	{
		public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			//Handle abstract classes or Interfaces
			if ((bindingContext.ModelType.IsAbstract || bindingContext.ModelType.IsInterface) && !bindingContext.ModelType.GetInterfaces()
                            .Any(x => x.IsGenericType &&
                            x.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
			{
				Type desiredType = GetDerivedType(bindingContext);
				bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(null, desiredType);
			}
			//Handle enums with [Flags] attribute
			else if (bindingContext.ModelType.IsEnum && bindingContext.ModelType.GetCustomAttributes(typeof(FlagsAttribute), false).Any())
			{
				var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
				if (value != null)
				{
					var rawValues = value.RawValue as string[];
					if (rawValues != null)
					{
						return Enum.Parse(bindingContext.ModelType, string.Join(",", rawValues));
					}
				}
			}

			return base.BindModel(controllerContext, bindingContext);
		}

		//attempts to retrieve the type that implements the interface or inherits the abstract class
		private Type GetDerivedType(ModelBindingContext bindingContext)
		{
			string assemblyQualifiedName = null;
			if (bindingContext.ValueProvider.ContainsPrefix(String.Format("{0}.AssemblyQualifiedName", bindingContext.ModelName)))
				assemblyQualifiedName = (string)bindingContext.ValueProvider.GetValue(String.Format("{0}.AssemblyQualifiedName", bindingContext.ModelName)).ConvertTo(typeof(string));
			else if (bindingContext.ValueProvider.ContainsPrefix("AssemblyQualifiedName"))
				assemblyQualifiedName = (string)bindingContext.ValueProvider.GetValue("AssemblyQualifiedName").ConvertTo(typeof(string));

			if (String.IsNullOrEmpty(assemblyQualifiedName))
				throw new Exception(
					String.Format("Unable to bind {0}: {1}. Add the following to your view to specify desired type: @Html.Hidden(\"[PropertyName.]AssemblyQualifiedName\", EncryptionService.Encrypt(Model.GetType().AssemblyQualifiedName))",
						bindingContext.ModelType.IsInterface ? "interface" : "abstract class", bindingContext.ModelType.FullName));

			return Type.GetType(assemblyQualifiedName);
		}
	}
}

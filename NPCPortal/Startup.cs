﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NPCPortal.Startup))]
namespace NPCPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

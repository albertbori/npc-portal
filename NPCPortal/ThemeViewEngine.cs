﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal
{
	public class ThemeViewEngine : RazorViewEngine
	{
		public ThemeViewEngine(string themeName)
		{
			base.MasterLocationFormats = new string[] {
				"~/Themes/" + themeName + "/{1}/{0}.cshtml", 
				"~/Themes/" + themeName + "/Shared/{0}.cshtml",
				"~/Views/{1}/{0}.cshtml", 
				"~/Views/Shared/{0}.cshtml"
			};
			base.ViewLocationFormats = new string[] { 
				"~/Themes/" + themeName + "/{1}/{0}.cshtml", 
				"~/Themes/" + themeName + "/Shared/{0}.cshtml", 
				"~/Views/{1}/{0}.cshtml",
				"~/Views/Shared/{0}.cshtml" 
			};
			base.PartialViewLocationFormats = new string[] {
				"~/Themes/" + themeName + "/{1}/{0}.cshtml",
				"~/Themes/" + themeName + "/Shared/{0}.cshtml",
				"~/Views/{1}/{0}.cshtml",
				"~/Views/Shared/{0}.cshtml"
			};
		}
	}
}
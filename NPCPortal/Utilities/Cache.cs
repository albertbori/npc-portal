﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace NPCPortal.Utilities
{
	public static class Cache
	{
		private const int cacheExpirationMinutes = 10800; //7 days
		
		public static object Get(string key)
		{
			return WebCache.Get(key);
		}

		public static T Get<T>(string key)
		{
			var item = Get(key);
			if (item == null)
				return default(T);

			try
			{
				return (T)item;
			}
			catch
			{
				Log.Error("Failed to get \"{0}\" from cache. Type mismatch.", key);
				return default(T);
			}
		}

		public static T GetByID<T>(int id)
		{
			string key = String.Format("{0}:{1}", typeof(T).AssemblyQualifiedName, id);
			var item = Get(key);
			if (item == null)
				return default(T);

			try
			{
				return (T)item;
			}
			catch
			{
				Log.Error("Failed to get \"{0}\" from cache. Type mismatch.", key);
				return default(T);
			}
		}

		public static T GetByID<T>(string id)
		{
			string key = String.Format("{0}:{1}", typeof(T).AssemblyQualifiedName, id);
			var item = Get(key);
			if (item == null)
				return default(T);

			try
			{
				return (T)item;
			}
			catch
			{
				Log.Error("Failed to get \"{0}\" from cache. Type mismatch.", key);
				return default(T);
			}
		}

		public static void Set(string key, object o)
		{
			WebCache.Set(key, o, cacheExpirationMinutes);
		}

		public static void SetByID(int id, object o)
		{
			string key = String.Format("{0}:{1}", o.GetType().AssemblyQualifiedName, id);
			Set(key, o);
		}

		public static void SetByID(string id, object o)
		{
			string key = String.Format("{0}:{1}", o.GetType().AssemblyQualifiedName, id);
			Set(key, o);
		}

		public static void Remove(string key)
		{
			WebCache.Remove(key);
		}

		public static void RemoveByID<T>(int id)
		{
			string key = String.Format("{0}:{1}", typeof(T).AssemblyQualifiedName, id);
			Remove(key);
		}

		public static void RemoveByID<T>(string id)
		{
			string key = String.Format("{0}:{1}", typeof(T).AssemblyQualifiedName, id);
			Remove(key);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using NPCPortal.Models;
using System.Text;

namespace NPCPortal.Utilities
{
	public static class Email
	{
		public static void Send(string recipientName, string recipientEmail, string subject, string htmlBody)
		{
			try
			{ 
				Send(recipientName, recipientEmail,
					null, NPCPortalWeb.EmailSettings.OutboundEmailAddress,
					subject, htmlBody);
			}
			catch (Exception ex)
			{
				Log.Error(ex);
			}
		}

		public static void Send(string recipientName, string recipientEmail, string senderName, string senderEmail, string subject, string htmlBody)
		{
			try
			{
				if(String.IsNullOrEmpty(senderName))
					senderName = !String.IsNullOrEmpty(NPCPortalWeb.GeneralSettings.SiteName) ? NPCPortalWeb.GeneralSettings.SiteName : "No Reply";

				MailMessage message = new MailMessage(new MailAddress(senderEmail, senderName), new MailAddress(recipientEmail, recipientName));
				message.Subject = subject;
				message.Body = htmlBody;
				message.IsBodyHtml = true;
				message.BodyEncoding = Encoding.UTF8;

				Send(message);
			}
			catch (Exception ex)
			{
				Log.Error(ex);
			}
		}

		public static void Send(MailMessage message)
		{
			if (message == null)
				throw new Exception("Message object was null");
			if (String.IsNullOrEmpty(message.Subject.Trim()))
				throw new Exception("Subject was empty");
			if (String.IsNullOrEmpty(message.Body.Trim()))
				throw new Exception("Body was empty");

			try
			{
				if (NPCPortalWeb.EmailSettings.EmailEnabled)
				{
					var client = new SmtpClient();
					client.Credentials = new NetworkCredential(NPCPortalWeb.EmailSettings.UserName, NPCPortalWeb.EmailSettings.Password);
					client.Port = NPCPortalWeb.EmailSettings.Port;
					client.Host = NPCPortalWeb.EmailSettings.MailServer;
					client.EnableSsl = NPCPortalWeb.EmailSettings.UseSSL;

					if (client != null)
					{
						client.Send(message);
					}
				}

				Log.Debug("Email sent to {0}\n{1}", message.To, message.Body);
			}
			catch(Exception ex)
			{
				Log.Error(ex, "Error sending email to {0}", message.To);
			}
		}

		public static void Send(string recipientName, string recipientEmail, string subject, string razorViewPath, object model)
		{
			Send(recipientName, recipientEmail, subject, razorViewPath, model, new Dictionary<string,object>());
		}
		public static void Send(string recipientName, string recipientEmail, string subject, string razorViewPath, object model, Dictionary<string, object> viewData)
		{
			Send(recipientName, recipientEmail, null, NPCPortalWeb.EmailSettings.OutboundEmailAddress, subject, razorViewPath, model, viewData);
		}
		
		public static void Send(string recipientName, string recipientEmail, string senderName, string senderEmail, string subject, 
			string razorViewPath, object model)
		{
			Send(recipientName, recipientEmail, senderName, senderEmail, subject, razorViewPath, model, new Dictionary<string, object>());
		}

		public static void Send(string recipientName, string recipientEmail, string senderName, string senderEmail, string subject, 
			string razorViewPath, object model, Dictionary<string, object> viewData)
		{
			var viewDataDictionary = new System.Web.Mvc.ViewDataDictionary(model);
			foreach(var pair in viewData)
			{
				viewDataDictionary.Add(pair.Key, pair.Value);
			}

			string viewError;
			var controller = new NPCPortal.Controllers.BaseController();
			string html = controller.RenderViewToString(razorViewPath, viewDataDictionary, out viewError);

			if (!String.IsNullOrEmpty(viewError))
				throw new Exception("Error retrieving email html: " + viewError);

			Send(recipientName, recipientEmail, senderName, senderEmail, subject, html);
		}
	}
}
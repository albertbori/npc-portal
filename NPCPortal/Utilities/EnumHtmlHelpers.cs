﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Utilities
{
	public static class EnumHtmlHelpers
	{
		public static IHtmlString CheckBoxListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
		{
			return CheckBoxListFor(htmlHelper, expression, 0);
		}

		public static IHtmlString CheckBoxListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, int columns)
		{
			ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

			// Check to make sure this is an enum.
			if (!metadata.ModelType.IsEnum || !metadata.ModelType.GetCustomAttributes(typeof(FlagsAttribute), false).Any())
			{
				throw new ArgumentException("This helper can only be used with enums with the [Flags] attribute. Type used was: " + metadata.ModelType.FullName.ToString() + ".");
			}
			return CheckBoxListForEnum(htmlHelper, expression, columns);
		}

		private static IHtmlString CheckBoxListForEnum<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, int columns)
		{
			ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

			var model = metadata.Model as Enum;
			var templateInfo = htmlHelper.ViewData.TemplateInfo;
			var checkboxItems = Enum.GetValues(metadata.ModelType).Cast<Enum>()
				.Where(e => Convert.ToInt64(e) != 0)
				.Select(e => new CheckBoxItem()
				{
					ID = templateInfo.GetFullHtmlFieldId(e.ToString()),
					Name = templateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression)),
					Value = e.ToString(),
					Checked = model != null && model.HasFlag(e),
					LabelText = StringHelpers.GetEnumDescription(e)
				});

			return BuildCheckBoxList(checkboxItems, columns);
		}

		public static IHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> items)
		{
			return CheckBoxListFor(htmlHelper, expression, items, 0);
		}

		public static IHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> items, int columns)
		{
			var templateInfo = htmlHelper.ViewData.TemplateInfo;
			var checkboxItems = items.Select(item => new CheckBoxItem()
				{
					ID = templateInfo.GetFullHtmlFieldId(item.ToString()),
					Name = templateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression)),
					Value = item.Value ?? item.Text,
					Checked = item.Selected,
					LabelText = item.Text ?? item.Value
				});

			return BuildCheckBoxList(checkboxItems, columns);
		}

		public static IHtmlString CheckBoxList<TModel>(this HtmlHelper<TModel> htmlHelper, string name, IEnumerable<SelectListItem> items)
		{
			return CheckBoxList(htmlHelper, name, items, 0);
		}

		public static IHtmlString CheckBoxList<TModel>(this HtmlHelper<TModel> htmlHelper, string name, IEnumerable<SelectListItem> items, int columns)
		{
			var templateInfo = htmlHelper.ViewData.TemplateInfo;
			var checkboxItems = items.Select(item => new CheckBoxItem()
			{
				ID = item.Value ?? item.Text,
				Name = name,
				Value = item.Value ?? item.Text,
				Checked = item.Selected,
				LabelText = item.Text ?? item.Value
			});

			return BuildCheckBoxList(checkboxItems, columns);
		}

		private static IHtmlString BuildCheckBoxList(IEnumerable<CheckBoxItem> checkboxes, int columnCount)
		{
			StringBuilder sbHTML = new StringBuilder();

			if (columnCount > 0)
				sbHTML.AppendFormat("<div class=\"row\"><div class=\"col-md-{0}\">", 12 / columnCount);

			int itemsPerColumn = columnCount > 0 ? ((checkboxes.Count() - 1) / columnCount) + 1 : checkboxes.Count();
			int itemCount = 1;
			foreach (var item in checkboxes)
			{
				var label = new TagBuilder("label");
				label.Attributes["class"] = "checkbox"; //twitter bootstrap
				label.Attributes["for"] = item.ID;
				label.SetInnerText(item.LabelText);

				var checkbox = new TagBuilder("input");
				checkbox.Attributes["id"] = item.ID;
				checkbox.Attributes["name"] = item.Name;
				checkbox.Attributes["type"] = "checkbox";
				checkbox.Attributes["value"] = item.Value;
				if(item.Checked)
					checkbox.Attributes["checked"] = "checked";

				label.InnerHtml = checkbox.ToString() + "&nbsp;" + label.InnerHtml;
				sbHTML.Append(label.ToString());


				if (columnCount > 0 && itemCount % itemsPerColumn == 0)
					sbHTML.AppendFormat("</div><div class=\"col-md-{0}\">", 12 / columnCount);

				itemCount++;
			}

			if (columnCount > 0)
				sbHTML.Append("</div></div>");

			return new HtmlString(sbHTML.ToString());
		}

		private class CheckBoxItem
		{
			public string ID { get; set; }
			public string Name { get; set; }
			public string Value { get; set; }
			public bool Checked { get; set; }
			public string LabelText { get; set; }
			public string For { get; set; }
		}
	}
}
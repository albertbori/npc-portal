﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace NPCPortal.Utilities
{
	public static class FileStore
	{
		public const string USER_IMAGES_DIRECTORY = "~/UserFiles/Images";
		public const string USER_FILES_DIRECTORY = "~/UserFiles/Images";
		public static string _localImageDirectory;
		public static string _localFileDirectory;
		public static readonly Dictionary<ThumbnailTypes, Size> ThumbnailSizes = new Dictionary<ThumbnailTypes, Size>() 
		{
			{ ThumbnailTypes.Avatar, new Size(64, 64) },
			{ ThumbnailTypes.Thumbnail, new Size(171, 180) },
			{ ThumbnailTypes.WideThumbnail, new Size(300, 200) }
		};

		#region Image Methods
		public static string SaveNewImage(HttpPostedFileBase file)
		{
			if(String.IsNullOrEmpty(_localImageDirectory))
				InitializeLocalDirectories();

			string newFileName = Guid.NewGuid() + Path.GetExtension(file.FileName);

			string filePath = Path.Combine(_localFileDirectory, newFileName);

			Image i = Image.FromStream(file.InputStream);

			i.Save(filePath);
				
			SaveThumbnails(i, newFileName);

			return newFileName;
		}

		public static void DeleteImage(string fileName)
		{
			if (String.IsNullOrEmpty(_localImageDirectory))
				InitializeLocalDirectories();

			foreach(ThumbnailTypes thumbnailType in Enum.GetValues(typeof(ThumbnailTypes)).Cast<ThumbnailTypes>())
			{
				System.IO.File.Delete(Path.Combine(_localImageDirectory, thumbnailType.ToString() + fileName));
			}

			System.IO.File.Delete(Path.Combine(_localImageDirectory, fileName));
		}

		public static string GetImageURL(string fileName)
		{
			return VirtualPathUtility.ToAbsolute(USER_IMAGES_DIRECTORY + "/" + fileName);
		}

		public static string GetThumbnailURL(string fileName, ThumbnailTypes thumbnailType)
		{
			return VirtualPathUtility.ToAbsolute(USER_IMAGES_DIRECTORY + "/" + thumbnailType.ToString() + fileName);
		}
		#endregion Image Methods

		#region File Methods
		public static string SaveNewFile(HttpPostedFileBase file)
		{
			if (String.IsNullOrEmpty(_localFileDirectory))
				InitializeLocalDirectories();

			string newFileName = Guid.NewGuid() + Path.GetExtension(file.FileName);

			string filePath = Path.Combine(_localFileDirectory, newFileName);

			file.SaveAs(filePath);

			return newFileName;
		}

		public static void DeleteFile(string fileName)
		{
			if (String.IsNullOrEmpty(_localFileDirectory))
				InitializeLocalDirectories();

			System.IO.File.Delete(Path.Combine(_localFileDirectory, fileName));
		}

		public static string GetFileURL(string fileName)
		{
			return VirtualPathUtility.ToAbsolute(USER_FILES_DIRECTORY + "/" + fileName);
		}
		#endregion File Methods

		#region HelperMethods
		private static void InitializeLocalDirectories()
		{
			_localImageDirectory = HttpContext.Current.Server.MapPath(USER_IMAGES_DIRECTORY);
			if (!Directory.Exists(_localImageDirectory))
				Directory.CreateDirectory(_localImageDirectory);

			_localFileDirectory = HttpContext.Current.Server.MapPath(USER_FILES_DIRECTORY);
			if (!Directory.Exists(_localFileDirectory))
				Directory.CreateDirectory(_localFileDirectory);
		}

		private static void SaveThumbnails(Image original, string fileName)
		{
			foreach (var size in ThumbnailSizes)
			{
				//Image thumbnail = ImageHelpers.HardResizeImage(size.Value.Width, size.Value.Height, original);
				Image thumbnail = Resize(original, size.Value.Width, size.Value.Height, true);
				thumbnail.Save(Path.Combine(_localImageDirectory, size.Key.ToString() + fileName));
			}
		}

		private static Image Resize(Image img, int width, int height, bool autoCrop)
		{
			int originalWidth = img.Width;
			int originalHeight = img.Height;

			double scale = 0;

			//Always fit to the height of the desired dimensions (and crop any extra width)
			scale = height / (double)originalHeight;

			int destWidth = (int)Math.Round(originalWidth * scale);
			int destHeight = (int)Math.Round(originalHeight * scale);
			int offsetX = (width - destWidth) / 2;
			int offsetY = (height - destHeight) / 2;

			//resize image to fit within desired size
			System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(width, height, PixelFormat.Format32bppRgb);
			using (System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto))
			{
				grPhoto.Clear(System.Drawing.Color.White);
				grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
				grPhoto.CompositingQuality = CompositingQuality.HighQuality;
				grPhoto.SmoothingMode = SmoothingMode.HighQuality;

				//crop off the excess
				grPhoto.DrawImage(img,
					new System.Drawing.Rectangle(offsetX, offsetY, destWidth, destHeight),
					new System.Drawing.Rectangle(0, 0, originalWidth, originalHeight),
					System.Drawing.GraphicsUnit.Pixel);
			}

			return bmPhoto;
		}
		#endregion Helper Methods
	}
}
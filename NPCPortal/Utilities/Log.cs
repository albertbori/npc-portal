﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace NPCPortal.Utilities
{
	public static class Log
	{
		public static void Error(string message)
		{
			Message(message, LogLevel.Error);
		}

		public static void Error(string message, params object[] formatValues)
		{
			Error(String.Format(message, formatValues));
		}

		public static void Error(Exception ex)
		{
			Message(ex.ToString(), LogLevel.Error);
		}

		public static void Error(Exception ex, string message)
		{
			Message(String.Format("{0}\n{1}", message, ex.ToString()), LogLevel.Error);
		}

		public static void Error(Exception ex, string message, params object[] formatValues)
		{
			Error(ex, String.Format(message, formatValues));
		}

		public static void Warning(string message)
		{
			Message(message, LogLevel.Warning);
		}

		public static void Warning(string message, params object[] formatValues)
		{
			Warning(String.Format(message, formatValues));
		}

		public static void Info(string message)
		{
			Message(message, LogLevel.Info);
		}

		public static void Info(string message, params object[] formatValues)
		{
			Info(String.Format(message, formatValues));
		}

		public static void Debug(string message)
		{
			Message(message, LogLevel.Debug);
		}

		public static void Debug(string message, params object[] formatValues)
		{
			Debug(String.Format(message, formatValues));
		}

		public static void Message(string message, LogLevel level)
		{
			AsyncLogMessageCaller caller = new AsyncLogMessageCaller(LogMessage);
			caller.BeginInvoke(message, level, HttpContext.Current, null, null);
		}
		private static void LogMessage(string message, LogLevel level)
		{
			LogMessage(message, level, HttpContext.Current);
		}

		private static void LogMessage(string message, LogLevel level, HttpContext httpContext)
		{
            var logSettings = NPCPortal.NPCPortalWeb.SiteSettings.OfType<LogSettings>().FirstOrDefault();

            if (logSettings == null || logSettings.MinimumLogLevel <= level)
			{
				//strip out System messages to save on log space
				message = Regex.Replace(message, @"[\r\n]{1,2}\s+at System\.[^\r\n]+", String.Empty, RegexOptions.IgnoreCase);

				if (httpContext != null)
				{
					message += GetRequestInfo(httpContext);
				}

				using (NPCPortalDB db = new NPCPortalDB())
				{
					db.LogMessages.Add(new LogMessage()
						{
							Date = DateTime.UtcNow,
							Level = level,
							Message = message
						});

					db.SaveChanges();
				}
			}
		}

		private delegate void AsyncLogMessageCaller(string message, LogLevel level, HttpContext httpContext);

		private static string GetRequestInfo(HttpContext httpContext)
		{
			try
			{
				var request = httpContext.Request;
				string requestFormat = @"
--- Request Info ---
URL: {0}
Method: {1}
IP: {2}
User ID: {3},
User Agent: {4},
Localizations: {5},
Accept: {6},
Referrer: {7}";
				string requestInfo = String.Format(requestFormat,
					request.Url,
					request.HttpMethod,
					request.GetIPAddress(),
					request.GetCurrentUser() != null ? request.GetCurrentUser().Id : "N/A",
					request.UserAgent,
					request.UserLanguages != null ? string.Join(", ", request.UserLanguages) : "null",
					request.AcceptTypes != null ? string.Join(", ", request.AcceptTypes) : "null",
					request.UrlReferrer);

				return requestInfo;
			}
			catch //Can't get the request in this context, don't hold up the log message
			{
				return "";
			}
		}
	}
}

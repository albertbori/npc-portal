﻿using NPCPortal.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Utilities
{
	public static class MarkdownHtmlHelper
	{
		public static MvcHtmlString MarkdownEditorFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
		{
			var data = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
			string propertyName = data.PropertyName;

			BaseController controller = new BaseController();
			string viewError;
			var viewData = new ViewDataDictionary();
			viewData["name"] = data.PropertyName;

			if (data.Model != null)
				viewData["value"] = data.Model;
			
			if (htmlAttributes != null)
			{
				var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
				viewData["htmlAttributes"] = attributes.Select(a => a.Key + "=" + a.Value).ToArray();
			}
			var partial = controller.RenderViewToString("~/Views/Shared/_MarkdownEditor.cshtml", viewData, out viewError, true);

			if (!String.IsNullOrEmpty(viewError))
				throw new Exception(viewError);

			return new MvcHtmlString(partial);
		}

		public static MvcHtmlString MarkdownEditor(this HtmlHelper helper, string name)
		{
			return MarkdownEditor(helper, name, null);
		}

		public static MvcHtmlString MarkdownEditor(this HtmlHelper helper, string name, string value)
		{
			return MarkdownEditor(helper, name, value, null);
		}

		public static MvcHtmlString MarkdownEditor(this HtmlHelper helper, string name, string value, object htmlAttributes = null)
		{
			BaseController controller = new BaseController();
			string viewError;
			var viewData = new ViewDataDictionary();
			viewData["name"] = name;
			if(value != null)
				viewData["value"] = value;
			
			if (htmlAttributes != null)
			{
				var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
				viewData["htmlAttributes"] = attributes.Select(a => a.Key + "=" + a.Value).ToArray();
			}
			var partial = controller.RenderViewToString("~/Views/Shared/_MarkdownEditor.cshtml", viewData, out viewError, true);

			if (!String.IsNullOrEmpty(viewError))
				throw new Exception(viewError);

			return new MvcHtmlString(partial);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace NPCPortal.Utilities
{
	public static class ProfanityFilter
	{
		#region Profanities
		private static readonly List<Profanity> _badWordMap = new List<Profanity>()
		{
			new Profanity("shit", "crap", false),
			new Profanity("fuck", "freak", false),
			new Profanity("damn", "darn", false),
			new Profanity("bitch", "witch", false),
			new Profanity("piss", "pee", true),
			new Profanity("dick", "dirt", false),
			new Profanity("cock", "dirt", true),
			new Profanity("pussy", "wuss", false),
			new Profanity("asshole", "dork", true),
			new Profanity("ass", "butt", true),
			new Profanity("fag", "dork", false),
			new Profanity("bastard", "dork", false),
			new Profanity("slut", "dork", false),
			new Profanity("douche", "dork", false),
			new Profanity("cunt", null, false),
			new Profanity("cum", null, true),
			new Profanity("nigger", "I'm an idiot.", false)
		};
		#endregion Profanities

		public static string FilterProfanity(string s)
		{
			if (s == null)
				return s;

			foreach(var profanity in _badWordMap)
			{
				s = Regex.Replace(s, profanity.BadWordRegex, profanity.GoodWord, RegexOptions.IgnoreCase|RegexOptions.Singleline);
			}

			return s;
		}
		
		private static string GetRegex(string word, bool matchWholeWordOnly)
		{
			StringBuilder sb = new StringBuilder();
			if(matchWholeWordOnly)
				sb.Append(@"\b");

			sb.Append(String.Join(@"+\s*", word.ToArray()));
			sb.Append("+");

			if(matchWholeWordOnly)
				sb.Append(@"\b");

			return sb.ToString();
		}

		public class Profanity
		{
			public string BadWord { get; set; }
			public string BadWordRegex { get; set; }
			public string GoodWord { get; set; }

			public Profanity(string badWord, string goodWord, bool wholeWordOnly)
			{
				this.BadWord = badWord;
				this.GoodWord = goodWord ?? "*****";
				this.BadWordRegex = GetRegex(badWord, wholeWordOnly);
			}
		}
	}
}
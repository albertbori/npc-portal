﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace NPCPortal.Utilities
{
	public static class RequestHelpers
	{
		public static User GetCurrentUser(this HttpContext context)
		{
			if(context != null && context.User.Identity.IsAuthenticated)
			{
				if (!context.Items.Contains("CurrentUser"))
				{
					using(NPCPortalDB db = new NPCPortalDB())
					{
						var user = db.Users.Find(context.User.Identity.GetUserId());
						db.Entry(user).Collection(u => u.Settings).Load();
						context.Items.Add("CurrentUser", user);
					}
				}
				return (User)context.Items["CurrentUser"];
			}
			return null;
		}

		public static User GetCurrentUser(this HttpRequest request)
		{
			return HttpContext.Current.GetCurrentUser();
		}

		public static string GetIPAddress(this HttpRequestBase request)
		{
			return GetRequestIPAddress(request);
		}

		public static string GetIPAddress(this HttpRequest request)
		{
			return GetRequestIPAddress(new HttpRequestWrapper(request));
		}

		private static string GetRequestIPAddress(HttpRequestBase request)
		{
			try
			{
				string szRemoteAddr = request.UserHostAddress;
				string szXForwardedFor = request.ServerVariables["X_FORWARDED_FOR"];
				string szIP = "";

				if (szXForwardedFor == null)
				{
					szIP = szRemoteAddr;
				}
				else
				{
					szIP = szXForwardedFor;
					if (szIP.IndexOf(",") > 0)
					{
						string[] arIPs = szIP.Split(',');

						foreach (string item in arIPs)
						{
							if (!IsPrivateIP(item))
								return item;
						}
					}
				}
				return szIP;
			}
			catch //This throws sometimes. Don't know why.
			{
				return null;
			}
		}

		private static bool IsPrivateIP(string ipAddress)
		{
			String[] straryIPAddress = ipAddress.ToString().Split(new String[] { "." }, StringSplitOptions.RemoveEmptyEntries);
			int[] iaryIPAddress = new int[] { int.Parse(straryIPAddress[0]), int.Parse(straryIPAddress[1]), int.Parse(straryIPAddress[2]), int.Parse(straryIPAddress[3]) };
			if (iaryIPAddress[0] == 10 || (iaryIPAddress[0] == 192 && iaryIPAddress[1] == 168) || (iaryIPAddress[0] == 172 && (iaryIPAddress[1] >= 16 && iaryIPAddress[1] <= 31)))
			{
				return true;
			}
			else
			{
				// IP Address is "probably" public. This doesn't catch some VPN ranges like OpenVPN and Hamachi.
				return false;
			}
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace NPCPortal.Utilities
{
	public class Security
	{
		//Changing this will cause all your existing encrypted data to be lost.
		private const string _key = "~!crCT|bTry*hrsE)stPLR?~";
		public static string Encrypt(string toEncrypt)
		{
			byte[] keyArray;
			byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
			
			keyArray = UTF8Encoding.UTF8.GetBytes(_key);

			TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider();
			tDes.Key = keyArray;
			tDes.Mode = CipherMode.ECB;
			tDes.Padding = PaddingMode.PKCS7;
			ICryptoTransform cTransform = tDes.CreateEncryptor();
			byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
			tDes.Clear();
			return Convert.ToBase64String(resultArray, 0, resultArray.Length);
		}

		public static string Decrypt(string cypherString)
		{
			byte[] keyArray;
			byte[] toDecryptArray = Convert.FromBase64String(cypherString);

			keyArray = UTF8Encoding.UTF8.GetBytes(_key);

			TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider();
			tDes.Key = keyArray;
			tDes.Mode = CipherMode.ECB;
			tDes.Padding = PaddingMode.PKCS7;
			ICryptoTransform cTransform = tDes.CreateDecryptor();
			try
			{
				byte[] resultArray = cTransform.TransformFinalBlock(toDecryptArray, 0, toDecryptArray.Length);

				tDes.Clear();
				return UTF8Encoding.UTF8.GetString(resultArray, 0, resultArray.Length);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
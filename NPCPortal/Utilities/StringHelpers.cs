﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace NPCPortal.Utilities
{
	public static class StringHelpers
	{
		public static string Truncate(string s, int desiredLength)
		{
			if(s == null)
				return null;

			if (s.Length < desiredLength)
				return s;

			string[] words = s.Split(new char[] { ' ', '\r', '\n', '\t' });
			StringBuilder truncated = new StringBuilder();
			foreach(var word in words)
			{
				if (truncated.Length + word.Length + 1 < desiredLength)
				{
					if (truncated.Length > 0)
						truncated.Append(" ");
					truncated.Append(word);
				}
				else
					break;
			}
			truncated.Append("...");
			return truncated.ToString();
		}

		public static string jQueryFilterEscape(string s)
		{
			return Regex.Replace(s, @"([!""\#\$%&'\(\)\*\+,\./:;<=>\?@\[\\\]\^`\{\|}~])", @"\\$1", RegexOptions.IgnoreCase);
		}

		public static string GetTypeDisplayName(object o)
		{
			return GetTypeDisplayName(o.GetType());
		}

		public static string GetTypeDisplayName(Type t)
		{
			var descriptions = (DescriptionAttribute[])t.GetCustomAttributes(typeof(DescriptionAttribute), true);

			if (descriptions.Length == 0)
				return t.Name;

			return descriptions[0].Description;
		}

		public static string GetPropertyDisplayName(object item)
		{
			var field = item.GetType().GetField(item.ToString());
			return GetPropertyDescription(field);
		}

		public static string GetPropertyDisplayName(PropertyInfo propertyInfo)
		{
			// Check to see if Display attribute has been set for item.
			var display = propertyInfo.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault() as DisplayAttribute;
			if (display != null)
				return display.Name;
			else
				return propertyInfo.Name;
		}

		public static string GetDescription(this Enum value)
		{
			return GetEnumDescription(value);
		}

		public static string GetEnumDescription(Enum value)
		{
			FieldInfo fi = value.GetType().GetField(value.ToString());

			//handle enum flags
			if (fi == null)
			{
				List<String> descriptions = new List<String>();
				foreach (Enum e in Enum.GetValues(value.GetType()))
					if (value.HasFlag(e) && Convert.ToInt32(e) != 0) //if has flag, and flag isn't unknown
						descriptions.Add(GetEnumDescription(e));

				return String.Join(", ", descriptions.ToArray());
			}

			DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes( typeof(DescriptionAttribute), false);

			if (attributes != null &&
				attributes.Length > 0)
				return attributes[0].Description;

			return value.ToString();
		}

		public static string ToString(this Enum value)
		{
			return GetEnumDescription(value);
		}

		public static string GetPropertyDescription(object item)
		{
			if (item == null)
				return String.Empty;
			var field = item.GetType().GetField(item.ToString());
			return GetPropertyDescription(field);
		}

		public static string GetPropertyDescription(PropertyInfo propertyInfo)
		{
			// Check to see if Display attribute has been set for item.
			var display = propertyInfo.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault() as DisplayAttribute;
			if (display != null)
				return display.Description;
			else
				return null;		
		}

		/// <summary>
		/// Preps a user-string for web display
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static string Display(this String s)
		{
			if (HttpContext.Current != null)
			{
				return Display(s, HttpContext.Current.GetCurrentUser());
			}
			else
				return s;
		}

		public static string Display(this String s, User user)
		{
			bool filterProfanities = false;

			//Check site settings to see if profanity should be filtered
			var moderationSettings = NPCPortalWeb.SiteSettings.OfType<ModerationSettings>().FirstOrDefault();
			if (moderationSettings != null)
				filterProfanities = moderationSettings.FilterProfanity;

			//Override profanity settings with the current user's preferences
			if (user != null)
			{
				var userSettings = user.Settings.OfType<UserGeneralSettings>().FirstOrDefault();
				filterProfanities = userSettings == null || userSettings.FilterProfanity;
			}

			if (filterProfanities)
				s = ProfanityFilter.FilterProfanity(s);

			return s;
		}

		/// <summary>
		/// Preps a user-string for web display that is formatted with markdown
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static string DisplayMarkdown(this String s)
		{
			return s.TransformMarkdown();
		}

		private static MarkdownDeep.Markdown _markdown = new MarkdownDeep.Markdown() { SafeMode = true };
		public static string TransformMarkdown(this String markdownText)
		{
			markdownText = markdownText.Display();

			string html = _markdown.Transform(markdownText);

			//find unformatted links and convert them
			string urlRegex = @"((?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:/[a-z\u00a1-\uffff0-9-._~:/?#\[\]@!$&'()*+,;=%]*)?)";
			html = Regex.Replace(html,
				@"(?<!src=[""'])(?<!href=[""'])" + urlRegex, 
				"<a href=\"$1\" target=\"blank\">$1</a>",
				RegexOptions.IgnoreCase);

			//clean up images
			html = Regex.Replace(html, @"(<img[^>]+)(>)", "$1 class=\"thumbnail\" style=\"max-width:100%\"$2", RegexOptions.IgnoreCase);
			//keep the tab open for users clicking external links
			html = Regex.Replace(html, @"(<a[^>]+)(>)", "$1 target=\"blank\"$2", RegexOptions.IgnoreCase);

			//replace youtube auto-links with embeds instead
			var matches = Regex.Matches(html, @"<a[^>]+?href=""(https?://(?:www\.)?youtube\.com/[^""]+)""[^>]*>([^<]+)</a>", RegexOptions.IgnoreCase);

			foreach(Match match in matches)
			{
				if (match.Groups[1].Value == match.Groups[2].Value) //check for auto-link
				{
					html = html.Replace(match.Groups[0].Value,
						String.Format(@"<iframe width=""774"" height=""435"" src=""//www.youtube.com/embed/{0}"" frameborder=""0"" allowfullscreen></iframe>",
						Regex.Match(match.Groups[1].Value, @"v=([^&]+)").Groups[1].Value));
				}
			}

			//replace vimeo auto-links with embeds instead
			matches = Regex.Matches(html, @"<a[^>]+?href=""(https?://(?:www\.)?vimeo\.com/[^""]+)""[^>]*>([^<]+)</a>", RegexOptions.IgnoreCase);

			foreach(Match match in matches)
			{
				if (match.Groups[1].Value == match.Groups[2].Value) //check for auto-link
				{
					html = html.Replace(match.Groups[0].Value,
						String.Format(@"<iframe src=""//player.vimeo.com/video/{0}?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff"" width=""774"" height=""435"" frameborder=""0"" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>",
						Regex.Match(match.Groups[1].Value, @"vimeo\.com/([^&]+)").Groups[1].Value));
				}
			}

			return html;
		}

		public static string StripMarkdown(this String markdownText)
		{
			return HttpUtility.HtmlDecode(Regex.Replace(markdownText.DisplayMarkdown(), "<[^>]+>", String.Empty, RegexOptions.IgnoreCase));
		}

		public static string GetFileSize(long fileSize)
		{
			if(fileSize > Math.Pow(1024, 3))
			{
				return ((double)fileSize / Math.Pow(1024, 3)).ToString("n2") + " GB";
			}
			if(fileSize > 1024 * 1024)
			{
				return ((double)fileSize / (1024 * 1024)).ToString("n2") + " MB";
			}
			if(fileSize > 1024)
			{
				//rounding is OK on KB
				return (fileSize / 1024) + " KB";
			}
			return fileSize.ToString() + " bytes";
		}

		public static string GetTimeDelta(DateTime date, string preposition = null)
		{
			//Load correct time zone for server
			TimeZoneInfo timeZone = NPCPortalWeb.GetCurrentTimeZone();

			var actualDate = TimeZoneInfo.ConvertTimeFromUtc(date, timeZone);
			var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
			TimeSpan span = new TimeSpan(now.Ticks - actualDate.Ticks);

			if (now > actualDate)
			{
				int quantity = 0;
				string unit = "";
				if (span.TotalSeconds < 60)
				{
					quantity = (int)span.TotalSeconds;
					unit = "second";
				}
				else if (span.TotalMinutes < 60)
				{
					quantity = (int)span.TotalMinutes;
					unit = "minute";
				}
				else if (span.TotalHours < 24)
				{
					quantity = (int)span.TotalHours;
					unit = "hour";
				}
				else if (span.TotalDays < 7)
				{
					quantity = (int)span.TotalDays;
					unit = "day";
				}
				else
				{
					if (preposition != null)
						return preposition + " " + actualDate.ToString("MMMM dd, yyyy");
					else
						return actualDate.ToString("MMMM dd, yyyy");
				}

				return String.Format("{0} {1}{2} ago",
					quantity,
					unit,
					quantity != 1 ? "s" : String.Empty);
			}


			return actualDate.ToString("MMMM dd, yyyy");
		}

		public static string ToRelativeString(this DateTime date, string preposition = null)
		{
			return GetTimeDelta(date, preposition);
		}

		public static string ToTimeZoneString(this DateTime date)
		{
			TimeZoneInfo timeZone = NPCPortalWeb.GetCurrentTimeZone();
			return TimeZoneInfo.ConvertTimeFromUtc(date, timeZone).ToString();
		}

		public static string URLEncode(this String s)
		{
			return HttpUtility.UrlPathEncode(s).Replace("/", "%2F").Replace("=", "%3D").Replace("+", "%2B");
		}

		public static string PluralizeWord(string word, int count)
		{
			if (count == 1)
				return word;

			if (word.EndsWith("y"))
				return word.Substring(0, word.Length - 1) + "ies";
			else
				return word + "s";
		}

		public static string MakeSingular(this String s)
		{
			if (s.EndsWith("ies", true, CultureInfo.CurrentCulture))
				return s.Substring(0, s.Length - 3) + "y";
			else if (s.EndsWith("s"))
				return s.Substring(0, s.Length - 1);
			else
				return s;
		}

		public static string MakePlural(this String s)
		{
			if (s.EndsWith("y", true, CultureInfo.CurrentCulture))
				return s.Substring(0, s.Length - 1) + "ies";
			else
				return s + "s";
		}
	}
}
﻿using NPCPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.WebPages;
using Microsoft.AspNet.Identity;

namespace NPCPortal.Utilities
{
	public static class UserExtensions
	{
		public static bool CanDoAny(this IPrincipal webUser, params PermissionTypes[] permissionTypes)
		{
			foreach (var permission in permissionTypes)
			{
				if (CanDo(webUser, permission))
					return true;
			}

			return false;
		}

		public static bool CanDo(this IPrincipal webUser, PermissionTypes permissionType)
		{
			using (NPCPortalDB db = new NPCPortalDB())
			{
				PermissionsSettings settings = HttpContext.Current.Items["Permissions"] as PermissionsSettings;
				if (settings == null)
				{
					settings = db.SiteSettings
								.Include("ReputationPermissions")
								.OfType<PermissionsSettings>().First();

					HttpContext.Current.Items["Permissions"] = settings;
				}

				if (settings.AnonymousPermissions.HasFlag(permissionType))
					return true;

				User user = null;
				if (webUser.Identity.IsAuthenticated)
				{
					user = db.Users.Find(webUser.Identity.GetUserId());

					if (user.UserState.HasFlag(UserStates.Banned))
						return false;

					var registrationSettings = NPCPortalWeb.SiteSettings.OfType<RegistrationSettings>().First();
					//require initial confirmed email, or they are stuck to pretty much no priviliges.
					if (registrationSettings.RequireEmailValidation && !user.UserState.HasFlag(UserStates.EmailValidated))
						return false;

					if (registrationSettings.RequireApproval && !user.UserState.HasFlag(UserStates.Approved))
						return false;
					
					//See if all authenticated users have this permission
					if (settings.MemberPermissions.HasFlag(permissionType))
						return true;

					if (user.Roles.Any(r => ((r.Role as Role).Permissions & permissionType) != 0))
						return true;

					if(settings.ReputationSystemEnabled)
					{
						if (settings.ReputationPermissions.Any(s => (s.PermissionType & permissionType) != 0 && s.MinimumReputation <= user.Reputation))
							return true;
					}
				}
			}
			return false;
		}

		public static string GetTrackingURL(this IPrincipal webUser, string regularURL)
		{
			string key = HttpUtility.UrlEncode(Security.Encrypt(webUser.Identity.GetUserId()));
			if (regularURL.Contains("?"))
				return regularURL + "&t=" + key;
			else
				return regularURL + "?t=" + key;
		}
	}
}
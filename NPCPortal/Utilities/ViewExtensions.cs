﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NPCPortal.Utilities
{
	public static class ViewExtensions
	{
		public static string FullyQualifiedAction(this UrlHelper urlHelper, string action, string controller = null, dynamic routeValues = null)
		{
			var fullURL = urlHelper.Action(action, controller,
				   routeValues: routeValues,
				   protocol: urlHelper.RequestContext.HttpContext.Request.Url.Scheme);
			return fullURL;
		}
	}
}